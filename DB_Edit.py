
from PyQt5 import QtCore, QtGui, QtWidgets, Qt
from PyQt5.QtWidgets import QLineEdit, QMessageBox

import os
import subprocess

from xml.etree import cElementTree as ET

from SaveData import SaveData
from Confirm_Dialog import Confirm_Dialog


class DB_Edit(QtWidgets.QMainWindow):
    def __init__(self, Linux, ib_user, ib_pwd, ib_name, server, pLastPath, ib_details=None, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        if Linux:
            a = 230
            b = 191
            c = 670
            d = 171
            e = 230
        else:
            a = 200
            b = 221
            c = 640
            d = 201
            e = 200

        global LastPath
        LastPath = pLastPath

        indexes = parent.selectedIndexes()

        self.setObjectName("DB_Edit")
        self.setWindowModality(QtCore.Qt.WindowModal)
        self.resize(1004, 810)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/AdminConsole/Icons/sdatabase-edit.png"), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
        self.setWindowIcon(QtGui.QIcon(icon))

        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(140, 30, 851, 701))
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.frame.setLineWidth(1)
        self.frame.setMidLineWidth(0)
        self.frame.setObjectName("frame")
        self.label_3 = QtWidgets.QLabel(self.frame)
        self.label_3.setGeometry(QtCore.QRect(10, 20, 180, 16))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.frame)
        self.label_4.setGeometry(QtCore.QRect(10, 70, 180, 16))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.frame)
        self.label_5.setGeometry(QtCore.QRect(10, 120, 180, 16))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.frame)
        self.label_6.setGeometry(QtCore.QRect(10, 170, 180, 16))
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.frame)
        self.label_7.setGeometry(QtCore.QRect(10, 220, 180, 16))
        self.label_7.setObjectName("label_7")
        self.label_8 = QtWidgets.QLabel(self.frame)
        self.label_8.setGeometry(QtCore.QRect(10, 270, 180, 16))
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.frame)
        self.label_9.setGeometry(QtCore.QRect(10, 320, 180, 16))
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(self.frame)
        self.label_10.setGeometry(QtCore.QRect(10, 370, 180, 16))
        self.label_10.setObjectName("label_10")
        self.label_11 = QtWidgets.QLabel(self.frame)
        self.label_11.setGeometry(QtCore.QRect(10, 420, 180, 16))
        self.label_11.setObjectName("label_11")
        self.label_11.setDisabled(True)
        self.label_12 = QtWidgets.QLabel(self.frame)
        self.label_12.setGeometry(QtCore.QRect(10, 470, 180, 16))
        self.label_12.setObjectName("label_12")
        self.label_13 = QtWidgets.QLabel(self.frame)
        self.label_13.setGeometry(QtCore.QRect(10, 540, 180, 16))
        self.label_13.setObjectName("label_13")
        self.label_14 = QtWidgets.QLabel(self.frame)
        self.label_14.setGeometry(QtCore.QRect(10, 590, 180, 16))
        self.label_14.setObjectName("label_14")
        self.label_15 = QtWidgets.QLabel(self.frame)
        if Linux:
            self.label_15.setGeometry(QtCore.QRect(10, 660, 210, 16))
        else:
            self.label_15.setGeometry(QtCore.QRect(10, 660, 180, 16))
        self.label_15.setObjectName("label_15")
        self.label_16 = QtWidgets.QLabel(self.frame)
        self.label_16.setGeometry(QtCore.QRect(10, 610, 180, 16))
        self.label_16.setObjectName("label_16")
        self.label_17 = QtWidgets.QLabel(self.frame)
        self.label_17.setGeometry(QtCore.QRect(10, 520, 180, 16))
        self.label_17.setObjectName("label_17")
        self.lineEdit_DB_Name = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_DB_Name.setGeometry(QtCore.QRect(a, 20, b, 20))
        self.lineEdit_DB_Name.setObjectName("lineEdit_DB_Name")
        self.lineEdit_DB_Name.setText(parent.itemFromIndex(indexes[0]).data(9, 0))
        self.lineEdit_DB_Name.setText(ib_name)
        self.lineEdit_Description = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Description.setGeometry(QtCore.QRect(a, 70, b, 20))
        self.lineEdit_Description.setObjectName("lineEdit_Description")
        self.lineEdit_Description.setText(parent.itemFromIndex(indexes[0]).data(10, 0))
        self.comboBox_Protected = QtWidgets.QComboBox(self.frame)
        self.comboBox_Protected.setGeometry(QtCore.QRect(a, 120, b, 22))
        self.comboBox_Protected.setObjectName("comboBox_Protected")
        self.comboBox_Protected.addItem("")
        self.comboBox_Protected.addItem("")
        self.comboBox_Protected.addItem("")
        self.comboBox_Protected.setCurrentText(parent.itemFromIndex(indexes[0]).data(12, 0))
        self.lineEdit_DB_Srvr_Address = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_DB_Srvr_Address.setGeometry(QtCore.QRect(a, 170, b, 20))
        self.lineEdit_DB_Srvr_Address.setObjectName("lineEdit_DB_Srvr_Address")
        self.lineEdit_DB_Srvr_Address.setText(server)
        self.comboBox_DBMS_Type = QtWidgets.QComboBox(self.frame)
        self.comboBox_DBMS_Type.setGeometry(QtCore.QRect(a, 220, b, 22))
        self.comboBox_DBMS_Type.setObjectName("comboBox_DBMS_Type")
        self.comboBox_DBMS_Type.addItem("")
        self.comboBox_DBMS_Type.addItem("")
        self.comboBox_DBMS_Type.addItem("")
        self.comboBox_DBMS_Type.addItem("")
        self.comboBox_DBMS_Type.setCurrentText(parent.itemFromIndex(indexes[0]).data(14, 0))
        self.lineEdit_DB = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_DB.setGeometry(QtCore.QRect(a, 270, b, 20))
        self.lineEdit_DB.setObjectName("lineEdit_DB")
        self.lineEdit_DB.setText(parent.itemFromIndex(indexes[0]).data(11, 0))
        self.lineEdit_DB_User = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_DB_User.setGeometry(QtCore.QRect(a, 320, b, 20))
        self.lineEdit_DB_User.setObjectName("lineEdit_DB_User")
        self.lineEdit_DB_User.setText(parent.itemFromIndex(indexes[0]).data(15, 0))
        self.lineEdit_DB_User_Pwd = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_DB_User_Pwd.setGeometry(QtCore.QRect(a, 370, b, 20))
        self.lineEdit_DB_User_Pwd.setObjectName("lineEdit_DB_User_Pwd")
        self.lineEdit_DB_User_Pwd.setText(parent.itemFromIndex(indexes[0]).data(16, 0))
        self.lineEdit_DB_User_Pwd.setEchoMode(QLineEdit.Password)
        self.lineEdit_Lang = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Lang.setGeometry(QtCore.QRect(a, 420, b, 20))
        self.lineEdit_Lang.setObjectName("lineEdit_Lang")
        self.lineEdit_Lang.setText(parent.itemFromIndex(indexes[0]).data(17, 0))
        self.lineEdit_Lang.setReadOnly(True)
        self.lineEdit_Lang.setDisabled(True)
        self.comboBox_Dates_Shift = QtWidgets.QComboBox(self.frame)
        self.comboBox_Dates_Shift.setGeometry(QtCore.QRect(a, 470, b, 22))
        self.comboBox_Dates_Shift.setObjectName("comboBox_Dates_Shift")
        self.comboBox_Dates_Shift.addItem("")
        self.comboBox_Dates_Shift.addItem("")
        self.comboBox_Dates_Shift.setCurrentText(parent.itemFromIndex(indexes[0]).data(18, 0))
        self.checkBox_allow = QtWidgets.QCheckBox(self.frame)
        self.checkBox_allow.setGeometry(QtCore.QRect(a, 520, 21, 17))
        self.checkBox_allow.setText("")
        self.checkBox_allow.setObjectName("checkBox_allow")
        #self.checkBox_allow.setChecked(indexes[18].data())
        self.checkBox_create = QtWidgets.QCheckBox(self.frame)
        self.checkBox_create.setGeometry(QtCore.QRect(a, 590, 21, 17))
        self.checkBox_create.setText("")
        self.checkBox_create.setObjectName("checkBox_create")
        self.checkBox_block = QtWidgets.QCheckBox(self.frame)
        self.checkBox_block.setGeometry(QtCore.QRect(a, 660, 21, 17))
        self.checkBox_block.setText("")
        self.checkBox_block.setObjectName("checkBox_block")
        self.label_18 = QtWidgets.QLabel(self.frame)
        self.label_18.setGeometry(QtCore.QRect(440, 20, e, 16))
        self.label_18.setObjectName("label_18")
        self.label_19 = QtWidgets.QLabel(self.frame)
        self.label_19.setGeometry(QtCore.QRect(440, 40, e, 16))
        self.label_19.setObjectName("label_19")
        self.label_20 = QtWidgets.QLabel(self.frame)
        self.label_20.setGeometry(QtCore.QRect(440, 90, e, 16))
        self.label_20.setObjectName("label_20")
        self.label_21 = QtWidgets.QLabel(self.frame)
        self.label_21.setGeometry(QtCore.QRect(440, 110, e, 16))
        self.label_21.setObjectName("label_21")
        self.label_22 = QtWidgets.QLabel(self.frame)
        self.label_22.setGeometry(QtCore.QRect(440, 160, e, 16))
        self.label_22.setObjectName("label_22")
        self.label_23 = QtWidgets.QLabel(self.frame)
        self.label_23.setGeometry(QtCore.QRect(440, 210, e, 16))
        self.label_23.setObjectName("label_23")
        self.label_24 = QtWidgets.QLabel(self.frame)
        self.label_24.setGeometry(QtCore.QRect(440, 230, e, 16))
        self.label_24.setObjectName("label_24")
        self.label_25 = QtWidgets.QLabel(self.frame)
        self.label_25.setGeometry(QtCore.QRect(440, 280, e, 16))
        self.label_25.setObjectName("label_25")
        self.label_26 = QtWidgets.QLabel(self.frame)
        self.label_26.setGeometry(QtCore.QRect(440, 300, e, 16))
        self.label_26.setObjectName("label_26")
        self.label_27 = QtWidgets.QLabel(self.frame)
        self.label_27.setGeometry(QtCore.QRect(440, 350, e, 16))
        self.label_27.setObjectName("label_27")
        self.label_28 = QtWidgets.QLabel(self.frame)
        self.label_28.setGeometry(QtCore.QRect(440, 420, e, 16))
        self.label_28.setObjectName("label_28")
        self.label_29 = QtWidgets.QLabel(self.frame)
        self.label_29.setGeometry(QtCore.QRect(440, 490, e, 16))
        self.label_29.setObjectName("label_29")
        self.label_30 = QtWidgets.QLabel(self.frame)
        self.label_30.setGeometry(QtCore.QRect(440, 560, e, 16))
        self.label_30.setObjectName("label_30")
        self.label_31 = QtWidgets.QLabel(self.frame)
        self.label_31.setGeometry(QtCore.QRect(440, 610, e, 16))
        self.label_31.setObjectName("label_31")
        self.label_32 = QtWidgets.QLabel(self.frame)
        self.label_32.setGeometry(QtCore.QRect(440, 440, e, 16))
        self.label_32.setObjectName("label_32")
        self.label_33 = QtWidgets.QLabel(self.frame)
        self.label_33.setGeometry(QtCore.QRect(440, 370, e, 16))
        self.label_33.setObjectName("label_33")
        self.label_34 = QtWidgets.QLabel(self.frame)
        self.label_34.setGeometry(QtCore.QRect(440, 510, e, 16))
        self.label_34.setObjectName("label_34")
        self.lineEdit_Begin = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Begin.setGeometry(QtCore.QRect(c, 20, d, 20))
        self.lineEdit_Begin.setObjectName("lineEdit_Begin")
        self.lineEdit_Message = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Message.setGeometry(QtCore.QRect(c, 90, d, 20))
        self.lineEdit_Message.setObjectName("lineEdit_Message")
        self.lineEdit_Block_Param = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Block_Param.setGeometry(QtCore.QRect(c, 160, d, 20))
        self.lineEdit_Block_Param.setObjectName("lineEdit_Block_Param")
        self.lineEdit_Inteval_End = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Inteval_End.setGeometry(QtCore.QRect(c, 220, d, 20))
        self.lineEdit_Inteval_End.setObjectName("lineEdit_Inteval_End")
        self.lineEdit_Allow_Code = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Allow_Code.setGeometry(QtCore.QRect(c, 280, d, 20))
        self.lineEdit_Allow_Code.setObjectName("lineEdit_Allow_Code")
        self.lineEdit_Outer_Param = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Outer_Param.setGeometry(QtCore.QRect(c, 350, d, 20))
        self.lineEdit_Outer_Param.setObjectName("lineEdit_Outer_Param")
        self.lineEdit_DB_Scurity_Profile = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_DB_Scurity_Profile.setGeometry(QtCore.QRect(c, 420, d, 20))
        self.lineEdit_DB_Scurity_Profile.setObjectName("lineEdit_DB_Scurity_Profile")
        self.lineEdit_Outer_Security_Profile = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_Outer_Security_Profile.setGeometry(QtCore.QRect(c, 490, d, 20))
        self.lineEdit_Outer_Security_Profile.setObjectName("lineEdit_Outer_Security_Profile")
        self.checkBox_block_mode = QtWidgets.QCheckBox(self.frame)
        self.checkBox_block_mode.setGeometry(QtCore.QRect(c, 560, 21, 17))
        self.checkBox_block_mode.setText("")
        self.checkBox_block_mode.setObjectName("checkBox_block_mode")
        self.checkBox_outer = QtWidgets.QCheckBox(self.frame)
        self.checkBox_outer.setGeometry(QtCore.QRect(c, 610, 21, 17))
        self.checkBox_outer.setText("")
        self.checkBox_outer.setObjectName("checkBox_outer")
        self.line = QtWidgets.QFrame(self.frame)
        self.line.setGeometry(QtCore.QRect(415, 0, 31, 701))
        self.line.setFrameShadow(QtWidgets.QFrame.Plain)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setObjectName("line")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(140, 10, 261, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 30, 111, 105))
        self.label_2.setText("")

        if Linux:
            self.label_2.setPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/database-edit.png"))
        else:
            self.label_2.setPixmap(QtGui.QPixmap(r":\AdminConsole\Icons\database-edit.png"))

        self.label_2.setScaledContents(True)
        self.label_2.setObjectName("label_2")
        self.pushButtonOK = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonOK.setGeometry(QtCore.QRect(790, 760, 75, 23))
        self.pushButtonOK.setObjectName("pushButtonOK")
        self.pushButtonCancel = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonCancel.setGeometry(QtCore.QRect(890, 760, 75, 23))
        self.pushButtonCancel.setObjectName("pushButtonCancel")
        self.setCentralWidget(self.centralwidget)

        self.pushButtonCancel.clicked.connect(self.close)
        self.pushButtonOK.clicked.connect(lambda: self.changeDB(Linux, ib_user, ib_pwd, LastPath, parent))  # Редактировать базу данных

        self.setWindowModality(Qt.Qt.ApplicationModal)

        self.retranslateUi()

        self.fill_parameters(ib_details)

    def changeDB(self, Linux, ib_user, ib_pwd,LastPath, parent):
        indexes = parent.selectedIndexes()
        currNode = parent.currentItem()
        for a in range(len(indexes)):
            if a == 9:
                currNode.setData(a, 0, self.lineEdit_DB_Name.text())
            elif a == 10:
                currNode.setData(a, 0, self.lineEdit_Description.text())
            elif a == 11:
                currNode.setData(a, 0, self.lineEdit_DB.text())
            elif a == 12:
                currNode.setData(a, 0, self.comboBox_Protected.currentText())
            elif a == 13:
                currNode.setData(a, 0, self.lineEdit_DB_Srvr_Address.text())
            elif a == 14:
                currNode.setData(a, 0, self.comboBox_DBMS_Type.currentText())
            elif a == 15:
                currNode.setData(a, 0, self.lineEdit_DB_User.text())
            elif a == 16:
                currNode.setData(a, 0, self.lineEdit_DB_User_Pwd.text())
            elif a == 17:
                currNode.setData(a, 0, self.lineEdit_Lang.text())
            elif a == 17:
                currNode.setData(a, 0, self.comboBox_Dates_Shift.currentText())

        self.check_settings(Linux)
        item = parent.currentItem()
        server_connect = ""
        if self.net_server:
            server = item.parent().parent().data(0, 0)
            server_port = item.parent().parent().data(22, 0)
            server_connect = " " + server + ":" + server_port
            clusterID = self.get_cluster_id(Linux, server, server_port)
        else:
            clusterID = self.get_cluster_id(Linux)
        clusterUser = item.parent().parent().child(0).data(7, 0)
        clusterUserPwd = item.parent().parent().child(0).data(8, 0)

        if clusterUser == "":
            cluster_admin_pwd_command = ""
        else:
            cluster_admin_pwd_command = " --cluster-user=" + clusterUser + " --cluster-pwd=" + clusterUserPwd

        if Linux:
            cluster_conn_data = " --cluster=" + clusterID + cluster_admin_pwd_command
        else:
            cluster_conn_data = " --cluster=" + clusterID

        if ib_user == "":
            ib_user = parent.selectedIndexes()[0].child(3, 19).data(0)
        if ib_pwd == "":
            ib_user_pwd = parent.selectedIndexes()[0].child(3, 20).data(0)
        else:
            ib_user_pwd = ib_pwd
        nameDB = self.lineEdit_DB.text()

        if Linux:
            bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" +\
                          clusterID + cluster_admin_pwd_command
        else:
            bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" + clusterID

        ib_data = self.check_ib_command(bashCommand)
        need_to_create = True
        for ib in ib_data:
            ib_id = ib[0]
            if ib[1] == nameDB:
                need_to_create = False
        if need_to_create == True and self.checkBox_create.isChecked():
            self.createDB(Linux, LastPath, parent)
        else:
            DB_Desc = self.lineEdit_Description.text()
            DB = self.lineEdit_DB.text()
            DB_Address = self.lineEdit_DB_Srvr_Address.text()
            DBMS_Type = self.comboBox_DBMS_Type.currentText()
            DB_User = self.lineEdit_DB_User.text()
            DB_User_Pwd = self.lineEdit_DB_User_Pwd.text()
            denied_from = self.lineEdit_Begin.text()
            denied_message = self.lineEdit_Message.text()
            denied_parameter = self.lineEdit_Block_Param.text()
            denied_to = self.lineEdit_Inteval_End.text()
            permission_code = self.lineEdit_Allow_Code.text()
            if self.checkBox_block.isChecked():
                sessions_deny = "on"
            else:
                sessions_deny = "off"
            if self.checkBox_block.isChecked():
                scheduled_jobs_deny = "on"
            else:
                scheduled_jobs_deny = "off"
            if self.checkBox_allow.isChecked():
                license_distribution = "allow"
            else:
                license_distribution = "deny"
            external_session_manager_connection_string = self.lineEdit_Outer_Param.text()
            if self.checkBox_outer.isChecked():
                external_session_manager_required = "yes"
            else:
                external_session_manager_required = "no"
            reserve_working_processes = ""
            if self.checkBox_block_mode.isChecked():
                if Linux:
                    reserve_working_processes = " --reserve-working-processes=yes"
            else:
                if Linux:
                    reserve_working_processes = " --reserve-working-processes=no"
            security_profile_name = self.lineEdit_DB_Scurity_Profile.text()
            safe_mode_security_profile_name = self.lineEdit_Outer_Security_Profile.text()

            if Linux:
                error_file = "/tmp/db_edit_command_errors.txt"
                error_file_string = " 2>\"" + error_file + "\""
            else:
                win_path = os.path.expandvars("%LOCALAPPDATA%")
                error_file = win_path + r"\Temp\db_edit_command_errors.txt"
                error_file_string = " 2>" + error_file
            if Linux:
                db_user_pwd = " --db-user=" + DB_User + " --db-pwd=" + DB_User_Pwd
            else:
                db_user_pwd = ""

            bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase" + cluster_conn_data +\
                          " update --infobase=" + ib_id + " --infobase-user=" + "\"" + ib_user + "\"" + " --infobase-pwd=" +\
                          ib_user_pwd + " --dbms=" + DBMS_Type + " --db-server=" + DB_Address + " --db-name=" + DB +\
                          db_user_pwd + " --descr=" + DB_Desc +\
                          " --denied-from=" + denied_from + " --denied-message=" + denied_message +\
                          " --denied-parameter=" + denied_parameter + " --denied-to=" + denied_to +\
                          " --permission-code=" + permission_code + " --sessions-deny=" + sessions_deny +\
                          " --scheduled-jobs-deny=" + scheduled_jobs_deny + " --license-distribution=" +\
                          license_distribution + " --external-session-manager-connection-string=" +\
                          external_session_manager_connection_string + " --external-session-manager-required=" +\
                          external_session_manager_required + reserve_working_processes +\
                          " --security-profile-name=" + security_profile_name +\
                          " --safe-mode-security-profile-name=" + safe_mode_security_profile_name + error_file_string
            os.system(bashCommand)
            f = open(error_file, 'r')
            ErrorsData = f.read()
            if Linux:
                pass
            else:
                ErrorsData = ErrorsData.encode("cp1251")
                ErrorsData = ErrorsData.decode("cp866")

            if not ErrorsData == "":
                QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
                f.close()
                os.remove(error_file)
                return
            f.close()
            os.remove(error_file)

        SaveData(Linux, parent)
        self.close()

    def createDB(self, Linux, LastPath, parent):
        self.check_settings()
        item = parent.currentItem()
        server_connect = ""
        if self.net_server:
            server = item.parent().parent().data(0, 0)
            server_port = item.parent().parent().data(22, 0)
            server_connect = " " + server + ":" + server_port
            clusterID = self.get_cluster_id(Linux, server, server_port)
        else:
            clusterID = self.get_cluster_id(Linux)
        clusterUser = item.parent().child(0).data(7, 0)
        clusterUserPwd = item.parent().child(0).data(8, 0)
        nameDB = self.lineEdit_BD.text()
        descrDB = self.lineEdit_Description.text()

        if Linux:
            error_file = "/tmp/check_ib_command_errors.txt"
            error_file_string = " 2>\"" + error_file + "\""
        else:
            win_path = os.path.expandvars("%LOCALAPPDATA%")
            error_file = win_path + r"\Temp\check_ib_command_errors.txt"
            error_file_string = " 2>" + error_file

        if clusterUser == "":
            cluster_admin_pwd_command = ""
        else:
            cluster_admin_pwd_command = " --cluster-user=" + clusterUser + " --cluster-pwd=" + clusterUserPwd

        if self.checkBox_Create_Missing_DB.isChecked():
            if Linux:
                bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" + \
                              clusterID + cluster_admin_pwd_command +\
                              error_file_string
            else:
                bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" +\
                              clusterID + error_file_string
        else:
            if Linux:
                bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" +\
                              clusterID + cluster_admin_pwd_command +\
                              error_file_string
            else:
                bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" +\
                              clusterID + error_file_string
        os.system(bashCommand)
        f = open(error_file, 'r')
        ErrorsData = f.read()
        if Linux:
            pass
        else:
            ErrorsData = ErrorsData.encode("cp1251")
            ErrorsData = ErrorsData.decode("cp866")

        if not ErrorsData == "":
            QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
            f.close()
            os.remove(error_file)
            return
        else:
            ib_data = self.check_ib_command(bashCommand)
        f.close()
        os.remove(error_file)

        need_to_create = True
        for ib in ib_data:
            if ib[1] == nameDB:
                need_to_create = False
        if need_to_create == True and self.checkBox_Create_Missing_DB.isChecked():
            nodeName = self.lineEdit_DB_Name.text()
            DB_Name = self.lineEdit_DB_Name.text()
            DB_Desc = self.lineEdit_Description.text()
            DB = self.lineEdit_BD.text()
            DB_Protected = self.comboBox_Protected.currentText()
            DB_Address = self.lineEdit_Serv_Addr.text()
            DBMS_Type = self.comboBox_Type_DBMS.currentText()
            DB_User = self.lineEdit_BD_User.text()
            DB_User_Pwd = self.lineEdit_BD_User_Pwd.text()
            DB_Lang = self.lineEdit_BD_Lang.text()
            DB_Dates_Shift = self.comboBox_Dates_Shift.currentText()
            DB_Locale = self.lineEdit_BD_Lang.text()
            if self.checkBox_Block_Shedules.isChecked():
                DB_Block_Shedules = "on"
            else:
                DB_Block_Shedules = "off"
            if self.checkBox_Allow_License.isChecked():
                DB_Allow_License = "allow"
            else:
                DB_Allow_License = "deny"

            if DB_User_Pwd == "":
                db_user_pwd_command = ""
            else:
                db_user_pwd_command = " --db-pwd=" + DB_User_Pwd
            if Linux:
                error_file = "/tmp/edit_db_errors.txt"
                error_file_string = " 2>\"" + error_file + "\""
            else:
                win_path = os.path.expandvars("%LOCALAPPDATA%")
                error_file = win_path + r"\Temp\edit_db_errors.txt"
                error_file_string = " 2>" + error_file
            if clusterUser == "":
                cluster_admin_pwd_command = ""
            else:
                cluster_admin_pwd_command = " --cluster-user=" + clusterUser + " --cluster-pwd=" + clusterUserPwd

            if self.net_server:
                bashCommand = LastPath + " " + server + ":" + server_port + " infobase create --cluster=" + clusterID +\
                              cluster_admin_pwd_command +\
                              " --create-database --name=" + DB_Name + " --dbms=" + DBMS_Type + " --db-server=" +\
                              DB_Address + " --db-name=" + DB + " --locale=" + DB_Locale + " --db-user=" + DB_User +\
                              db_user_pwd_command + " --descr=" + descrDB + " --date-offset=" + DB_Dates_Shift +\
                              " --security-level=" + DB_Protected + " --scheduled-jobs-deny=" + DB_Block_Shedules +\
                              " --license-distribution=" + DB_Allow_License + error_file_string
            else:
                bashCommand = LastPath + " infobase create --cluster=" + clusterID + cluster_admin_pwd_command +\
                              " --create-database --name=" + DB_Name +\
                              " --dbms=" + DBMS_Type + " --db-server=" + DB_Address + " --db-name=" + DB +\
                              " --locale=" + DB_Locale + " --db-user=" + DB_User + db_user_pwd_command +\
                              " --descr=" + descrDB + " --date-offset=" + DB_Dates_Shift + " --security-level=" +\
                              DB_Protected + " --scheduled-jobs-deny=" + DB_Block_Shedules +\
                              " --license-distribution=" + DB_Allow_License + error_file_string
            os.system(bashCommand)
            f = open(error_file, 'r')
            ErrorsData = f.read()
            if Linux:
                pass
            else:
                ErrorsData = ErrorsData.encode("cp1251")
                ErrorsData = ErrorsData.decode("cp866")

            if not ErrorsData == "":
                QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
                f.close()
                os.remove(error_file)
                return
            f.close()
            os.remove(error_file)

        nodeLevel = "4"

        if self.net_server == True:
            newDB = QtWidgets.QTreeWidgetItem([nodeName, nodeLevel, "", "", "", "", "", "", "", DB_Name,
                                               DB_Desc, DB, DB_Protected, DB_Address, DBMS_Type, DB_User, DB_User_Pwd,
                                               DB_Lang, DB_Dates_Shift, "", "", "", server_port])
        else:
            newDB = QtWidgets.QTreeWidgetItem([nodeName, nodeLevel, "", "", "", "", "", "", "", DB_Name,
                                               DB_Desc, DB, DB_Protected, DB_Address, DBMS_Type, DB_User, DB_User_Pwd,
                                               DB_Lang, DB_Dates_Shift, "", "", "", ""])
        sessions = QtWidgets.QTreeWidgetItem(["Сеансы", "5", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                              "", "", "", "", "", ""])
        blockings = QtWidgets.QTreeWidgetItem(["Блокировки", "5", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                               "", "", "", "", "", "", "", ""])
        connections = QtWidgets.QTreeWidgetItem(["Соединения", "5", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                                 "", "", "", "", "", "", "", ""])
        adminDB = QtWidgets.QTreeWidgetItem(["Администратор базы", "5", "", "", "", "", "", "", "", "", "", "", "", "",
                                             "", "", "", "", "", "", "", "", ""])
        newDB.addChild(sessions)
        newDB.addChild(blockings)
        newDB.addChild(connections)
        newDB.addChild(adminDB)
        self.parent().currentItem().addChild(newDB)

        # SaveData(Linux, parent)

        self.close()

    def fill_parameters(self, ib_details):
        if ib_details is not None and len(ib_details) > 0:
            # list = [infobase=0, name=1, dbms=2, db_server=3, db_name=4, db_user=5, security_level=6, license_distribution=7,
            #         scheduled_jobs_deny=8, sessions_deny=9, denied_from=10, denied_message=11, denied_parameter=12, denied_to=13,
            #         permission_code=14, external_session_manager_connection_string=15, external_session_manager_required=16,
            #         security_profile_name=17, safe_mode_security_profile_name=18, reserve_working_processes=19, descr=20]

            self.lineEdit_DB_Name.setText(ib_details[1])
            self.lineEdit_Description.setText(ib_details[20][1:-1])
            self.comboBox_Protected.setCurrentText(ib_details[6])
            self.lineEdit_DB_Srvr_Address.setText(ib_details[3])
            self.comboBox_DBMS_Type.setCurrentText(ib_details[2])
            self.lineEdit_DB.setText(ib_details[4])
            self.lineEdit_DB_User.setText(ib_details[5])
            self.lineEdit_DB_User_Pwd.setText("")
            self.lineEdit_Lang.setText("ru")
            self.comboBox_Dates_Shift.setCurrentText("0")
            if ib_details[7] == "allow":
                self.checkBox_allow.setChecked(True)
            elif ib_details[7] == "deny":
                self.checkBox_allow.setChecked(False)
            self.checkBox_create.setChecked(False)
            if ib_details[8] == "on":
                self.checkBox_block.setChecked(True)
            elif ib_details[8] == "off":
                self.checkBox_block.setChecked(False)

            self.lineEdit_Begin.setText(ib_details[10])
            self.lineEdit_Message.setText(ib_details[11])
            self.lineEdit_Block_Param.setText(ib_details[12])
            self.lineEdit_Inteval_End.setText(ib_details[13])
            self.lineEdit_Allow_Code.setText(ib_details[14])
            self.lineEdit_Outer_Param.setText(ib_details[15])
            self.lineEdit_DB_Scurity_Profile.setText(ib_details[17])
            self.lineEdit_Outer_Security_Profile.setText(ib_details[18])
            if ib_details[19] == "yes":
                self.checkBox_block_mode.setChecked(True)
            elif ib_details[19] == "no":
                self.checkBox_block_mode.setChecked(False)
            if ib_details[16] == "yes":
                self.checkBox_outer.setChecked(True)
            elif ib_details[16] == "no":
                self.checkBox_outer.setChecked(False)

    def check_settings(self, Linux):
        if Linux:
            home = os.path.expanduser("~")
            dir = home + "/.config/AdminConsole"
            file = "common_settings.xml"
            xml_file = dir + "/" + file
        else:
            # %APPDATA%          возвращает "AppData\Roaming"
            # %LOCALAPPDATA%     возвращает "AppData\Local"
            # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
            xml_file = os.path.expandvars(r"%APPDATA%\AdminConsole\common_settings.xml")
        try:
            tree = ET.ElementTree(file=xml_file)
        except:
            return
        root = tree.getroot()

        for child in root:
            if child.tag == "CommonSettings":
                for step_child in child:
                    if step_child.tag == "local_server":
                        if step_child.text == "True":
                            self.local_server = True
                            self.net_server = False
                        else:
                            self.local_server = False
                            self.net_server = True

    def get_cluster_id(self, Linux, server=None, server_port=None):
        if self.net_server:
            bashCommand = "\"" + LastPath + "\"" + " " + server + ":" + server_port + " cluster list"
        else:
            bashCommand = "\"" + LastPath + "\"" + " cluster list"
        raw_output = subprocess.check_output(bashCommand, shell=True)
        if Linux:
            output = raw_output.decode('utf-8')
        else:
            output = raw_output.decode('cp866')
        output = output.replace("\r", "")
        str_ng = output.split("\n")
        for line in str_ng:
            double_quote = line.find(":") + 1
            if line.find("cluster") != -1:
                cluster_id = line[double_quote:len(line)].strip()
                break
        return cluster_id

    def check_ib_command(self, bashCommand):
        raw_output = subprocess.check_output(bashCommand, shell=True)
        output = raw_output.decode('utf-8')
        output = output.replace("\r", "")
        str_ng = output.split("\n")
        lst = []
        need_to_add = False
        for line in str_ng:
            double_quote = line.find(":") + 1
            if line.find("infobase") != -1:
                infobase = line[double_quote:len(line)].strip()
            elif line.find("name") != -1:
                name = line[double_quote:len(line)].strip()
            elif line.find("descr") != -1:
                descr = line[double_quote:len(line)].strip()
            elif line == "":
                ib = [infobase, name, descr]
                need_to_add = True
            if need_to_add:
                lst.append(ib)
                need_to_add = False
        lst.remove(ib)
        return lst

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("DB_Edit", "Редактирование информационной базы"))
        self.label_3.setText(_translate("DB_Edit", "Имя информационной базы"))
        self.label_4.setText(_translate("DB_Edit", "Описание"))
        self.label_5.setText(_translate("DB_Edit", "Защищенное соединение"))
        self.label_6.setText(_translate("DB_Edit", "Адрес сервера базы данных"))
        self.label_7.setText(_translate("DB_Edit", "Тип СУБД"))
        self.label_8.setText(_translate("DB_Edit", "База данных"))
        self.label_9.setText(_translate("DB_Edit", "Имя пользователя базы данных"))
        self.label_10.setText(_translate("DB_Edit", "Пароль"))
        self.label_11.setText(_translate("DB_Edit", "Язык базы данных"))
        self.label_12.setText(_translate("DB_Edit", "Смещение дат"))
        self.label_13.setText(_translate("DB_Edit", "сервером 1С"))
        self.label_14.setText(_translate("DB_Edit", "Создать БД в случае ее"))
        self.label_15.setText(_translate("DB_Edit", "Блокировка регламентных заданий"))
        self.label_16.setText(_translate("DB_Edit", "отсутствия"))
        self.label_17.setText(_translate("DB_Edit", "Разрешить выдачу лицензий"))
        self.comboBox_Protected.setItemText(0, _translate("DB_Edit", "0"))
        self.comboBox_Protected.setItemText(1, _translate("DB_Edit", "1"))
        self.comboBox_Protected.setItemText(2, _translate("DB_Edit", "2"))
        self.comboBox_DBMS_Type.setItemText(0, _translate("DB_Edit", "PostgreSQL"))
        self.comboBox_DBMS_Type.setItemText(1, _translate("DB_Edit", "MSSQLServer"))
        self.comboBox_DBMS_Type.setItemText(2, _translate("DB_Edit", "IBMDB2"))
        self.comboBox_DBMS_Type.setItemText(3, _translate("DB_Edit", "OracleDatabase"))
        self.comboBox_Dates_Shift.setItemText(0, _translate("DB_Edit", "0"))
        self.comboBox_Dates_Shift.setItemText(1, _translate("DB_Edit", "2000"))
        self.label_18.setText(_translate("DB_Edit", "Начало интервала времени действия"))
        self.label_19.setText(_translate("DB_Edit", "режима блокировки сеансов"))
        self.label_20.setText(_translate("DB_Edit", "Сообщение при попытке нарушения"))
        self.label_21.setText(_translate("DB_Edit", "блокировки сеансов"))
        self.label_22.setText(_translate("DB_Edit", "Параметр блокировки сеансов"))
        self.label_23.setText(_translate("DB_Edit", "Конец интервала времени действия"))
        self.label_24.setText(_translate("DB_Edit", "режима блокировки сеансов"))
        self.label_25.setText(_translate("DB_Edit", "Код разрешения начала сеанса"))
        self.label_26.setText(_translate("DB_Edit", "вопреки блокировке сеансов"))
        self.label_27.setText(_translate("DB_Edit", "Параметры внешнего управления"))
        self.label_28.setText(_translate("DB_Edit", "Профиль безопасности"))
        self.label_29.setText(_translate("DB_Edit", "Профиль безопасности"))
        self.label_30.setText(_translate("DB_Edit", "Режим блокировки сеансов"))
        self.label_31.setText(_translate("DB_Edit", "Внешнее управление сеансами"))
        self.label_32.setText(_translate("DB_Edit", "информационной базы"))
        self.label_33.setText(_translate("DB_Edit", "сеансами"))
        self.label_34.setText(_translate("DB_Edit", "внешнего кода"))
        self.label.setText(_translate("DB_Edit", "Редактирование информационной базы"))
        self.pushButtonOK.setText(_translate("DB_Edit", "ОК"))
        self.pushButtonCancel.setText(_translate("DB_Edit", "Отмена"))
