from PyQt5 import QtCore, QtGui, QtWidgets, Qt
from PyQt5.QtWidgets import QLineEdit, QMessageBox

from AdminDB import AdminDB
from Confirm_Dialog import Confirm_Dialog
from SaveData import SaveData

import xml.etree.cElementTree as ET

import os
import subprocess
import codecs

LastPath = ""
Linux = False
Windows = False


class InfoBaseDialog(QtWidgets.QMainWindow):
    def __init__(self, pLinux, pWindows, pLastPath, server, parent=None):
        super().__init__(parent, QtCore.Qt.Window)

        global Linux
        global Windows
        global LastPath
        Linux = pLinux
        Windows = pWindows
        LastPath = pLastPath

        self.setWindowTitle("Добавление информационной базы")
        self.resize(695, 760)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/AdminConsole/Icons/database-add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(QtGui.QIcon(icon))

        self.setMinimumSize(QtCore.QSize(695, 760))
        self.setMaximumSize(QtCore.QSize(695, 760))
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.label_InfobaseName = QtWidgets.QLabel(self.centralwidget)
        self.label_InfobaseName.setGeometry(QtCore.QRect(180, 52, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_InfobaseName.setFont(font)
        self.label_InfobaseName.setObjectName("label_InfobaseName")
        self.label_InfobaseName.setText("Имя информационной базы")
        self.label_Description = QtWidgets.QLabel(self.centralwidget)
        self.label_Description.setGeometry(QtCore.QRect(180, 100, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_Description.setFont(font)
        self.label_Description.setObjectName("label_Description")
        self.label_Description.setText("Описание")
        self.label_Protected = QtWidgets.QLabel(self.centralwidget)
        self.label_Protected.setGeometry(QtCore.QRect(180, 150, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_Protected.setFont(font)
        self.label_Protected.setObjectName("label_Protected")
        self.label_Protected.setText("Защищенное соединение")
        self.label_DB_Server_Address = QtWidgets.QLabel(self.centralwidget)
        self.label_DB_Server_Address.setGeometry(QtCore.QRect(180, 200, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_DB_Server_Address.setFont(font)
        self.label_DB_Server_Address.setObjectName("label_DB_Server_Address")
        self.label_DB_Server_Address.setText("Адрес сервера баз данных")
        self.label_Type_DBMS = QtWidgets.QLabel(self.centralwidget)
        self.label_Type_DBMS.setGeometry(QtCore.QRect(180, 250, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_Type_DBMS.setFont(font)
        self.label_Type_DBMS.setObjectName("label_Type_DBMS")
        self.label_Type_DBMS.setText("Тип СУБД")
        self.label_DB = QtWidgets.QLabel(self.centralwidget)
        self.label_DB.setGeometry(QtCore.QRect(180, 300, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_DB.setFont(font)
        self.label_DB.setObjectName("label_DB")
        self.label_DB.setText("База данных")
        self.label_DB_Usesr = QtWidgets.QLabel(self.centralwidget)
        self.label_DB_Usesr.setGeometry(QtCore.QRect(180, 350, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_DB_Usesr.setFont(font)
        self.label_DB_Usesr.setObjectName("label_DB_Usesr")
        self.label_DB_Usesr.setText("Имя пользователя БД")
        self.label_DB_User_Pwd = QtWidgets.QLabel(self.centralwidget)
        self.label_DB_User_Pwd.setGeometry(QtCore.QRect(180, 400, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_DB_User_Pwd.setFont(font)
        self.label_DB_User_Pwd.setObjectName("label_DB_User_Pwd")
        self.label_DB_User_Pwd.setText("Пароль пользователя БД")
        self.label_BD_Lang = QtWidgets.QLabel(self.centralwidget)
        self.label_BD_Lang.setGeometry(QtCore.QRect(180, 450, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_BD_Lang.setFont(font)
        self.label_BD_Lang.setObjectName("label_BD_Lang")
        self.label_BD_Lang.setText("Язык БД")
        self.label_Dates_Shift = QtWidgets.QLabel(self.centralwidget)
        self.label_Dates_Shift.setGeometry(QtCore.QRect(180, 500, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_Dates_Shift.setFont(font)
        self.label_Dates_Shift.setObjectName("label_Dates_Shift")
        self.label_Dates_Shift.setText("Смещение дат")
        self.label_Allow_License = QtWidgets.QLabel(self.centralwidget)
        self.label_Allow_License.setGeometry(QtCore.QRect(180, 550, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_Allow_License.setFont(font)
        self.label_Allow_License.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.label_Allow_License.setObjectName("label_Allow_License")
        self.label_Allow_License.setText("Разрешить выдачу лицензий")
        self.label_Block_Shedules = QtWidgets.QLabel(self.centralwidget)
        self.label_Block_Shedules.setGeometry(QtCore.QRect(180, 670, 251, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_Block_Shedules.setFont(font)
        self.label_Block_Shedules.setObjectName("label_Block_Shedules")
        self.label_Block_Shedules.setText("Блокировка регламентных заданий")
        self.label_Create_Missing_DB = QtWidgets.QLabel(self.centralwidget)
        if Linux:
            self.label_Create_Missing_DB.setGeometry(QtCore.QRect(180, 620, 225, 20))
        else:
            self.label_Create_Missing_DB.setGeometry(QtCore.QRect(180, 620, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_Create_Missing_DB.setFont(font)
        self.label_Create_Missing_DB.setObjectName("label_Create_Missing_DB")
        self.label_Create_Missing_DB.setText("Создать БД в случае ее отсутствия")
        self.label_14 = QtWidgets.QLabel(self.centralwidget)
        self.label_14.setGeometry(QtCore.QRect(180, 570, 215, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_14.setFont(font)
        self.label_14.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.label_14.setObjectName("label_14")
        self.label_14.setText("сервером 1С")
        self.pushButton_OK = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_OK.setGeometry(QtCore.QRect(500, 720, 75, 30))
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.pushButton_OK.setText("ОК")
        self.pushButton_Cancel = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Cancel.setGeometry(QtCore.QRect(590, 720, 75, 30))
        self.pushButton_Cancel.setObjectName("pushButton_Cancel")
        self.pushButton_Cancel.setText("Отмена")
        self.lineEdit_DB_Name = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_DB_Name.setGeometry(QtCore.QRect(440, 50, 241, 20))
        self.lineEdit_DB_Name.setObjectName("lineEdit_DB_Name")
        self.lineEdit_Description = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Description.setGeometry(QtCore.QRect(440, 100, 241, 20))
        self.lineEdit_Description.setObjectName("lineEdit_Description")
        self.comboBox_Protected = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_Protected.setGeometry(QtCore.QRect(440, 150, 241, 22))
        self.comboBox_Protected.setObjectName("comboBox_Protected")
        self.comboBox_Protected.addItem("0")
        self.comboBox_Protected.addItem("1")
        self.comboBox_Protected.addItem("2")
        self.lineEdit_Serv_Addr = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Serv_Addr.setGeometry(QtCore.QRect(440, 200, 241, 20))
        self.lineEdit_Serv_Addr.setObjectName("lineEdit_Serv_Addr")
        self.lineEdit_Serv_Addr.setText(server)
        self.comboBox_Type_DBMS = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_Type_DBMS.setGeometry(QtCore.QRect(440, 250, 241, 22))
        self.comboBox_Type_DBMS.setObjectName("comboBox_Type_DBMS")
        self.comboBox_Type_DBMS.addItem("PostgreSQL")
        self.comboBox_Type_DBMS.addItem("MSSQLServer")
        self.comboBox_Type_DBMS.addItem("IBMDB2")
        self.comboBox_Type_DBMS.addItem("OracleDatabase")
        self.lineEdit_BD = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_BD.setGeometry(QtCore.QRect(440, 300, 241, 20))
        self.lineEdit_BD.setObjectName("lineEdit_BD")
        self.lineEdit_BD_User = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_BD_User.setGeometry(QtCore.QRect(440, 350, 241, 20))
        self.lineEdit_BD_User.setObjectName("lineEdit_BD_User")
        self.lineEdit_BD_User_Pwd = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_BD_User_Pwd.setGeometry(QtCore.QRect(440, 400, 241, 20))
        self.lineEdit_BD_User_Pwd.setObjectName("lineEdit_BD_User_Pwd")
        self.lineEdit_BD_User_Pwd.setEchoMode(QLineEdit.Password)
        self.lineEdit_BD_Lang = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_BD_Lang.setGeometry(QtCore.QRect(440, 450, 241, 20))
        self.lineEdit_BD_Lang.setObjectName("lineEdit_BD_Lang")
        self.lineEdit_BD_Lang.setText("ru")
        self.comboBox_Dates_Shift = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_Dates_Shift.setGeometry(QtCore.QRect(440, 500, 241, 20))
        self.comboBox_Dates_Shift.setObjectName("comboBox_Dates_Shift")
        self.comboBox_Dates_Shift.addItem("0")
        self.comboBox_Dates_Shift.addItem("2000")
        self.checkBox_Allow_License = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_Allow_License.setGeometry(QtCore.QRect(440, 550, 70, 17))
        self.checkBox_Allow_License.setText("")
        self.checkBox_Allow_License.setObjectName("checkBox_Allow_License")
        self.checkBox_Create_Missing_DB = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_Create_Missing_DB.setGeometry(QtCore.QRect(440, 620, 70, 17))
        self.checkBox_Create_Missing_DB.setText("")
        self.checkBox_Create_Missing_DB.setChecked(True)
        self.checkBox_Create_Missing_DB.setObjectName("checkBox_Create_Missing_DB")
        self.checkBox_Block_Shedules = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_Block_Shedules.setGeometry(QtCore.QRect(440, 670, 70, 17))
        self.checkBox_Block_Shedules.setText("")
        self.checkBox_Block_Shedules.setChecked(False)
        self.checkBox_Block_Shedules.setObjectName("checkBox_Block_Shedules")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(169, 29, 521, 671))
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frame.setLineWidth(2)
        self.frame.setObjectName("frame")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 40, 131, 131))
        self.label.setText("")

        if Linux:
            self.label.setPixmap(QtGui.QPixmap(":/AdminConsole/Icons/database-add.png"))
        else:
            self.label.setPixmap(QtGui.QPixmap(":\AdminConsole\Icons\database-add.png"))
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.frame.raise_()
        self.label_InfobaseName.raise_()
        self.label_Description.raise_()
        self.label_Protected.raise_()
        self.label_DB_Server_Address.raise_()
        self.label_Type_DBMS.raise_()
        self.label_DB.raise_()
        self.label_DB_Usesr.raise_()
        self.label_DB_User_Pwd.raise_()
        self.label_BD_Lang.raise_()
        self.label_Dates_Shift.raise_()
        self.label_Allow_License.raise_()
        self.label_Block_Shedules.raise_()
        self.label_Create_Missing_DB.raise_()
        self.label_14.raise_()
        self.pushButton_OK.raise_()
        self.pushButton_Cancel.raise_()
        self.lineEdit_DB_Name.raise_()
        self.lineEdit_Description.raise_()
        self.comboBox_Protected.raise_()
        self.lineEdit_Serv_Addr.raise_()
        self.comboBox_Type_DBMS.raise_()
        self.lineEdit_BD.raise_()
        self.lineEdit_BD_User.raise_()
        self.lineEdit_BD_User_Pwd.raise_()
        self.lineEdit_BD_Lang.raise_()
        self.comboBox_Dates_Shift.raise_()
        self.checkBox_Allow_License.raise_()
        self.checkBox_Create_Missing_DB.raise_()
        self.checkBox_Block_Shedules.raise_()
        self.label.raise_()

        self.setCentralWidget(self.centralwidget)

        self.setWindowModality(Qt.Qt.ApplicationModal)

        self.pushButton_Cancel.clicked.connect(self.close)
        self.pushButton_OK.clicked.connect(lambda: self.createDB(Linux, parent))  # Создать базу данных

    def createDB(self, Linux, parent):
        self.check_settings()
        item = parent.currentItem()
        server_connect = ""
        if self.net_server:
            server = item.parent().parent().data(0, 0)
            server_port = item.parent().parent().data(22, 0)
            server_connect = " " + server + ":" + server_port
            clusterID = self.get_cluster_id(Linux, server, server_port)
        else:
            clusterID = self.get_cluster_id(Linux)
        clusterUser = item.parent().child(0).data(7, 0)
        clusterUserPwd = item.parent().child(0).data(8, 0)
        if Linux:
            if clusterUser == "":
                cluster_admin_pwd_command = ""
            else:
                cluster_admin_pwd_command = " --cluster-user=" + clusterUser + " --cluster-pwd=" + clusterUserPwd
        else:
            cluster_admin_pwd_command = ""
        nameDB = self.lineEdit_BD.text()
        descrDB = self.lineEdit_Description.text()

        nodeName = self.lineEdit_DB_Name.text()
        DB_Name = self.lineEdit_DB_Name.text()
        DB_Desc = self.lineEdit_Description.text()
        DB = self.lineEdit_BD.text()
        DB_Protected = self.comboBox_Protected.currentText()
        DB_Address = self.lineEdit_Serv_Addr.text()
        DBMS_Type = self.comboBox_Type_DBMS.currentText()
        DB_User = self.lineEdit_BD_User.text()
        DB_User_Pwd = self.lineEdit_BD_User_Pwd.text()
        DB_Lang = self.lineEdit_BD_Lang.text()
        DB_Dates_Shift = self.comboBox_Dates_Shift.currentText()
        DB_Locale = self.lineEdit_BD_Lang.text()
        if self.checkBox_Block_Shedules.isChecked():
            DB_Block_Shedules = "on"
        else:
            DB_Block_Shedules = "off"
        if self.checkBox_Allow_License.isChecked():
            DB_Allow_License = "allow"
        else:
            DB_Allow_License = "deny"

        if DB_User_Pwd == "":
            db_user_pwd_command = ""
        else:
            db_user_pwd_command = " --db-pwd=" + DB_User_Pwd

        if Linux:
            error_file = "/tmp/check_ib_command_errors.txt"
            error_file_string = " 2>\"" + error_file + "\""
        else:
            win_path = os.path.expandvars("%LOCALAPPDATA%")
            error_file = win_path + r"\Temp\check_ib_command_errors.txt"
            error_file_string = " 2>" + error_file

        if self.checkBox_Create_Missing_DB.isChecked():
            if Linux:
                _bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" + \
                               clusterID + cluster_admin_pwd_command +\
                               error_file_string
            else:
                _bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" + \
                               clusterID + error_file_string
            os.system(_bashCommand)
            f = open(error_file, 'r')
            ErrorsData = f.read()
            if Linux:
                pass
            else:
                ErrorsData = ErrorsData.encode("cp1251")
                ErrorsData = ErrorsData.decode("cp866")

            if not ErrorsData == "":
                QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
                f.close()
                os.remove(error_file)
                return
            else:
                ib_data = self.check_ib_command(_bashCommand)
            f.close()
            os.remove(error_file)

            need_to_create = True
            if ib_data is not None:
                for ib in ib_data:
                    if ib[1] == nameDB:
                        need_to_create = False
            if need_to_create and self.checkBox_Create_Missing_DB.isChecked() or ib_data is None:
                if Linux:
                    error_file = "/tmp/create_errors.txt"
                    error_file_string = " 2>\"" + error_file + "\""
                else:
                    win_path = os.path.expandvars("%LOCALAPPDATA%")
                    error_file = win_path + r"\Temp\create_errors.txt"
                    error_file_string = " 2>" + error_file

                bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase create --cluster=" + clusterID +\
                              cluster_admin_pwd_command + " --create-database --name=" + DB_Name + " --dbms=" + DBMS_Type +\
                              " --db-server=" + DB_Address + " --db-name=" + DB + " --locale=" + DB_Locale +\
                              " --db-user=" + DB_User + db_user_pwd_command + " --descr=" + descrDB + " --date-offset=" +\
                              DB_Dates_Shift + " --security-level=" + DB_Protected + " --scheduled-jobs-deny=" +\
                              DB_Block_Shedules + " --license-distribution=" + DB_Allow_License + error_file_string
                os.system(bashCommand)
                f = open(error_file, 'r')
                ErrorsData = f.read()
                if Linux:
                    pass
                else:
                    ErrorsData = ErrorsData.encode("cp1251")
                    ErrorsData = ErrorsData.decode("cp866")

                if not ErrorsData == "":
                    QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
                    f.close()
                    os.remove(error_file)
                    return
                f.close()
                os.remove(error_file)
        else:
            if Linux:
                error_file = "/tmp/check_ib_command_errors.txt"
                error_file_string = " 2>\"" + error_file + "\""
            else:
                win_path = os.path.expandvars("%LOCALAPPDATA%")
                error_file = win_path + r"\Temp\check_ib_command_errors.txt"
                error_file_string = " 2>" + error_file
            if Linux:
                _bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" + \
                               clusterID + cluster_admin_pwd_command +\
                               error_file_string
            else:
                _bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase summary list --cluster=" + \
                               clusterID + error_file_string
            os.system(_bashCommand)
            f = open(error_file, 'r')
            ErrorsData = f.read()
            if Linux:
                pass
            else:
                ErrorsData = ErrorsData.encode("cp1251")
                ErrorsData = ErrorsData.decode("cp866")

            if not ErrorsData == "":
                QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
                f.close()
                os.remove(error_file)
                return
            f.close()
            os.remove(error_file)
        nodeLevel = "4"

        ib_data = self.check_ib_command(_bashCommand)
        for ib in ib_data:
            if ib[1] == DB_Name:
                ib_id = ib[0]
                break
            else:
                ib_id = ""

        if self.net_server:
            newDB = QtWidgets.QTreeWidgetItem([nodeName, nodeLevel, "", "", "", "", "", "", "", DB_Name,
                                               DB_Desc, DB, DB_Protected, DB_Address, DBMS_Type, DB_User, DB_User_Pwd,
                                               DB_Lang, DB_Dates_Shift, "", "", ib_id, server_port])
        else:
            newDB = QtWidgets.QTreeWidgetItem([nodeName, nodeLevel, "", "", "", "", "", "", "", DB_Name,
                                               DB_Desc, DB, DB_Protected, DB_Address, DBMS_Type, DB_User, DB_User_Pwd,
                                               DB_Lang, DB_Dates_Shift, "", "", ib_id, ""])
        sessions = QtWidgets.QTreeWidgetItem(["Сеансы", "5", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                              "", "", "", "", "", ""])
        blockings = QtWidgets.QTreeWidgetItem(["Блокировки", "5", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                               "", "", "", "", "", "", "", ""])
        connections = QtWidgets.QTreeWidgetItem(["Соединения", "5", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                                 "", "", "", "", "", "", "", ""])
        adminDB = QtWidgets.QTreeWidgetItem(["Администратор базы", "5", "", "", "", "", "", "", "", "", "", "", "", "",
                                             "", "", "", "", "", "", "", "", ""])
        newDB.addChild(sessions)
        newDB.addChild(blockings)
        newDB.addChild(connections)
        newDB.addChild(adminDB)
        self.parent().currentItem().addChild(newDB)

        self.close()

        title = "Создание администратора информационной базы"
        question = "Создать администратора информационной базы?"
        x = 420
        y = 100
        ex = Confirm_Dialog(Linux, title, question, x, y, self.parent())
        if ex.exec():
            title = "Добавить администратора ИБ"
            adminDB_Dialog = AdminDB(Linux, title, self.parent())
            adminDB_Dialog.show()
        else:
            SaveData(Linux, parent)
            ex.close()

    def check_settings(self):
        if Linux:
            home = os.path.expanduser("~")
            dir = home + "/.config/AdminConsole"
            file = "common_settings.xml"
            xml_file = dir + "/" + file
        else:
            # %APPDATA%          возвращает "AppData\Roaming"
            # %LOCALAPPDATA%     возвращает "AppData\Local"
            # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
            xml_file = os.path.expandvars(r"%APPDATA%\AdminConsole\common_settings.xml")
        try:
            tree = ET.ElementTree(file=xml_file)
        except:
            return
        root = tree.getroot()

        for child in root:
            if child.tag == "CommonSettings":
                for step_child in child:
                    if step_child.tag == "local_server":
                        if step_child.text == "True":
                            self.local_server = True
                            self.net_server = False
                        else:
                            self.local_server = False
                            self.net_server = True

    def get_cluster_id(self, Linux, server=None, server_port=None):
        if self.net_server == True:
            bashCommand = "\"" + LastPath + "\"" + " " + server + ":" + server_port + " cluster list"
        else:
            bashCommand = "\"" + LastPath + "\"" + " cluster list"
        raw_output = subprocess.check_output(bashCommand, shell=True)
        if Linux:
            output = raw_output.decode('utf-8')
        else:
            output = raw_output.decode('cp866')
        output = output.replace("\r", "")
        str_ng = output.split("\n")
        for line in str_ng:
            double_quote = line.find(":") + 1
            if line.find("cluster") != -1:
                cluster_id = line[double_quote:len(line)].strip()
                break
        return cluster_id

    def check_ib_command(self, bashCommand):
        raw_output = subprocess.check_output(bashCommand, shell=True)
        output = raw_output.decode('utf-8')
        output = output.replace("\r", "")
        if output == "":
            return None
        str_ng = output.split("\n")
        li_st = []
        need_to_add = False
        for line in str_ng:
            double_quote = line.find(":") + 1
            if line.find("infobase") != -1:
                infobase = line[double_quote:len(line)].strip()
            elif line.find("name") != -1:
                name = line[double_quote:len(line)].strip()
            elif line.find("descr") != -1:
                descr = line[double_quote:len(line)].strip()
            elif line == "":
                ib = [infobase, name, descr]
                need_to_add = True
            if need_to_add:
                li_st.append(ib)
                need_to_add = False
        li_st.remove(ib)
        return li_st
