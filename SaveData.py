
import os

from xml.etree import cElementTree as etree

class SaveData():
    def __init__(self, Linux, parent=None):
        super().__init__()
        def build(item, root):
            for row in range(item.childCount()):
                child = item.child(row)
                element = etree.SubElement(root, 'node', nodeName=child.text(0), level=child.text(1),
                                           serverName=child.text(2),
                                           serverAddress=child.text(3), port=child.text(4), agentAdmin=child.text(5),
                                           agentAdminPwd=child.text(6), clusterAdmin=child.text(7),
                                           clusterAdminPwd=child.text(8), dbName=child.text(9), description=child.text(10),
                                           db=child.text(11), protected=child.text(12), dbAdress=child.text(13),
                                           dbmsType=child.text(14), dbUser=child.text(15), dbUserPwd=child.text(16),
                                           dbLang=child.text(17), datesShift=child.text(18), ibUser=child.text(19),
                                           ibUserPwd=child.text(20), ib_id=child.text(21), conn_port=child.text(22))
                build(child, element)

        root = etree.Element('root')
        build(parent.invisibleRootItem(), root)
        from xml.dom import minidom
        xml_text = minidom.parseString(etree.tostring(root)).toprettyxml()
        if Linux:
            home = os.path.expanduser("~")
            dir = home + "/.config/AdminConsole"
            with open(dir + "/" + "db_settings.xml", "w", encoding='utf-8') as xml_file:
                xml_file.write(xml_text)
        else:
            file = os.path.expandvars(r"%APPDATA%\AdminConsole\db_settings.xml")
            with open(file, "w", encoding='utf-8') as xml_file:
                xml_file.write(xml_text)
        xml_file.close()