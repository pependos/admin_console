
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QLineEdit, QMessageBox

from SaveData import SaveData

import icons_rc

class AdminDB(QtWidgets.QDialog):
    def __init__(self, Linux, title, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        self.setObjectName("AdminDB_new")
        self.setWindowTitle(title)
        self.resize(390, 120)
        self.setMinimumSize(QtCore.QSize(390, 120))
        self.setMaximumSize(QtCore.QSize(390, 120))
        self.gridLayout = QtWidgets.QGridLayout(self)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_admin = QtWidgets.QLabel(self)
        self.label_admin.setMinimumSize(QtCore.QSize(81, 20))
        self.label_admin.setMaximumSize(QtCore.QSize(81, 20))
        self.label_admin.setObjectName("label_admin")
        self.horizontalLayout.addWidget(self.label_admin)
        self.line_admin = QtWidgets.QLineEdit(self)
        self.line_admin.setObjectName("line_admin")
        self.horizontalLayout.addWidget(self.line_admin)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_admin_pwd = QtWidgets.QLabel(self)
        self.label_admin_pwd.setMinimumSize(QtCore.QSize(80, 20))
        self.label_admin_pwd.setMaximumSize(QtCore.QSize(80, 20))
        self.label_admin_pwd.setObjectName("label_admin_pwd")
        self.horizontalLayout_2.addWidget(self.label_admin_pwd)
        self.line_admin_pwd = QtWidgets.QLineEdit(self)
        self.line_admin_pwd.setObjectName("line_admin_pwd")
        self.line_admin_pwd.setEchoMode(QLineEdit.Password)
        self.horizontalLayout_2.addWidget(self.line_admin_pwd)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 1)

        self.retranslateUi()
        self.buttonBox.accepted.connect(lambda: self.createDB_Admin(Linux, parent))
        self.buttonBox.rejected.connect(self.closeDialog)
        QtCore.QMetaObject.connectSlotsByName(self)

    def closeDialog(self) -> bool:
        self.close()

    def createDB_Admin(self, Linux, parent):
        tree = self.parent()
        try:
            currNode = self.parent().currentItem()
            adminDB = self.line_admin.text()
            adminDB_Pwd = self.line_admin_pwd.text()
            if adminDB != "":
                if currNode.data(0, 0) == "Администратор базы":
                    currNode.setData(19, 0, adminDB)
                    currNode.setData(20, 0, adminDB_Pwd)
                    SaveData(Linux, parent)
                elif currNode.data(0, 0) == "Информационные базы":
                    currNode.child(0).child(3).setData(19, 0, adminDB)
                    currNode.child(0).child(3).setData(20, 0, adminDB_Pwd)
                    SaveData(Linux, parent)
                self.close()
            else:
                warning_text = "Заполните пользователя!"
                QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
        except Exception:
            print(Exception)

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("AdminDB_new", "Dialog"))
        self.label_admin.setText(_translate("AdminDB_new", "Пользователь:"))
        self.label_admin_pwd.setText(_translate("AdminDB_new", "Пароль:"))
