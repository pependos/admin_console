
from PyQt5 import QtCore, Qt, QtWidgets, QtGui

import os
import subprocess

from xml.etree import cElementTree as ET

from Confirm_Dialog import Confirm_Dialog

class Del_DB(QtWidgets.QMainWindow):
    def __init__(self, Linux, LastPath, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/database-delete.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        self.setObjectName("MainWindow")
        self.resize(631, 192)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton_drop_database = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_drop_database.setGeometry(QtCore.QRect(450, 20, 161, 31))
        self.pushButton_drop_database.setObjectName("pushButton_drop_database")
        self.pushButton_clear_database = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_clear_database.setGeometry(QtCore.QRect(450, 60, 161, 31))
        self.pushButton_clear_database.setObjectName("pushButton_clear_database")
        self.pushButton_delete_from_list = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_delete_from_list.setGeometry(QtCore.QRect(450, 100, 161, 31))
        self.pushButton_delete_from_list.setObjectName("pushButton_delete_from_list")
        self.pushButton_Cancel = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Cancel.setGeometry(QtCore.QRect(450, 140, 161, 31))
        self.pushButton_Cancel.setObjectName("pushButton_Cancel")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(10, 20, 411, 151))
        self.textEdit.setObjectName("textEdit")
        self.setCentralWidget(self.centralwidget)

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)
        self.setWindowModality(Qt.Qt.ApplicationModal)

        self.pushButtons_connectors(Linux, LastPath, parent)

    def pushButtons_connectors(self, Linux, LastPath, parent):
        self.pushButton_drop_database.clicked.connect(lambda: self.pushButton_drop_database_clicked(Linux, LastPath, parent))
        self.pushButton_clear_database.clicked.connect(lambda: self.pushButton_clear_database_clicked(Linux, LastPath, parent))
        self.pushButton_delete_from_list.clicked.connect(lambda: self.pushButton_delete_from_list_clicked(Linux, LastPath, parent))
        self.pushButton_Cancel.clicked.connect(self.pushButton_Cancel_clicked)

    def pushButton_drop_database_clicked(self, Linux, LastPath, parent):
        title = "Удаление информационной базы"
        question = "Удалить информационную базу ПОЛНОСТЬЮ?"
        x = 320
        y = 100
        ex = Confirm_Dialog(Linux, title, question, x, y, parent)
        if ex.exec():
            self.check_settings(Linux)
            item = parent.currentItem()
            server_connect = ""
            if self.net_server:
                server = item.parent().parent().data(0, 0)
                server_port = item.parent().parent().data(22, 0)
                server_connect = " " + server + ":" + server_port
                clusterID = self.get_cluster_id(Linux, LastPath, server, server_port)
            else:
                clusterID = self.get_cluster_id(Linux, LastPath)
            clusterUser = item.parent().parent().child(0).data(7, 0)
            clusterUserPwd = item.parent().parent().child(0).data(8, 0)
            ib_id = item.data(21, 0)
            ib_user = parent.selectedIndexes()[0].child(3, 19).data(0)
            ib_user_pwd = parent.selectedIndexes()[0].child(3, 20).data(0)
            if Linux:
                if clusterUser == "":
                    cluster_admin_pwd_command = ""
                else:
                    cluster_admin_pwd_command = " --cluster-user=" + clusterUser + " --cluster-pwd=" + clusterUserPwd
            else:
                cluster_admin_pwd_command = ""
            try:
                bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase --cluster=" + clusterID +\
                              cluster_admin_pwd_command + " drop --infobase=" + ib_id + " --infobase-user=" + ib_user +\
                              " --infobase-pwd=" + ib_user_pwd + " --drop-database"
                os.system(bashCommand)
                currNode = parent.currentItem()
                parent1 = currNode.parent()
                parent1.removeChild(currNode)
            except Exception:
                print(Exception)
        else:
            ex.close()
        self.close()

    def pushButton_clear_database_clicked(self, Linux, LastPath, parent):
        title = "Удаление информационной базы"
        question = "Удалить информационную базу\n(и очистить данные на SQL-сервере)?"
        x = 320
        y = 100
        ex = Confirm_Dialog(Linux, title, question, x, y, parent)
        if ex.exec():
            self.check_settings(Linux)
            item = parent.currentItem()
            server_connect = ""
            if self.net_server:
                server = item.parent().parent().data(0, 0)
                server_port = item.parent().parent().data(22, 0)
                server_connect = " " + server + ":" + server_port
                clusterID = self.get_cluster_id(Linux, LastPath, server, server_port)
            else:
                clusterID = self.get_cluster_id(Linux, LastPath)
            clusterUser = item.parent().parent().child(0).data(7, 0)
            clusterUserPwd = item.parent().parent().child(0).data(8, 0)
            ib_id = item.data(21, 0)
            ib_user = parent.selectedIndexes()[0].child(3, 19).data(0)
            ib_user_pwd = parent.selectedIndexes()[0].child(3, 20).data(0)
            if Linux:
                cluster_conn_data = " --cluster-user=" + clusterUser + " --cluster-pwd=" + clusterUserPwd
            else:
                cluster_conn_data = ""
            try:
                bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase --cluster=" + clusterID +\
                              cluster_conn_data + " drop --infobase=" + ib_id + " --infobase-user=" + ib_user +\
                              " --infobase-pwd=" + ib_user_pwd + " --clear-database"
                os.system(bashCommand)
                currNode = parent.currentItem()
                parent1 = currNode.parent()
                parent1.removeChild(currNode)
            except Exception:
                print(Exception)
        else:
            ex.close()
        self.close()

    def pushButton_delete_from_list_clicked(self, Linux, LastPath, parent):
        title = "Удаление информационной базы"
        question = "Удалить информационную базу из списка\n(база на SQL-сервере сохранится)?"
        x = 320
        y = 100
        ex = Confirm_Dialog(Linux, title, question, x, y, parent)
        if ex.exec():
            self.check_settings(Linux)
            item = parent.currentItem()
            server_connect = ""
            if self.net_server:
                server = item.parent().parent().data(0, 0)
                server_port = item.parent().parent().data(22, 0)
                server_connect = " " + server + ":" + server_port
                clusterID = self.get_cluster_id(Linux, LastPath, server, server_port)
            else:
                clusterID = self.get_cluster_id(Linux, LastPath)
            clusterUser = item.parent().parent().child(0).data(7, 0)
            clusterUserPwd = item.parent().parent().child(0).data(8, 0)
            ib_id = item.data(21, 0)
            ib_user = parent.selectedIndexes()[0].child(3, 19).data(0)
            ib_user_pwd = parent.selectedIndexes()[0].child(3, 20).data(0)
            if Linux:
                if clusterUser == "":
                    cluster_admin_pwd_command = ""
                else:
                    cluster_admin_pwd_command = " --cluster-user=" + clusterUser + " --cluster-pwd=" + clusterUserPwd
            else:
                cluster_admin_pwd_command = ""
            try:
                bashCommand = "\"" + LastPath + "\"" + server_connect + " infobase --cluster=" + clusterID +\
                              cluster_admin_pwd_command + " drop --infobase=" + ib_id + " --infobase-user=" + ib_user +\
                              " --infobase-pwd=" + ib_user_pwd
                os.system(bashCommand)
                currNode = parent.currentItem()
                parent1 = currNode.parent()
                parent1.removeChild(currNode)
            except Exception:
                print(Exception)
        else:
            ex.close()
        self.close()

    def pushButton_Cancel_clicked(self):
        self.close()

    def check_settings(self, Linux):
        if Linux:
            home = os.path.expanduser("~")
            dir = home + "/.config/AdminConsole"
            file = "common_settings.xml"
            xml_file = dir + "/" + file
        else:
            # %APPDATA%          возвращает "AppData\Roaming"
            # %LOCALAPPDATA%     возвращает "AppData\Local"
            # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
            xml_file = os.path.expandvars(r"%APPDATA%\AdminConsole\common_settings.xml")
        try:
            tree = ET.ElementTree(file=xml_file)
        except:
            return
        root = tree.getroot()

        for child in root:
            if child.tag == "CommonSettings":
                for step_child in child:
                    if step_child.tag == "local_server":
                        if step_child.text == "True":
                            self.local_server = True
                            self.net_server = False
                        else:
                            self.local_server = False
                            self.net_server = True

    def get_cluster_id(self, Linux, LastPath, server=None, server_port=None):
        if self.net_server:
            bashCommand = "\"" + LastPath + "\"" + " " + server + ":" + server_port + " cluster list"
        else:
            bashCommand = "\"" + LastPath + "\"" + " cluster list"
        raw_output = subprocess.check_output(bashCommand, shell=True)
        if Linux:
            output = raw_output.decode('utf-8')
        else:
            output = raw_output.decode('cp866')
        output = output.replace("\r", "")
        str_ng = output.split("\n")
        for line in str_ng:
            double_quote = line.find(":") + 1
            if line.find("cluster") != -1:
                cluster_id = line[double_quote:len(line)].strip()
                break
        return cluster_id

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Режим удаления информационной базы"))
        self.pushButton_drop_database.setText(_translate("MainWindow", "1. Удалить базу данных"))
        self.pushButton_clear_database.setText(_translate("MainWindow", "2. Очистить базу данных"))
        self.pushButton_delete_from_list.setText(_translate("MainWindow", "3. Оставить без изменений"))
        self.pushButton_Cancel.setText(_translate("MainWindow", "Отмена"))
        self.textEdit.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">При удалении информационной базы можно выбрать одно из 3-х</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">действий над базой данных, в которой содержатся данные</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">инфформационной базы:</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">   1. удалить базу данных целиком;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">   2. очистить базу данных, убрав из неё все данные информационной базы</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">       (опустошить SQL-базу);</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">   3. оставить базу данных и её содержимое без изменений (убрать из</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">       списка баз, но оставить SQL-базу)</p></body></html>"))
