
from PyQt5 import Qt, QtCore, QtWidgets
from PyQt5.QtWidgets import QMessageBox, QFileDialog

from Instruction_systemd import Instruction_systemd
from Instruction_systemd_win import Instruction_systemd_win

import os
import platform
import glob
import psutil
import ctypes
import sys
import threading
import subprocess



class Ras_lin(QtWidgets.QMainWindow):
    def __init__(self, ras_statuss, L_Linux, close, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        global ras_status
        ras_status = ras_statuss
        global Linux
        Linux = L_Linux

        self.setObjectName("MainWindow")
        self.resize(520, 570)
        self.setMinimumSize(QtCore.QSize(520, 570))
        self.setMaximumSize(QtCore.QSize(520, 570))
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.frame_3 = QtWidgets.QFrame(self.centralwidget)
        self.frame_3.setMaximumSize(QtCore.QSize(501, 128))
        self.frame_3.setFrameShape(QtWidgets.QFrame.Box)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setLineWidth(2)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.label_cluster_status = QtWidgets.QLabel(self.frame_3)
        self.label_cluster_status.setMinimumSize(QtCore.QSize(211, 27))
        self.label_cluster_status.setMaximumSize(QtCore.QSize(211, 27))
        self.label_cluster_status.setObjectName("label_cluster_status")
        self.horizontalLayout_8.addWidget(self.label_cluster_status)
        self.pushButton_create_cluster = QtWidgets.QPushButton(self.frame_3)
        self.pushButton_create_cluster.setMinimumSize(QtCore.QSize(151, 37))
        self.pushButton_create_cluster.setMaximumSize(QtCore.QSize(151, 37))
        self.pushButton_create_cluster.setObjectName("pushButton_create_cluster")
        self.pushButton_create_cluster.setEnabled(False)
        self.pushButton_create_cluster.clicked.connect(self.create_cluster)
        self.horizontalLayout_8.addWidget(self.pushButton_create_cluster)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem)
        self.gridLayout.addLayout(self.horizontalLayout_8, 0, 0, 1, 1)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.label_cluster_admin = QtWidgets.QLabel(self.frame_3)
        self.label_cluster_admin.setMinimumSize(QtCore.QSize(211, 27))
        self.label_cluster_admin.setMaximumSize(QtCore.QSize(211, 27))
        self.label_cluster_admin.setObjectName("label_cluster_admin")
        self.horizontalLayout_9.addWidget(self.label_cluster_admin)
        self.lineEdit_cluster_admin = QtWidgets.QLineEdit(self.frame_3)
        self.lineEdit_cluster_admin.setMinimumSize(QtCore.QSize(151, 24))
        self.lineEdit_cluster_admin.setMaximumSize(QtCore.QSize(151, 24))
        self.lineEdit_cluster_admin.setObjectName("lineEdit_cluster_admin")
        self.horizontalLayout_9.addWidget(self.lineEdit_cluster_admin)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_9.addItem(spacerItem1)
        self.gridLayout.addLayout(self.horizontalLayout_9, 1, 0, 1, 1)
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.label_cluster_admin_pwd = QtWidgets.QLabel(self.frame_3)
        self.label_cluster_admin_pwd.setMinimumSize(QtCore.QSize(211, 27))
        self.label_cluster_admin_pwd.setMaximumSize(QtCore.QSize(211, 27))
        self.label_cluster_admin_pwd.setObjectName("label_cluster_admin_pwd")
        self.horizontalLayout_10.addWidget(self.label_cluster_admin_pwd)
        self.lineEdit_cluster_admin_pwd = QtWidgets.QLineEdit(self.frame_3)
        self.lineEdit_cluster_admin_pwd.setMinimumSize(QtCore.QSize(151, 24))
        self.lineEdit_cluster_admin_pwd.setMaximumSize(QtCore.QSize(151, 24))
        self.lineEdit_cluster_admin_pwd.setObjectName("lineEdit_cluster_admin_pwd")
        self.horizontalLayout_10.addWidget(self.lineEdit_cluster_admin_pwd)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_10.addItem(spacerItem2)
        self.gridLayout.addLayout(self.horizontalLayout_10, 2, 0, 1, 1)
        self.gridLayout_2.addWidget(self.frame_3, 0, 0, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(20, 27, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem3, 1, 0, 1, 1)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setMinimumSize(QtCore.QSize(501, 128))
        self.frame.setMaximumSize(QtCore.QSize(501, 128))
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setLineWidth(2)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_start_systemd = QtWidgets.QLabel(self.frame)
        self.label_start_systemd.setMinimumSize(QtCore.QSize(259, 27))
        self.label_start_systemd.setMaximumSize(QtCore.QSize(259, 27))
        self.label_start_systemd.setObjectName("label_start_systemd")
        self.horizontalLayout.addWidget(self.label_start_systemd)
        spacerItem4 = QtWidgets.QSpacerItem(18, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.pushButton_instr = QtWidgets.QPushButton(self.frame)
        self.pushButton_instr.setMinimumSize(QtCore.QSize(181, 30))
        self.pushButton_instr.setMaximumSize(QtCore.QSize(181, 30))
        self.pushButton_instr.setObjectName("pushButton_instr")
        self.pushButton_instr.clicked.connect(lambda: self.show_instr(Linux))
        self.horizontalLayout.addWidget(self.pushButton_instr)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(self.frame)
        self.label.setMinimumSize(QtCore.QSize(91, 18))
        self.label.setMaximumSize(QtCore.QSize(18, 16777215))
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        spacerItem5 = QtWidgets.QSpacerItem(328, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem5)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.lineEdit_service_name_systemd = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_service_name_systemd.setMinimumSize(QtCore.QSize(350, 24))
        self.lineEdit_service_name_systemd.setMaximumSize(QtCore.QSize(350, 24))
        self.lineEdit_service_name_systemd.setObjectName("lineEdit_service_name_systemd")
        self.horizontalLayout_3.addWidget(self.lineEdit_service_name_systemd)
        spacerItem6 = QtWidgets.QSpacerItem(17, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem6)
        self.pushButton_start_systemd = QtWidgets.QPushButton(self.frame)
        self.pushButton_start_systemd.setMinimumSize(QtCore.QSize(92, 37))
        self.pushButton_start_systemd.setMaximumSize(QtCore.QSize(92, 37))
        self.pushButton_start_systemd.setObjectName("pushButton_start_systemd")
        self.pushButton_start_systemd.clicked.connect(self.start_systemd)
        self.horizontalLayout_3.addWidget(self.pushButton_start_systemd)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.gridLayout_2.addWidget(self.frame, 2, 0, 1, 1)
        spacerItem7 = QtWidgets.QSpacerItem(20, 34, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem7, 3, 0, 1, 1)
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setMinimumSize(QtCore.QSize(501, 128))
        self.frame_2.setMaximumSize(QtCore.QSize(501, 128))
        self.frame_2.setFrameShape(QtWidgets.QFrame.Box)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setLineWidth(2)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_start_cmd = QtWidgets.QLabel(self.frame_2)
        self.label_start_cmd.setMinimumSize(QtCore.QSize(241, 27))
        self.label_start_cmd.setMaximumSize(QtCore.QSize(241, 27))
        self.label_start_cmd.setObjectName("label_start_cmd")
        self.horizontalLayout_4.addWidget(self.label_start_cmd)
        spacerItem8 = QtWidgets.QSpacerItem(218, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem8)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_2 = QtWidgets.QLabel(self.frame_2)
        self.label_2.setMinimumSize(QtCore.QSize(111, 18))
        self.label_2.setMaximumSize(QtCore.QSize(111, 18))
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_6.addWidget(self.label_2)
        spacerItem9 = QtWidgets.QSpacerItem(318, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem9)
        self.verticalLayout_2.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.lineEdit_path_to_rac = QtWidgets.QLineEdit(self.frame_2)
        self.lineEdit_path_to_rac.setMinimumSize(QtCore.QSize(320, 24))
        self.lineEdit_path_to_rac.setMaximumSize(QtCore.QSize(320, 24))
        self.lineEdit_path_to_rac.setObjectName("lineEdit_path_to_rac")
        self.horizontalLayout_5.addWidget(self.lineEdit_path_to_rac)
        self.pushButton_sel_rac = QtWidgets.QPushButton(self.frame_2)
        self.pushButton_sel_rac.setMinimumSize(QtCore.QSize(41, 37))
        self.pushButton_sel_rac.setMaximumSize(QtCore.QSize(41, 37))
        self.pushButton_sel_rac.setObjectName("pushButton_sel_rac")
        self.pushButton_sel_rac.clicked.connect(lambda: self.chooseFileRas(Linux))
        self.horizontalLayout_5.addWidget(self.pushButton_sel_rac)
        self.pushButton_start_cmd = QtWidgets.QPushButton(self.frame_2)
        self.pushButton_start_cmd.setMinimumSize(QtCore.QSize(92, 37))
        self.pushButton_start_cmd.setMaximumSize(QtCore.QSize(92, 37))
        self.pushButton_start_cmd.setObjectName("pushButton_start_cmd")
        self.pushButton_start_cmd.clicked.connect(self.start_cmd)
        self.horizontalLayout_5.addWidget(self.pushButton_start_cmd)
        self.verticalLayout_2.addLayout(self.horizontalLayout_5)
        self.gridLayout_2.addWidget(self.frame_2, 4, 0, 1, 1)
        spacerItem10 = QtWidgets.QSpacerItem(20, 26, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem10, 5, 0, 1, 1)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        spacerItem11 = QtWidgets.QSpacerItem(388, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem11)
        self.pushButton_Close = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Close.setMinimumSize(QtCore.QSize(92, 37))
        self.pushButton_Close.setMaximumSize(QtCore.QSize(92, 37))
        self.pushButton_Close.setObjectName("pushButton_Close")
        self.pushButton_Close.clicked.connect(self.close_window)
        self.horizontalLayout_7.addWidget(self.pushButton_Close)
        self.gridLayout_2.addLayout(self.horizontalLayout_7, 6, 0, 1, 1)
        self.setCentralWidget(self.centralwidget)

        self.retranslateUi(Linux)
        QtCore.QMetaObject.connectSlotsByName(self)
        if close == False:
            self.setWindowModality(Qt.Qt.ApplicationModal)
            self.show()

        self.onStartUp()

    def onStartUp(self):
        if ras_status == True:
            self.pushButton_create_cluster.setEnabled(True)
            self.pushButton_start_systemd.setDisabled(False)
            self.pushButton_sel_rac.setEnabled(False)
        self.check_cluster_status()

    def check_cluster_status(self):
        self.timer_started = False
        targetPattern = r"/opt/1cv8/x86_64/**/ras"
        Files = glob.glob(targetPattern, recursive=True)
        Files.sort(reverse=True)
        if len(Files) > 0:
            self.lineEdit_path_to_rac.setText(Files[0])
        if self.isVisible():
            if self.parent().service_started == False:
                self.label_cluster_status.setText("ЗАПУСТИТЕ СЕРВЕР RAS!")
            else:
                bashCommand = self.lineEdit_path_to_rac.text().replace("/ras", "/rac") + " cluster list" + " 2>/tmp/check_cluster_tmp.txt"
                os.system(bashCommand)
                f = open("/tmp/check_cluster_tmp.txt", 'r')
                something_in_file = f.read()
                if something_in_file != "":
                    self.label_cluster_status.setText("Статус кластера: НЕ СОЗДАН")
                    self.pushButton_create_cluster.setEnabled(True)
                    os.remove('/tmp/check_cluster_tmp.txt')
                else:
                    self.label_cluster_status.setText("Статус кластера: СОЗДАН")
                    self.pushButton_create_cluster.setEnabled(False)
            if self.timer_started == False:
                self.t = threading.Timer(5, self.check_cluster_status)
                self.timer_started = True
                self.t.start()

    def start_systemd(self):
        service_name = self.lineEdit_service_name_systemd.text()
        if Linux:
            need_to_close = False
            if service_name != "":
                need_to_close = True
                bash_command = "systemctl start " + service_name +  " 2>\"/tmp/check_service_tmp.txt\""
                os.system(bash_command)
                f = open("/tmp/check_service_tmp.txt", 'r')
                something_in_file = f.read()
                if something_in_file != "":
                    need_to_close = False
                    warning_text = "Нет службы с таким именем!"
                    QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
                os.remove('/tmp/check_service_tmp.txt')
            if need_to_close == True:
                self.close()
        else:
            if self.lineEdit_service_name_systemd.text() != "":
                self.pushButton_start_systemd.setDisabled(False)
                bashCommand = "sc start " + "\"" + self.lineEdit_service_name_systemd.text() + "\""
                QtWidgets.QApplication.clipboard().setText(bashCommand)
                self.parent().trayIcon.showMessage(r"Команда скопирована",
                                          r"Команда запуска службы скопирована в буфер обмена", )
                ctypes.windll.shell32.ShellExecuteW(None, 'runas', 'cmd.exe', " ".join(sys.argv), None, 1)
            else:
                self.pushButton_start_systemd.setDisabled(True)

    def close_window(self):
        self.cancel_threading()
        self.close()

    def chooseFileRas(self, Linux):
        Path = self.lineEdit_path_to_rac.text()
        SysArch = platform.architecture()
        if Linux:
            fname = QFileDialog.getOpenFileName(self.pushButton_sel_rac, "Выберите файл ras", r"/opt/1cv8/x86_64", "ras")
        else:
            if SysArch[0] == "64bit":
                fname = QFileDialog.getOpenFileName(self.pushButton_sel_rac, "Выберите файл ras.exe", Path, "ras.exe")
            else:
                fname = QFileDialog.getOpenFileName(self.pushButton_sel_rac, "Выберите файл ras.exe", Path, "ras.exe")
        if not fname[0] == "":
            self.lineEdit_path_to_rac.setText(fname[0])

    def show_instr(self, Linux):
        text = self.lineEdit_path_to_rac.text()
        if Linux:
            self.instr_window = Instruction_systemd()
        else:
            self.instr_window = Instruction_systemd_win(text)
        self.instr_window.setWindowModality(Qt.Qt.ApplicationModal)
        self.instr_window.show()

    def start_cmd(self):
        bashCommand = self.lineEdit_path_to_rac.text() + " cluster --daemon"
        os.system(bashCommand)

    def cancel_threading(self):
        try:
            if self.t.is_alive():
                self.t.cancel()
        except:
            pass

    def create_cluster(self):
        warning_text = "Поля \"Администратор кластера\" и \"Пароль админа кластера\" \nобязательны к заполнению!\n" \
                       "ВАЖНО: запомните их (а лучше запишите)!"
        QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
        if self.label_cluster_admin == "" or self.label_cluster_admin_pwd == "":
            warning_text = "Поля \"Администратор кластера\" и \"Пароль админа кластера\" \nобязательны к заполнению!\n" \
                           "ВАЖНО: запомните их (а лучше запишите)!"
            QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
        else:
            cluster = ""
            bashCommand = self.lineEdit_path_to_rac.text().replace("/ras", "/rac") + " cluster list"
            raw_output = subprocess.check_output(bashCommand, shell=True)
            output = raw_output.decode('utf-8')
            output = output.replace("\r", "")
            str = output.split("\n")
            for line in str:
                doublequote = line.find(":") + 1
                if line.find("cluster") != -1:
                    cluster = line[doublequote:len(line)].strip()
                    break
            name = self.lineEdit_cluster_admin.text()
            pwd = self.lineEdit_cluster_admin_pwd.text()
            if cluster != "":
                bashCommand = self.lineEdit_path_to_rac.text().replace("/ras", "/rac") + " cluster admin" +\
                              " --cluster=" + cluster + " --name=" + name + " --pwd=" + pwd + " --auth=pwd"
                os.system(bashCommand)

    def retranslateUi(self, Linux):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Запуск службы ras"))
        self.label_cluster_status.setText(_translate("MainWindow", "Кластер"))
        self.pushButton_create_cluster.setText(_translate("MainWindow", "Создать кластер"))
        self.label_cluster_admin.setText(_translate("MainWindow", "Администратор кластера:"))
        self.label_cluster_admin_pwd.setText(_translate("MainWindow", "Пароль админа кластера:"))
        self.label_start_systemd.setText(_translate("MainWindow", "Запустить службу ras через systemd"))
        self.pushButton_instr.setText(_translate("MainWindow", "Инструкция и создание"))
        self.label.setText(_translate("MainWindow", "Имя службы:"))
        self.pushButton_start_systemd.setText(_translate("MainWindow", "Запустить!"))
        self.label_start_cmd.setText(_translate("MainWindow", "Запустить службу ras стандартно"))
        self.label_2.setText(_translate("MainWindow", "Путь к файлу ras:"))
        self.pushButton_sel_rac.setText(_translate("MainWindow", "..."))
        self.pushButton_start_cmd.setText(_translate("MainWindow", "Запустить!"))
        self.pushButton_Close.setText(_translate("MainWindow", "Закрыть"))

        if Linux:
            self.label_start_systemd.setText(_translate("MainWindow", r"[Linux] Создать демон и запустить (systemd)"))
            self.label_start_cmd.setText(_translate("MainWindow", r"[Linux] Запустить сервер ras стандартно"))
            self.label_2.setText(_translate("MainWindow", r"Путь к файлу ras:"))
        else:
            self.label_start_systemd.setText(_translate("MainWindow", r"[Win] Создать как службу и запустить"))
            self.label_start_cmd.setText(_translate("MainWindow", r"[Win] Запустить сервер ras"))
            self.label_2.setText(_translate("MainWindow", r"Путь к файлу ras.exe:"))
