
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QTableWidgetItem, QMessageBox

import os
from xml.etree import cElementTree as common_settings
from SaveXML_settings import SaveXML_settings
from ImportXML_settings import ImportXML_settings


class Settings(QtWidgets.QMainWindow):
    def __init__(self, Linux, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        if Linux:
            a = 225
        else:
            a = 191
        self.setObjectName("MainWindow")
        self.resize(830, 648)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/AdminConsole/Icons/settings.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setMinimumSize(QtCore.QSize(280, 480))
        self.tabWidget.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_srvr_ras = QtWidgets.QWidget()
        self.tab_srvr_ras.setObjectName("tab_srvr_ras")
        self.formLayout_2 = QtWidgets.QFormLayout(self.tab_srvr_ras)
        self.formLayout_2.setObjectName("formLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.tab_srvr_ras)
        self.label.setMinimumSize(QtCore.QSize(251, 21))
        self.label.setMaximumSize(QtCore.QSize(251, 21))
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.frame = QtWidgets.QFrame(self.tab_srvr_ras)
        self.frame.setMinimumSize(QtCore.QSize(251, 121))
        self.frame.setMaximumSize(QtCore.QSize(251, 121))
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.frame.setLineWidth(2)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.checkBox_local_srvr = QtWidgets.QCheckBox(self.frame)
        self.checkBox_local_srvr.setMinimumSize(QtCore.QSize(201, 20))
        self.checkBox_local_srvr.setMaximumSize(QtCore.QSize(201, 20))
        self.checkBox_local_srvr.setObjectName("checkBox_local_srvr")
        self.gridLayout_2.addWidget(self.checkBox_local_srvr, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setMinimumSize(QtCore.QSize(31, 20))
        self.label_2.setMaximumSize(QtCore.QSize(20, 16777215))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 0, 1, 1, 1)
        self.checkBox_net_srvr = QtWidgets.QCheckBox(self.frame)
        self.checkBox_net_srvr.setMinimumSize(QtCore.QSize(201, 20))
        self.checkBox_net_srvr.setMaximumSize(QtCore.QSize(201, 20))
        self.checkBox_net_srvr.setObjectName("checkBox_net_srvr")
        self.gridLayout_2.addWidget(self.checkBox_net_srvr, 1, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.frame)
        self.label_3.setMinimumSize(QtCore.QSize(31, 20))
        self.label_3.setMaximumSize(QtCore.QSize(31, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 1, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 44, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem, 2, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame)
        self.label_5 = QtWidgets.QLabel(self.tab_srvr_ras)
        self.label_5.setText("")
        self.label_5.setObjectName("label_5")
        self.verticalLayout.addWidget(self.label_5)
        self.label_4 = QtWidgets.QLabel(self.tab_srvr_ras)
        self.label_4.setMinimumSize(QtCore.QSize(251, 20))
        self.label_4.setMaximumSize(QtCore.QSize(251, 20))
        self.label_4.setObjectName("label_4")
        self.verticalLayout.addWidget(self.label_4)
        self.tableWidget = QtWidgets.QTableWidget(self.tab_srvr_ras)
        self.tableWidget.setMinimumSize(QtCore.QSize(251, 192))
        self.tableWidget.setMaximumSize(QtCore.QSize(251, 192))
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(124)
        self.verticalLayout.addWidget(self.tableWidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_add_srvr = QtWidgets.QPushButton(self.tab_srvr_ras)
        self.pushButton_add_srvr.setMinimumSize(QtCore.QSize(30, 30))
        self.pushButton_add_srvr.setMaximumSize(QtCore.QSize(30, 30))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_add_srvr.setFont(font)
        self.pushButton_add_srvr.setObjectName("pushButton_add_srvr")
        self.horizontalLayout.addWidget(self.pushButton_add_srvr)
        self.pushButton_rem_srvr = QtWidgets.QPushButton(self.tab_srvr_ras)
        self.pushButton_rem_srvr.setMinimumSize(QtCore.QSize(30, 30))
        self.pushButton_rem_srvr.setMaximumSize(QtCore.QSize(30, 30))
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_rem_srvr.setFont(font)
        self.pushButton_rem_srvr.setObjectName("pushButton_rem_srvr")
        self.horizontalLayout.addWidget(self.pushButton_rem_srvr)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.formLayout_2.setLayout(0, QtWidgets.QFormLayout.LabelRole, self.verticalLayout)
        spacerItem2 = QtWidgets.QSpacerItem(496, 428, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout_2.setItem(0, QtWidgets.QFormLayout.FieldRole, spacerItem2)
        spacerItem3 = QtWidgets.QSpacerItem(20, 98, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.formLayout_2.setItem(1, QtWidgets.QFormLayout.LabelRole, spacerItem3)
        self.tabWidget.addTab(self.tab_srvr_ras, "")
        self.tab_etc = QtWidgets.QWidget()
        self.tab_etc.setObjectName("tab_etc")
        self.tabWidget.addTab(self.tab_etc, "")

        self.tab_about = QtWidgets.QWidget()
        self.tab_about.setObjectName("tab_about")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_about)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_6 = QtWidgets.QLabel(self.tab_about)
        self.label_6.setMinimumSize(QtCore.QSize(461, 31))
        self.label_6.setMaximumSize(QtCore.QSize(461, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_3.addWidget(self.label_6)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem4)
        self.gridLayout_3.addLayout(self.horizontalLayout_3, 0, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_7 = QtWidgets.QLabel(self.tab_about)
        self.label_7.setMinimumSize(QtCore.QSize(a, 16))
        self.label_7.setMaximumSize(QtCore.QSize(a, 16))
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_4.addWidget(self.label_7)
        self.label_8 = QtWidgets.QLabel(self.tab_about)
        self.label_8.setMinimumSize(QtCore.QSize(160, 16))
        self.label_8.setMaximumSize(QtCore.QSize(160, 16))
        self.label_8.setOpenExternalLinks(True)
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_4.addWidget(self.label_8)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem5)
        self.gridLayout_3.addLayout(self.horizontalLayout_4, 1, 0, 1, 1)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_9 = QtWidgets.QLabel(self.tab_about)
        self.label_9.setMinimumSize(QtCore.QSize(a, 16))
        self.label_9.setMaximumSize(QtCore.QSize(a, 16))
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_5.addWidget(self.label_9)
        self.label_10 = QtWidgets.QLabel(self.tab_about)
        self.label_10.setMinimumSize(QtCore.QSize(270, 16))
        self.label_10.setMaximumSize(QtCore.QSize(270, 16))
        self.label_10.setOpenExternalLinks(True)
        self.label_10.setObjectName("label_10")
        self.horizontalLayout_5.addWidget(self.label_10)
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem6)
        self.gridLayout_3.addLayout(self.horizontalLayout_5, 2, 0, 1, 1)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_11 = QtWidgets.QLabel(self.tab_about)
        self.label_11.setMinimumSize(QtCore.QSize(a, 16))
        self.label_11.setMaximumSize(QtCore.QSize(a, 16))
        self.label_11.setObjectName("label_11")
        self.horizontalLayout_6.addWidget(self.label_11)
        self.label_12 = QtWidgets.QLabel(self.tab_about)
        self.label_12.setMinimumSize(QtCore.QSize(111, 16))
        self.label_12.setMaximumSize(QtCore.QSize(111, 16))
        self.label_12.setOpenExternalLinks(True)
        self.label_12.setObjectName("label_12")
        self.horizontalLayout_6.addWidget(self.label_12)
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem7)
        self.gridLayout_3.addLayout(self.horizontalLayout_6, 3, 0, 1, 1)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label_13 = QtWidgets.QLabel(self.tab_about)
        if Linux:
            self.label_13.setMinimumSize(QtCore.QSize(255, 16))
            self.label_13.setMaximumSize(QtCore.QSize(255, 16))
        else:
            self.label_13.setMinimumSize(QtCore.QSize(225, 16))
            self.label_13.setMaximumSize(QtCore.QSize(225, 16))
        self.label_13.setObjectName("label_13")
        self.horizontalLayout_7.addWidget(self.label_13)
        self.label_14 = QtWidgets.QLabel(self.tab_about)
        if Linux:
            self.label_14.setMinimumSize(QtCore.QSize(108, 16))
            self.label_14.setMaximumSize(QtCore.QSize(108, 16))
        else:
            self.label_14.setMinimumSize(QtCore.QSize(101, 16))
            self.label_14.setMaximumSize(QtCore.QSize(101, 16))
        self.label_14.setObjectName("label_14")
        self.label_14.setOpenExternalLinks(True)
        self.horizontalLayout_7.addWidget(self.label_14)
        self.label_15 = QtWidgets.QLabel(self.tab_about)
        if Linux:
            self.label_15.setMinimumSize(QtCore.QSize(305, 16))
            self.label_15.setMaximumSize(QtCore.QSize(305, 16))
        else:
            self.label_15.setMinimumSize(QtCore.QSize(271, 16))
            self.label_15.setMaximumSize(QtCore.QSize(271, 16))
        self.label_15.setObjectName("label_15")
        self.horizontalLayout_7.addWidget(self.label_15)
        spacerItem8 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem8)
        self.gridLayout_3.addLayout(self.horizontalLayout_7, 4, 0, 1, 1)
        self.label_16 = QtWidgets.QLabel(self.tab_about)
        self.label_16.setText("")
        self.label_16.setObjectName("label_16")
        self.gridLayout_3.addWidget(self.label_16, 5, 0, 1, 1)
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.label_17 = QtWidgets.QLabel(self.tab_about)
        if Linux:
            self.label_17.setMinimumSize(QtCore.QSize(270, 16))
            self.label_17.setMaximumSize(QtCore.QSize(270, 16))
        else:
            self.label_17.setMinimumSize(QtCore.QSize(250, 16))
            self.label_17.setMaximumSize(QtCore.QSize(250, 16))
        self.label_17.setObjectName("label_17")
        self.horizontalLayout_8.addWidget(self.label_17)
        self.label_18 = QtWidgets.QLabel(self.tab_about)
        self.label_18.setMinimumSize(QtCore.QSize(200, 16))
        self.label_18.setMaximumSize(QtCore.QSize(200, 16))
        self.label_18.setObjectName("label_18")
        self.label_18.setOpenExternalLinks(True)
        self.horizontalLayout_8.addWidget(self.label_18)
        spacerItem9 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem9)
        self.gridLayout_3.addLayout(self.horizontalLayout_8, 6, 0, 1, 1)
        spacerItem10 = QtWidgets.QSpacerItem(785, 347, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem10, 7, 0, 1, 1)
        self.tabWidget.addTab(self.tab_about, "")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.textEdit = QtWidgets.QTextEdit(self.tab)
        self.textEdit.setObjectName("textEdit")
        self.gridLayout_4.addWidget(self.textEdit, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab, "")
        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem11 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem11)
        self.pushButton_OK = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_OK.setMinimumSize(QtCore.QSize(75, 30))
        self.pushButton_OK.setMaximumSize(QtCore.QSize(75, 30))
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.horizontalLayout_2.addWidget(self.pushButton_OK)
        self.pushButton_Cancel = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Cancel.setMinimumSize(QtCore.QSize(75, 30))
        self.pushButton_Cancel.setMaximumSize(QtCore.QSize(75, 30))
        self.pushButton_Cancel.setObjectName("pushButton_Cancel")
        self.horizontalLayout_2.addWidget(self.pushButton_Cancel)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.setCentralWidget(self.centralwidget)

        self.retranslateUi()
        self.push_buttons_connectors(Linux)
        self.load_settings_from_file(Linux)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(self)

    def push_buttons_connectors(self, Linux):
        self.pushButton_add_srvr.clicked.connect(self.pushButton_add_srvr_clicked)
        self.pushButton_rem_srvr.clicked.connect(self.pushButton_rem_srvr_clicked)
        self.pushButton_OK.clicked.connect(lambda: self.pushButton_OK_clicked(Linux))
        self.pushButton_Cancel.clicked.connect(self.pushButton_Cancel_clicked)
        self.checkBox_local_srvr.clicked.connect(self.checkBox_local_srvr_clicked)
        self.checkBox_net_srvr.clicked.connect(self.checkBox_net_srvr_clicked)

    def pushButton_add_srvr_clicked(self):
        rowPosition = self.tableWidget.rowCount()
        self.tableWidget.insertRow(rowPosition)
        self.tableWidget.setItem(rowPosition, 0, QTableWidgetItem(""))
        self.tableWidget.setItem(rowPosition, 1, QTableWidgetItem("1545"))

    def pushButton_rem_srvr_clicked(self):
        index = self.tableWidget.currentIndex()
        self.tableWidget.removeRow(index.row())

    def pushButton_OK_clicked(self, Linux):
        SaveXML_settings(Linux, self)
        self.close()

    def pushButton_Cancel_clicked(self):
        self.close()

    def checkBox_local_srvr_clicked(self):
        if self.checkBox_local_srvr.isChecked():
            self.checkBox_net_srvr.setChecked(False)
            self.tableWidget.setEnabled(False)
            self.pushButton_add_srvr.setEnabled(False)
            self.pushButton_rem_srvr.setEnabled(False)
        else:
            self.checkBox_local_srvr.setChecked(True)

    def checkBox_net_srvr_clicked(self):
        if self.checkBox_net_srvr.isChecked():
            self.checkBox_local_srvr.setChecked(False)
            self.tableWidget.setEnabled(True)
            self.pushButton_add_srvr.setEnabled(True)
            self.pushButton_rem_srvr.setEnabled(True)
        else:
            self.checkBox_net_srvr.setChecked(True)

    def load_settings_from_file(self, Linux):
        if Linux:
            home = os.path.expanduser("~")
            dir_path = home + "/.config/AdminConsole"
            file = "common_settings.xml"
            if os.path.isfile(os.path.expandvars(dir_path + r"/" + file)):
                self.do_import_settings_xml(Linux, dir_path + r"/" + file)
            else:
                ok = False
                if os.path.isdir(dir_path):
                    common_settings_file = open(os.path.expandvars(dir_path + r"/" + file), "w")
                    common_settings_file.write("")
                    common_settings_file.close()
                else:
                    try:
                        access_rights = 0o755
                        os.mkdir(dir_path, access_rights)
                        ok = True
                    except OSError:
                        self.create_dir_error(dir_path)
                    if ok:
                        common_settings_file = open(os.path.expandvars(dir_path + r"/" + file), "w")
                        common_settings_file.write("")
                        common_settings_file.close()
        else:
            # %APPDATA%          возвращает "AppData\Roaming"
            # %LOCALAPPDATA%     возвращает "AppData\Local"
            # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
            if os.path.isfile(os.path.expandvars(r"%APPDATA%\AdminConsole\common_settings.xml")):
                self.do_import_settings_xml(Linux, os.path.expandvars(r"%APPDATA%\AdminConsole\common_settings.xml"))
            else:
                win_path = os.path.expandvars("%APPDATA%")
                if os.path.isdir(win_path + "\AdminConsole"):
                    common_settings_file = open(win_path + "\AdminConsole\common_settings.xml", "w")
                    common_settings_file.write("")
                    common_settings_file.close()
                else:
                    try:
                        access_rights = 0o755
                        win_path = os.path.expandvars("%APPDATA%")
                        os.mkdir(win_path + r"\AdminConsole")
                        ok = True
                    except OSError:
                        self.create_dir_error(win_path + r"\AdminConsole")
                    if ok:
                        common_settings_file = open(win_path + "\AdminConsole\common_settings.xml", "w")
                        common_settings_file.write("")
                        common_settings_file.close()

    def do_import_settings_xml(self, Linux, file):
        ImportXML_settings(Linux, file, self)
        if self.checkBox_net_srvr.isChecked():
            self.tableWidget.setEnabled(True)
        else:
            self.tableWidget.setEnabled(False)

    def create_dir_error(self, dir):
        warning_text = "Не удалось создать директорию %s" % os.path.expandvars(dir)
        QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)

    def retranslateUi(self,):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Настройки программы"))
        self.pushButton_OK.setText(_translate("MainWindow", "ОК"))
        self.pushButton_Cancel.setText(_translate("MainWindow", "Отмена"))
        self.label.setText(_translate("MainWindow", "Сервер(-ы) 1С, где будет запущен ras:"))
        self.checkBox_local_srvr.setText(_translate("MainWindow", "Локальный сервер"))
        self.label_2.setToolTip(
            _translate("MainWindow", "Рекомендуется, если сервер 1С работает на локальном компьютере"))
        self.label_2.setText(_translate("MainWindow", "   ?   "))
        self.checkBox_net_srvr.setText(_translate("MainWindow", "Сервер(ы) в сети"))
        self.label_3.setToolTip(_translate("MainWindow",
                                           "Рекомендуется, если сервер(-ы) 1С работает(-ют) на отдельном(-ых) сервере(-ах) в сети"))
        self.label_3.setText(_translate("MainWindow", "   ?   "))
        self.label_4.setText(_translate("MainWindow", "Список серверов:"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "IP-адрес севера 1С:"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Порт:"))
        self.pushButton_add_srvr.setText(_translate("MainWindow", "+"))
        self.pushButton_rem_srvr.setText(_translate("MainWindow", "-"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_srvr_ras),
                                  _translate("MainWindow", "Настройки серверов ras"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_etc), _translate("MainWindow", "Прочие настройки"))
        self.label_6.setText(
            _translate("MainWindow", "Консоль администрирования серверов 1С (ras) для Linux и Windows"))
        self.label_7.setText(_translate("MainWindow", "Автор: Андрей Нешта"))
        self.label_8.setText(_translate("MainWindow",
                                        "<html><head/><body><p><a href=\"mailto:verdna2008@yandex.ru\"><span style=\" text-decoration: underline; color:#0000ff;\">verdna2008@yandex.ru</span></a></p></body></html>"))
        self.label_9.setText(_translate("MainWindow", "Сайт проекта:"))
        self.label_10.setText(_translate("MainWindow",
                                         "<html><head/><body><p><a href=\"https://bitbucket.org/pependos/admin_console\"><span style=\" text-decoration: underline; color:#0000ff;\">https://bitbucket.org/pependos/admin_console</span></a></p></body></html>"))
        self.label_11.setText(_translate("MainWindow", "Иконки для программы взяты с сайта"))
        self.label_12.setText(_translate("MainWindow",
                                         "<html><head/><body><p><a href=\"https://uxwing.com\"><span style=\" text-decoration: underline; color:#0000ff;\">https://uxwing.com</span></a></p></body></html>"))
        self.label_13.setText(_translate("MainWindow", "Выражаю благодарность Калинину Сергею"))
        self.label_14.setText(_translate("MainWindow",
                                         "<html><head/><body><p>(<a href=\"https://nuk-svk.ru\"><span style=\" text-decoration: underline; color:#0000ff;\">https://nuk-svk.ru</span></a>)</p></body></html>"))
        self.label_15.setText(_translate("MainWindow", "за идею и мотивацию к созданию этой программы."))
        self.label_17.setText(_translate("MainWindow", "Отблагодарить автора можно на странице:"))
        self.label_18.setText(_translate("MainWindow",
                                         "<html><head/><body><p><a href=\"https://sobe.ru/na/admin_console\"><span style=\" text-decoration: underline; color:#0000ff;\">https://sobe.ru/na/admin_console</span></a></p></body></html>"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_about), _translate("MainWindow", "О программе"))
        self.textEdit.setHtml(_translate("MainWindow",
                                         "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                         "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                         "p, li { white-space: pre-wrap; }\n"
                                         "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
                                         "<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" font-family:\'Open Sans\'; font-size:8pt; color:#464646; background-color:#ffffff;\">1. Всеми авторскими правами на программу владеет автор программы — Нешта Андрей Вячеславович.</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" font-family:\'Open Sans\'; font-size:8pt; color:#464646; background-color:#ffffff;\">2.</span><span style=\" font-family:\'Open Sans\'; font-size:8pt; color:#464646;\"> </span><span style=\" font-family:\'Open Sans\'; font-size:8pt; font-weight:696; color:#464646;\">Программа бесплатна, не имеет ограничения по сроку использования и в таком варианте может свободно распространяться и тиражироваться.</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" font-family:\'Open Sans\'; font-size:8pt; color:#464646; background-color:#ffffff;\">3. Никакая копия программы не может быть продана, сдана в аренду или дана напрокат.</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" font-family:\'Open Sans\'; font-size:8pt; color:#464646; background-color:#ffffff;\">4. Программа распространяется по принципу «как есть». При этом не предусматривается никаких гарантий, явных или подразумеваемых. Вы используете её на свой собственный риск. Автор не отвечает за потери данных, повреждения, потери прибыли или любые другие виды потерь, связанные с использованием (правильным или неправильным) этой программы. Автор не гарантирует, что программа сможет удовлетворить все Ваши требования, что её работа будет свободной от ошибок. Автор не несёт ответственности ни за проблемы, вызванные изменением рабочих характеристик аппаратных средств или операционных систем, созданных после выпуска программы, ни за проблемы взаимодействия данной программы и программного обеспечения, выпущенного не автором. Автор не несёт ответственности по замене или возмещению стоимости случайно повреждённого имущества и/или данных.</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" font-family:\'Open Sans\'; font-size:8pt; color:#464646; background-color:#ffffff;\">5. Все права, явно не предоставленные здесь, принадлежат Неште Андрею Вячеславовиу.</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" font-family:\'Open Sans\'; font-size:8pt; color:#464646; background-color:#ffffff;\">7. Установка и использование программы свидетельствует о согласии с условиями данной лицензии.</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" font-family:\'Open Sans\'; font-size:8pt; color:#464646; background-color:#ffffff;\">8. Если вы не согласны с условиями данной лицензии, то должны удалить файлы программы со своих устройств хранения информации и отказаться от использования программы.</span></p></body></html>"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "Лицензионное соглашение"))
