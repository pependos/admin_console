import os
import platform
import glob
import threading
import psutil
import subprocess

from PyQt5 import QtCore, QtGui, QtWidgets, Qt
from PyQt5.QtWidgets import QDialog, QMenuBar, QInputDialog, QFileDialog, QMessageBox, QAbstractScrollArea, \
    QAbstractItemView, QMenu, QSystemTrayIcon, QAction
from PyQt5.Qt import QStandardItem
from PyQt5.QtGui import QFont, QIcon

from xml.etree import cElementTree as db_settings

from AdminDB import AdminDB
from Confirm_Dialog import Confirm_Dialog
from InfoBaseDialog import InfoBaseDialog
from ServerDialog import ServerDialog
from DB_Edit import DB_Edit
from Models import TreeTableModel
from ImportXML import ImportXML
from Ras import Ras
from Ras_lin import Ras_lin
from Cluster_admin import Cluster_admin
from SaveData import SaveData
from Settings import Settings
from Del_DB import Del_DB

import icons_rc

LastPath = ""
Linux = False
Windows = False
SysArch = ""


global columnCount  # Количество колонок
columnCount = 21


class StandardItem(QStandardItem):
    def __init__(self, txt='', font_size=12, set_bold=False):
        super().__init__()

        fnt = QFont('Open Sans', font_size)
        fnt.setBold(set_bold)

        self.setEditable(False)
        self.setFont(fnt)
        self.setText(txt)


class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self, app, parent=None):
        super().__init__(parent, QtCore.Qt.Window)

        self.already_shown = False
        self.service_started = False
        self.need_to_close = False
        self.close_dialog_shown = False

        self.trayIcon = QSystemTrayIcon(QIcon(r"Icons/server-setting.png"), self)
        self.trayIcon.activated.connect(self.onTrayIconActivated)
        self.trayIcon.setToolTip("Консоль администрирования серверов 1С")
        self.trayIcon.show()
        self.disambiguateTimer = QtCore.QTimer(self)
        self.disambiguateTimer.setSingleShot(True)
        self.disambiguateTimer.timeout.connect(self.disambiguateTimerTimeout)
        menu = QMenu()
        show_main_window = QAction(QIcon(''), "Показать основное окно", menu)
        show_main_window.triggered.connect(self.show_main_window)
        menu.addAction(show_main_window)
        eexit = QAction(QIcon(''), "Выход", menu)
        eexit.triggered.connect(self.exit_question)
        menu.addAction(eexit)
        self.trayIcon.setContextMenu(menu)

        self.beforeProgramStart()

        self.setObjectName("MainWindow")
        self.resize(1000, 800)

        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")

        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)

        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.PathToRac = QtWidgets.QLineEdit(self.centralwidget)
        self.PathToRac.setMaximumSize(QtCore.QSize(500, 16777215))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.PathToRac.setFont(font)
        self.PathToRac.setObjectName("PathToRac")
        self.horizontalLayout.addWidget(self.PathToRac)

        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setMaximumSize(QtCore.QSize(22, 22))
        self.pushButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pushButton.setAutoExclusive(False)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout.addWidget(self.pushButton)

        self.label_tooltip = QtWidgets.QLabel(self.centralwidget)
        self.label_tooltip.setMaximumSize(QtCore.QSize(22, 22))
        self.label_tooltip.setAlignment(QtCore.Qt.AlignCenter)
        self.label_tooltip.setObjectName("label_tooltip")
        self.horizontalLayout.addWidget(self.label_tooltip)
        # self.verticalLayout.addLayout(self.horizontalLayout)

        self.pushButtonRefresh = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButtonRefresh.sizePolicy().hasHeightForWidth())
        self.pushButtonRefresh.setSizePolicy(sizePolicy)
        self.pushButtonRefresh.setMaximumSize(QtCore.QSize(120, 22))
        self.pushButtonRefresh.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pushButtonRefresh.setAutoExclusive(False)
        self.pushButtonRefresh.setObjectName("pushButtonRefresh")
        self.horizontalLayout.addWidget(self.pushButtonRefresh)

        self.label_icon = QtWidgets.QLabel(self.centralwidget)
        self.label_icon.setMinimumSize(QtCore.QSize(22, 22))
        self.label_icon.setMaximumSize(QtCore.QSize(22, 22))
        self.label_icon.setText("")
        self.label_icon.setObjectName("label_icon")
        self.horizontalLayout.addWidget(self.label_icon)

        self.pushButton_start_ras = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_start_ras.sizePolicy().hasHeightForWidth())
        self.pushButton_start_ras.setSizePolicy(sizePolicy)
        self.pushButton_start_ras.setMaximumSize(QtCore.QSize(120, 22))
        self.pushButton_start_ras.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pushButton_start_ras.setAutoExclusive(False)
        self.pushButton_start_ras.setObjectName("pushButton_start_ras")
        self.pushButton_start_ras.setEnabled(False)
        self.horizontalLayout.addWidget(self.pushButton_start_ras)

        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem = QtWidgets.QSpacerItem(168, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)

        self.pushButton_settings = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_settings.sizePolicy().hasHeightForWidth())
        self.pushButton_settings.setSizePolicy(sizePolicy)
        self.pushButton_settings.setMaximumSize(QtCore.QSize(120, 22))
        self.pushButton_settings.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pushButton_settings.setAutoExclusive(False)
        self.pushButton_settings.setObjectName("pushButton_settings")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/settings.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_settings.setIcon(icon)
        self.horizontalLayout.addWidget(self.pushButton_settings)

        self.RacUnderText = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setBold(False)
        font.setItalic(True)
        font.setWeight(50)
        self.RacUnderText.setFont(font)
        self.RacUnderText.setStyleSheet("color: rgb(78, 90, 255);")
        self.RacUnderText.setObjectName("RacUnderText")
        self.verticalLayout.addWidget(self.RacUnderText)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")

        self.splitter = QtWidgets.QSplitter(self.centralwidget)
        self.splitter.setEnabled(True)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.splitter.sizePolicy().hasHeightForWidth())
        self.splitter.setSizePolicy(sizePolicy)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setOpaqueResize(False)
        self.splitter.setHandleWidth(1)
        self.splitter.setObjectName("splitter")
        self.treeView = QtWidgets.QTreeWidget(self.splitter)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.treeView.sizePolicy().hasHeightForWidth())
        self.treeView.setSizePolicy(sizePolicy)
        self.treeView.setMinimumSize(QtCore.QSize(350, 0))
        self.treeView.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.treeView.setBaseSize(QtCore.QSize(350, 0))
        self.treeView.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.treeView.setAlternatingRowColors(False)
        self.treeView.setObjectName("treeView")
        self.treeView.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.treeView.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        # treeModel = TreeViewModel()
        # self.treeView.setModel(treeModel)
        self.treeView.setHeaderHidden(True)
        self.treeView.setColumnWidth(0, 250)
        self.treeView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.tableView = QtWidgets.QTableView(self.splitter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(3)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableView.sizePolicy().hasHeightForWidth())
        self.tableView.setSizePolicy(sizePolicy)
        self.tableView.setObjectName("tableView")
        self.tableView.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)  # Для плавного скролла
        self.tableView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.horizontalLayout_2.addWidget(self.splitter)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.setCentralWidget(self.centralwidget)

        self.menubar = QMenuBar(self)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 600, 32))
        self.menubar.setObjectName("menubar")
        self.setMenuBar(self.menubar)

        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        self.retranslateUi(self)
        QtCore.QMetaObject.connectSlotsByName(self)

        self.OnProgramStartup()

        icon = QIcon()
        icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/server-setting.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)

        self.pushButtons_connectors()

        self.check_settings()

        self.check_service_status()

        if self.service_started == False:
            pixmap = QtGui.QPixmap(r":/AdminConsole/Icons/wrong.png")
            pixmap_scaled = pixmap.scaled(16, 16, Qt.Qt.KeepAspectRatio, Qt.Qt.FastTransformation)
            self.label_icon.setPixmap(pixmap_scaled)
            self.label_icon.setToolTip("Локальный сервер ras не запущен")
            self.pushButton_start_ras.setEnabled(True)

    def pushButtons_connectors(self):
        # Действие при нажатии на кнопку выбора файла
        self.pushButton.clicked.connect(self.ChooseFileRac)
        self.tableView.customContextMenuRequested.connect(self.create_context_menu_tableview)
        self.treeView.customContextMenuRequested.connect(self.create_context_menu_treeview)
        self.treeView.clicked.connect(self.nodeClicked)
        self.pushButton_settings.clicked.connect(self.edit_settings)
        self.pushButton_start_ras.clicked.connect(self.start_ras_service)
        self.pushButtonRefresh.clicked.connect(lambda: self.fill_tree(Linux))

    def closeEvent(self, event):
        if self.close_dialog_shown == True:
            self.close_dialog_shown = False
            self.trayIcon.hide()
            self.need_to_close = True
            thr = Ras_lin(Linux, True)
            thr.cancel_threading()
            thr.close_window()
            self.close()
        else:
            reply = QMessageBox.question(self, 'Информация', "Вы уверены, что хотите уйти?", QMessageBox.Yes,
                                         QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.trayIcon.hide()
                self.need_to_close = True
                thr = Ras_lin(Linux, True)
                thr.cancel_threading()
                thr.close_window()
                self.close()
            else:
                event.ignore()

    def onTrayIconActivated(self, reason):
        # print("onTrayIconActivated:", reason)
        if reason == QSystemTrayIcon.Trigger:
            # self.disambiguateTimer.start(QtWidgets.QApplication.doubleClickInterval())
            self.show()
        elif reason == QSystemTrayIcon.DoubleClick:
            self.disambiguateTimer.stop()
            # print("Tray icon double clicked")

    def disambiguateTimerTimeout(self):
        # print("Tray icon single clicked")
        a = 1

    def show_main_window(self):
        self.show()

    def exit_question(self):
        reply = QMessageBox.question(self, 'Информация', "Вы уверены, что хотите уйти?", QMessageBox.Yes,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.close_dialog_shown = True
            self.trayIcon.hide()
            self.need_to_close = True
            thr = Ras_lin(Linux, True)
            thr.cancel_threading()
            thr.close_window()
            self.close()
        else:
            self.close_dialog_shown = False

    def beforeProgramStart(self):
        global Linux
        Linux = False
        global Windows
        Windows = False
        global LastPath
        LastPath = ""
        global SysArch
        from os.path import expanduser
        # home = expanduser("~")
        # print(home)
        # print(os.name)
        SysType = platform.system()
        SysArch = platform.architecture()
        # print(SysType)
        # print(SysArch[0])

        if SysType == "Windows" and SysArch[0] == "32bit":
            Windows = True
            targetPattern = r"c:\Program Files (x86)\1cv8\**\rac.exe"
            Files = glob.glob(targetPattern, recursive=True)
            Files.sort(reverse=True)
            if len(Files) > 0:
                LastPath = Files[0]
        elif SysType == "Windows" and SysArch[0] == "64bit":
            Windows = True
            targetPattern = r'c:\Program Files\1cv8\**\rac.exe'
            Files = glob.glob(targetPattern, recursive=True)
            Files.sort(reverse=True)
            if len(Files) > 0:
                LastPath = Files[0]
        elif SysType == "Linux":
            Linux = True
            targetPattern = r"/opt/1cv8/x86_64/**/rac"
            Files = glob.glob(targetPattern, recursive=True)
            Files.sort(reverse=True)
            if len(Files) > 0:
                LastPath = Files[0]

    def check_service_status(self):
        global ras_status
        self.service_started = False
        if Linux:
            PROCNAME = "ras"
        else:
            PROCNAME = "ras.exe"
        for proc in psutil.process_iter():
            if proc.name() == PROCNAME:
                self.service_started = True
                break
        if self.service_started == True:
            pixmap = QtGui.QPixmap(r":/AdminConsole/Icons/correct.png")
            pixmap_scaled = pixmap.scaled(16, 16, Qt.Qt.KeepAspectRatio, Qt.Qt.FastTransformation)
            self.label_icon.setPixmap(pixmap_scaled)
            self.label_icon.setToolTip("Сервер ras запущен")
            self.pushButton_start_ras.setText("Управление ras")
            self.pushButton_start_ras.setEnabled(True)
            ras_status = True
        else:
            pixmap = QtGui.QPixmap(r":/AdminConsole/Icons/wrong.png")
            pixmap_scaled = pixmap.scaled(16, 16, Qt.Qt.KeepAspectRatio, Qt.Qt.FastTransformation)
            self.label_icon.setPixmap(pixmap_scaled)
            self.label_icon.setToolTip("Сервер ras не запущен")
            self.pushButton_start_ras.setEnabled(True)
            ras_status = False
        t = threading.Timer(5, self.check_service_status)
        t.start()
        if self.need_to_close == True:
            t.cancel()

    def OnProgramStartup(self):
        self.PathToRac.setText(LastPath)
        if Linux:
            home = os.path.expanduser("~")
            dir = home + "/.config/AdminConsole"
            file = "db_settings.xml"
            if os.path.isfile(os.path.expandvars(dir + r"/" + file)):
                self.do_import_xml(dir + r"/" + file)
            else:
                ok = False
                try:
                    access_rights = 0o755
                    os.mkdir(dir, access_rights)
                    ok = True
                except OSError:
                    self.create_dir_error(dir)
                if ok:
                    db_settings_file = open(os.path.expandvars(dir + r"/" + file), "w")
                    db_settings_file.write("")
                    db_settings_file.close()
        else:
            # %APPDATA%          возвращает "AppData\Roaming"
            # %LOCALAPPDATA%     возвращает "AppData\Local"
            # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
            if os.path.isfile(os.path.expandvars(r"%APPDATA%\AdminConsole\db_settings.xml")):
                self.do_import_xml(os.path.expandvars(r"%APPDATA%\AdminConsole\db_settings.xml"))
            else:
                try:
                    access_rights = 0o755
                    win_path = os.path.expandvars("%APPDATA%")
                    os.mkdir(win_path + r"\AdminConsole", access_rights)
                    db_settings_file = open(os.path.expandvars(r"%APPDATA%\AdminConsole\db_settings.xml"), "w")
                    db_settings_file.write("")
                    db_settings_file.close()
                except OSError:
                    self.create_dir_error()

        ver = self.LastPath_analyze(LastPath, Linux)
        if Linux:
            home = os.path.expanduser("~")
            dir = home + "/.config/AdminConsole"
            file = "common_settings.xml"
            if os.path.isfile(os.path.expandvars(dir + r"/" + file)):
                pass
            else:
                ok = False
                if os.path.isdir(dir):
                    common_settings_file = open(os.path.expandvars(dir + r"/" + file), "w")
                    common_settings_file.write("")
                    common_settings_file.close()
                else:
                    try:
                        access_rights = 0o755
                        os.mkdir(dir, access_rights)
                        ok = True
                    except OSError:
                        self.create_dir_error(dir)
                    if ok:
                        common_settings_file = open(os.path.expandvars(dir + r"/" + file), "w")
                        common_settings_file.write("")
                        common_settings_file.close()
        else:
            # %APPDATA%          возвращает "AppData\Roaming"
            # %LOCALAPPDATA%     возвращает "AppData\Local"
            # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
            if os.path.isfile(os.path.expandvars(r"%APPDATA%\AdminConsole\common_settings.xml")):
                pass
            else:
                win_path = os.path.expandvars("%APPDATA%")
                if os.path.isdir(win_path + "\AdminConsole"):
                    common_settings_file = open(win_path + "\AdminConsole\common_settings.xml", "w")
                    common_settings_file.write("")
                    common_settings_file.close()
                else:
                    try:
                        access_rights = 0o755
                        win_path = os.path.expandvars("%APPDATA%")
                        os.mkdir(win_path + r"\AdminConsole")
                        ok = True
                    except OSError:
                        self.create_dir_error(win_path + r"\AdminConsole")
                    if ok:
                        common_settings_file = open(win_path + "\AdminConsole\common_settings.xml", "w")
                        common_settings_file.write("")
                        common_settings_file.close()

    def start_ras_service(self):
        if Linux:
            self.sevice_ras = Ras_lin(ras_status, Linux, False, self)
        else:
            self.sevice_ras = Ras(Linux, self)
        self.sevice_ras.show()

    def edit_settings(self):
        self.settings = Settings(Linux)
        self.settings.setWindowModality(Qt.Qt.ApplicationModal)
        self.settings.show()

    def LastPath_analyze(self, LastPath, Linux):
        if Linux:
            right = LastPath.rfind(r"/")
            new_string = LastPath[0:right]
            left = new_string.rfind(r"/")
            ver = LastPath[left + 1:right]
        else:
            right = LastPath.rfind(r"\\")
            new_string = LastPath[0:right]
            left = new_string.rfind(r"\\")
            ver = LastPath[left + 1:right]
        return ver

    def create_dir_error(self, dir):
        warning_text = "Не удалось создать директорию %s" % os.path.expandvars(dir)
        QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)

    def do_import_xml(self, file):
        with open(file, encoding='utf-8') as xml_file:
            xml_text = xml_file.read()
            if len(xml_text) > 0:
                root = db_settings.fromstring(xml_text)
                ImportXML(self.treeView, xml_text)
                for a in range(1, 23):
                    self.treeView.hideColumn(a)  # setColumnHidden(a, True)
        xml_file.close()

    def ChooseFileRac(self):
        if Linux:
            fname = QFileDialog.getOpenFileName(self.pushButton, "Выберите файл rac", r"/opt/1cv8/x86_64", "rac")
        else:
            if SysArch[0] == "64bit":
                fname = QFileDialog.getOpenFileName(self.pushButton, "Выберите файл rac.exe", r"c:\Program\ Files\1cv8",
                                                    "rac.exe")
            else:
                fname = QFileDialog.getOpenFileName(self.pushButton, "Выберите файл rac.exe",
                                                    r"c:\Program\ Files\ (x86)\1cv8", "rac.exe")

        if not fname[0] == "":
            self.PathToRac.setText(fname[0])
            # LastPath = fname[0]
            bashCommand = self.PathToRac.text() + " cluster list 2>\"/tmp/errors.txt\""
            os.system(bashCommand)
            f = open("/tmp/errors.txt", 'r')
            ErrorsData = f.read()
            if not ErrorsData == "":
                print(ErrorsData)
                self.RacUnderText.setText("Указан недействительный rac!")
            else:
                self.RacUnderText.setText("")
            os.remove('/tmp/errors.txt')

    def TreeViewSetPoupMenu(self):
        edit_action = QtWidgets.QAction("Edit...", self.treeView)
        edit_action.triggered.connect(lambda: self.new_item_edit_options(False))
        self.treeView.addAction(edit_action)

    def create_context_menu_treeview(self, event):
        ix = self.treeView.indexAt(event)

        indexes = self.treeView.selectedIndexes()
        treeModel = self.treeView.model()
        index = treeModel.index(0, 0)
        # if index.isValid() is False:
        if ix.column() == -1:
            self.add_server_context_menu = QMenu(self.treeView)
            firstMenu = self.add_server_context_menu.addAction("Добавить сервер...")
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/server-add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)

            firstMenu.setIcon(icon)
            action = self.add_server_context_menu.exec_(self.treeView.mapToGlobal(event))
            if action is not None:
                if action == firstMenu:
                    self.create_server_window()
        elif len(indexes) == 0:
            return
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "1":
            self.add_server_context_menu = QMenu(self.treeView)
            remSrvr = self.add_server_context_menu.addAction("Удалить сервер...")
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/server-close.png"), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
            remSrvr.setIcon(icon)
            action = self.add_server_context_menu.exec_(self.treeView.mapToGlobal(event))
            if action is not None:
                if action == remSrvr:
                    self.removeSrvr()
        elif self.treeView.itemFromIndex(indexes[0]).data(0, 0) == "Информационные базы":
            self.add_server_context_menu = QMenu(self.treeView)
            addDB = self.add_server_context_menu.addAction("Добавить информационную базу...")
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/database-add.png"), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
            addDB.setIcon(icon)
            action = self.add_server_context_menu.exec_(self.treeView.mapToGlobal(event))
            if action is not None:
                if action == addDB:
                    self.create_infoBase_window()
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "4":
            self.add_server_context_menu = QMenu(self.treeView)
            editDB = self.add_server_context_menu.addAction("Редактировать информационную базу...")
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/database-edit.png"), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
            editDB.setIcon(icon)
            remDB = self.add_server_context_menu.addAction("Удалить информационную базу...")
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/database-delete.png"), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
            remDB.setIcon(icon)
            action = self.add_server_context_menu.exec_(self.treeView.mapToGlobal(event))
            if action is not None:
                if action == remDB:
                    self.removeDB()
                elif action == editDB:
                    ib_name = self.treeView.itemFromIndex(indexes[0]).data(0, 0)
                    self.editDB(ib_name)
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "5" \
                and self.treeView.itemFromIndex(indexes[0]).data(0, 0) == "Администратор базы" \
                and (len(self.treeView.itemFromIndex(indexes[0]).data(19, 0)) > 0
                     or len(self.treeView.itemFromIndex(indexes[0]).data(20, 0)) > 0):
            self.add_server_context_menu = QMenu(self.treeView)
            editDB_Admin = self.add_server_context_menu.addAction("Редактировать администратора базы")
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/edit-round-line.png"), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
            editDB_Admin.setIcon(icon)
            remDB_Admin = self.add_server_context_menu.addAction("Удалить администратора базы")
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/close-round-line.png"), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
            remDB_Admin.setIcon(icon)
            action = self.add_server_context_menu.exec_(self.treeView.mapToGlobal(event))
            if action is not None:
                if action == editDB_Admin:
                    currentPassword = self.treeView.itemFromIndex(indexes[0]).data(20, 0)
                    inputDialog = QInputDialog()
                    inputDialog.setOkButtonText("ОК")
                    inputDialog.resize(350, 100)
                    inputDialog.setCancelButtonText("Отмена")
                    inputDialog.setWindowTitle("Подтвердите пароль")
                    inputDialog.setLabelText("Введите текущий пароль:")

                    # text, ok = inputDialog.getText(self.treeView, 'Подтвердите пароль',
                    #                                 'Введите текущий пароль:')
                    if inputDialog.exec_() == QDialog.Accepted:
                        if inputDialog.textValue() == currentPassword:
                            title = "Редактировать администратора ИБ"
                            adminDB_Dialog = AdminDB(Linux, title, self.treeView)
                            adminDB_Dialog.show()
                            adminDB_Dialog.setWindowModality(Qt.Qt.ApplicationModal)
                        else:
                            warning_text = "Неверный пароль!"
                            QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
                elif action == remDB_Admin:
                    try:
                        currNode = self.treeView.currentItem()
                        currNode.setData(19, 0, "")
                        currNode.setData(20, 0, "")
                    except Exception:
                        print(Exception)
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "5" \
                and self.treeView.itemFromIndex(indexes[0]).data(0, 0) == "Администратор базы" \
                and len(self.treeView.itemFromIndex(indexes[0]).data(19, 0)) == 0 \
                and len(self.treeView.itemFromIndex(indexes[0]).data(20, 0)) == 0:
            self.add_server_context_menu = QMenu(self.treeView)
            icon = QtGui.QIcon()
            addDB_Admin = self.add_server_context_menu.addAction("Установить администратора базы")
            icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/manager.png"), QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)
            addDB_Admin.setIcon(icon)
            action = self.add_server_context_menu.exec_(self.treeView.mapToGlobal(event))
            if action is not None:
                if action == addDB_Admin:
                    title = "Установить администратора ИБ"
                    adminDB_Dialog = AdminDB(Linux, title, self.treeView)
                    adminDB_Dialog.setWindowModality(Qt.Qt.ApplicationModal)
                    adminDB_Dialog.show()

    def create_context_menu_tableview(self, event):
        ix = self.treeView.indexAt(event)
        if ix.column() == -1:
            return
        tree_indexes = self.treeView.selectedIndexes()
        if tree_indexes[0].data(0) == "Сеансы":
            indexes = self.tableView.selectedIndexes()
            if len(indexes) > 0:

                curr_index = self.tableView.currentIndex()
                curr_col = curr_index.column()
                cur_row = curr_index.row()
                user = self.tableView.model().index(cur_row, 5).data(0)
                c1c_type = self.tableView.model().index(cur_row, 7).data(0)
                if c1c_type == "Designer":
                    c1c_type = "Конфигуратор"
                else:
                    c1c_type = "Предприятие"
                self.add_server_context_menu = QMenu(self.tableView)
                firstMenu = self.add_server_context_menu.addAction("Завершить сеанс пользователя [" + user + " : " +
                                                                   c1c_type + "]")
                icon = QtGui.QIcon()
                icon.addPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/close-line.png"), QtGui.QIcon.Normal,
                               QtGui.QIcon.Off)

                firstMenu.setIcon(icon)
                action = self.add_server_context_menu.exec_(self.tableView.mapToGlobal(event))
                if action is not None:
                    if action == firstMenu:
                        session_id = self.tableView.model().index(cur_row, 0).data(0)
                        self.terminate_session(session_id)

    def terminate_session(self, session_id):
        indexes = self.treeView.selectedIndexes()
        ib_admin = indexes[0].sibling(3, 19).data(0)
        ib_admin_pwd = indexes[0].sibling(3, 20).data(0)
        item = self.treeView.currentItem()

        self.tableView.setModel(None)
        if self.net_server:
            net_server_conn_data = self.net_server_conn_data(item)
            cluster_id = self.cluster_id(item, net_server_conn_data)
            # cluster = self.cluster_id(item)
            # ib_id = self.ib_id(item)
            if Linux:
                cluster_auth_data = self.cluster_auth_data(item)
                cluster_admin = cluster_auth_data[0]
                cluster_admin_pwd = cluster_auth_data[1]

                if cluster_admin == "":
                    cluster_admin_pwd_command = ""
                else:
                    cluster_admin_pwd_command = " --cluster-user=" + cluster_admin + " --cluster-pwd=" + cluster_admin_pwd

                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                              net_server_conn_data[1] + " session terminate --session=" + session_id + \
                              " --cluster=" + cluster_id + cluster_admin_pwd_command
            else:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                              net_server_conn_data[1] + " session terminate --session=" + session_id + \
                              " --cluster=" + cluster_id
            try:
                raw_output = subprocess.check_output(bashCommand, shell=True)
            except:
                if Linux:
                    output = raw_output.decode('utf-8')
                else:
                    output = raw_output.decode('cp866')
                output = output.replace("\r", "")
                str = output.split("\n")
                warning_text = "Требуется ввести логин/пароль админа информационной базы!"
                QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
                return
        else:
            net_server_conn_data = None
            cluster_id = self.cluster_id(item)
            if Linux:
                cluster_auth_data = self.cluster_auth_data(item)
                cluster_admin = cluster_auth_data[0]
                cluster_admin_pwd = cluster_auth_data[1]

                if cluster_admin == "":
                    cluster_admin_pwd_command = ""
                else:
                    cluster_admin_pwd_command = " --cluster-user=" + cluster_admin + " --cluster-pwd=" + \
                              cluster_admin_pwd

                bashCommand = "\"" + self.PathToRac.text() + "\"" + " session terminate --session=" + session_id + \
                              " --cluster=" + cluster_id + cluster_admin_pwd_command
            else:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " session terminate --session=" + session_id + \
                              " --cluster=" + cluster_id
            try:
                raw_output = subprocess.check_output(bashCommand, shell=True)
            except:
                if Linux:
                    output = raw_output.decode('utf-8')
                else:
                    output = raw_output.decode('cp866')
                output = output.replace("\r", "")
                str = output.split("\n")
                warning_text = "Требуется ввести логин/пароль админа информационной базы!"
                QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
                return

        self.add_sessions_lock_conn(item, ib_admin, ib_admin_pwd, "session list", net_server_conn_data)

        self.tableView.resizeColumnsToContents()

    def create_server_window(self):
        self.server_dialog = ServerDialog(Linux, self.treeView)
        self.server_dialog.show()

    def create_infoBase_window(self):
        item = self.treeView.currentItem()
        item_parent = item.parent()
        while item_parent.data(1, 0) != "1":
            item_parent = item_parent.parent()
        server = item_parent.data(0, 0)

        self.infoBase_dialog = InfoBaseDialog(Linux, Windows, LastPath, server, self.treeView)
        self.infoBase_dialog.show()

    def removeSrvr(self):
        title = "Удаление сервера"
        question = "Действительно удалить сервер?"
        x = 320
        y = 100
        ex = Confirm_Dialog(Linux, title, question, x, y, self.treeView)
        if ex.exec():
            currNode = self.treeView.currentItem()
            try:
                # Try to delete the child node (via its parent node, call the removeChild function to delete)
                parent1 = currNode.parent()
                parent1.removeChild(currNode)
            except Exception:
                try:
                    rootIndex = self.treeView.indexOfTopLevelItem(currNode)
                    self.treeView.takeTopLevelItem(rootIndex)
                except Exception:
                    print(Exception)
        else:
            ex.close()

    def removeDB(self):
        self.del_db_dialog = Del_DB(Linux, LastPath, self.treeView)
        self.del_db_dialog.show()

    def editDB(self, ib_name):
        item = self.treeView.currentItem()
        indexes = self.treeView.selectedIndexes()
        net_server_conn_data = self.net_server_conn_data(item)
        cluster_id = self.cluster_id(item, net_server_conn_data)
        item_parent = item.parent()
        while item_parent.data(1, 0) != "2":
            item_parent = item_parent.parent()
        server = item_parent.parent().data(0, 0)
        cluster_sAdmin = item_parent.child(0).data(7, 0)
        cluster_sPwd = item_parent.child(0).data(8, 0)
        item = self.treeView.currentItem()
        ib_id = self.ib_id(item)
        ib_user = item.child(3).data(19, 0)
        ib_pwd = item.child(3).data(20, 0)

        if ib_id == "":
            ib_details = None
            a = DB_Edit(Linux, ib_name, server, LastPath, ib_details, self.treeView)
        else:
            if Linux:
                # if cluster_sAdmin == "" or cluster_sPwd == "":
                #     warning_text = "Требуется ввести логин/пароль админа кластера!"
                #     QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
                #     return
                # else:
                if cluster_sAdmin == "":
                    cluster_admin_pwd_command = ""
                else:
                    cluster_admin_pwd_command = " --cluster-user=" + cluster_sAdmin + " --cluster-pwd=" + cluster_sPwd

                if self.net_server:
                    bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                                  net_server_conn_data[1] + " infobase info --cluster=" + cluster_id + \
                                  cluster_admin_pwd_command + \
                                  " --infobase=" + ib_id + " --infobase-user=" + "\"" + ib_user + "\"" + \
                                  " --infobase-pwd=" + ib_pwd
                else:
                    bashCommand = "\"" + self.PathToRac.text() + "\"" + " infobase info --cluster=" + \
                                  cluster_id + cluster_admin_pwd_command + \
                                  " --infobase=" + ib_id + " --infobase-user=" + "\"" + ib_user + "\"" + \
                                  " --infobase-pwd=" + ib_pwd
            else:
                if self.net_server:
                    bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                                  net_server_conn_data[1] + " infobase info --cluster=" + cluster_id + \
                                  " --infobase=" + ib_id + " --infobase-user=" + "\"" + ib_user + "\"" + \
                                  " --infobase-pwd=" + ib_pwd
                else:
                    bashCommand = "\"" + self.PathToRac.text() + "\"" + " infobase info --cluster=" + cluster_id + \
                                  " --infobase=" + ib_id + " --infobase-user=" + "\"" + ib_user + "\"" + \
                                  " --infobase-pwd=" + ib_pwd
            ib_details = self.check_ib_details_command(bashCommand)
            if isinstance(ib_details, str):
                if ib_details.find("Недостаточно прав") != -1:
                    title = "Администратор инф. базы"
                    cluster_adm_pwd = Cluster_admin(title)
                    rez = cluster_adm_pwd.exec_()
                    if rez == 0:
                        return
                    ib_user = cluster_adm_pwd.lineEdit_login.text()
                    ib_pwd = cluster_adm_pwd.lineEdit_pwd.text()
                    if Linux:
                        # if cluster_sAdmin == "" or cluster_sPwd == "":
                        #     warning_text = "Требуется ввести логин/пароль админа кластера!"
                        #     QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
                        #     return
                        # else:
                        if cluster_sAdmin == "":
                            cluster_admin_pwd_command = ""
                        else:
                            cluster_admin_pwd_command = " --cluster-user=" + cluster_sAdmin + " --cluster-pwd=" + cluster_sPwd

                        if self.net_server == True:
                            bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[
                                0] + ":" + \
                                          net_server_conn_data[1] + " infobase info --cluster=" + cluster_id + \
                                          cluster_admin_pwd_command + \
                                          " --infobase=" + ib_id + " --infobase-user=" + "\"" + ib_user + "\"" + \
                                          " --infobase-pwd=" + ib_pwd
                        else:
                            bashCommand = "\"" + self.PathToRac.text() + "\"" + " infobase info --cluster=" + \
                                          cluster_id + cluster_admin_pwd_command + \
                                          " --infobase=" + ib_id + " --infobase-user=" + "\"" + ib_user + "\"" + \
                                          " --infobase-pwd=" + ib_pwd
                    else:
                        bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                                      net_server_conn_data[1] + " infobase info --cluster=" + cluster_id + \
                                      " --infobase=" + ib_id + " --infobase-user=" + "\"" + ib_user + "\"" + \
                                      " --infobase-pwd=" + ib_pwd
                ib_details = self.check_ib_details_command(bashCommand)

                if isinstance(ib_details, str):
                    warning_text = ib_details
                    QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
                    return
                elif isinstance(ib_details, list):
                    a = DB_Edit(Linux, ib_user, ib_pwd, ib_name, server, LastPath, ib_details, self.treeView)
        a.show()

    def nodeClicked(self):
        indexes = self.treeView.selectedIndexes()
        # Значение индекса берется из модели [def createTreeViewModel]:
        # [0] - Уровень ноды (1 - Сервер, 2 - Кластер, 3 - Инф. базы, 4 - База, 5 - Детали (Сеансы и т.п.))
        # [1] - Имя ноды
        # [2] - Имя сервера
        # [3] - Адрес сервера
        # [4] - Порт
        # []
        # []
        # if indexes[1].data() == "1":            #Если это 1-й уровень ("Сервер")
        if self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "1":
            model = TreeTableModel("1")
            self.tableView.setModel(model.model)
            self.tableView.model().insertRow(0)
            li_st = [0, 1, 2, 3, 4, 22]
            count = 0
            for a in li_st:
                cellData = self.treeView.itemFromIndex(indexes[0]).data(a, 0)  # indexes[a].data()    # Данные в ячейке
                item = QStandardItem(cellData)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                model.model.setItem(0, count, item)
                self.tableView.resizeColumnToContents(count)
                count += 1
        elif self.treeView.itemFromIndex(indexes[0]).data(1,
                                                          0) == "1.1":  # Если это 1.1-й уровень ("Администратор агента")
            model = TreeTableModel("1.1")
            self.tableView.setModel(model.model)
            li_st = [0, 1, 5, 6]
            count = 0
            for a in li_st:
                cellData = self.treeView.itemFromIndex(indexes[0]).data(a, 0)  # Данные в ячейке
                colHeader = self.treeView.model().headerData(a, Qt.Qt.Horizontal)  # Заголовок колонки дерева
                if a == 6:
                    if cellData != "":
                        item = QStandardItem("******")
                    else:
                        item = QStandardItem()
                else:
                    item = QStandardItem(cellData)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.tableView.model().setItem(0, count, item)
                self.tableView.model().setHeaderData(count, Qt.Qt.Horizontal, colHeader)  # Заголовок колонки таблицы
                if a == 0:
                    self.tableView.resizeColumnToContents(count)
                elif a == 6:
                    self.tableView.setColumnWidth(count, 140)
                count += 1
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "2":  # Если это 1-й уровень ("Локальный кластер")
            item = self.treeView.currentItem()
            net_server_conn_data = self.net_server_conn_data(item)
            model = TreeTableModel("2")
            self.tableView.setModel(model.model)
            self.tableView.model().insertRow(0)
            if net_server_conn_data != None:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                              net_server_conn_data[1] + " cluster list"
            else:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " cluster list"
            raw_output = subprocess.check_output(bashCommand, shell=True)
            if Linux:
                output = raw_output.decode('utf-8')
            else:
                output = raw_output.decode('cp866')
            output = output.replace("\r", "")
            str_ng = output.split("\n")
            for line in str_ng:
                double_quote = line.find(":") + 1
                if line.find("cluster") != -1:
                    cluster = line[double_quote:len(line)].strip()
                    break
            item = QStandardItem(cluster)
            item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            model.model.setItem(0, 0, item)
            self.tableView.resizeColumnsToContents()
        elif self.treeView.itemFromIndex(indexes[0]).data(1,
                                                          0) == "2.1":  # Если это 2.1-й уровень ("Администратор кластера")
            model = TreeTableModel("2.1")
            self.tableView.setModel(model.model)
            self.tableView.model().insertRow(0)
            li_st = [0, 1, 7, 8]
            count = 0
            for a in li_st:
                cellData = self.treeView.itemFromIndex(indexes[0]).data(a, 0)  # Данные в ячейке
                colHeader = self.treeView.model().headerData(a, Qt.Qt.Horizontal)  # Заголовок колонки дерева
                if a == 8:
                    if cellData != "":
                        item = QStandardItem("******")
                    else:
                        item = QStandardItem()
                else:
                    item = QStandardItem(cellData)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                model.model.setItem(0, count, item)
                count += 1
            self.tableView.resizeColumnsToContents()
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "4":  # Если это 4-й уровень ("Информационная база")
            item = self.treeView.currentItem()
            net_server_conn_data = self.net_server_conn_data(item)
            ib_admin = indexes[0].child(3, 19).data(0)
            ib_admin_pwd = indexes[0].child(3, 20).data(0)
            self.add_sessions_lock_conn(item, ib_admin, ib_admin_pwd, "infobase info", net_server_conn_data)
        # Если это 5-й уровень (внутри "Информационная база")
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "5" \
                and self.treeView.itemFromIndex(indexes[0]).data(0, 0) == "Сеансы":
            item = self.treeView.currentItem()
            net_server_conn_data = self.net_server_conn_data(item)
            ib_admin = indexes[0].sibling(3, 19).data(0)
            ib_admin_pwd = indexes[0].sibling(3, 20).data(0)
            self.add_sessions_lock_conn(item, ib_admin, ib_admin_pwd, "session list", net_server_conn_data)
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "5" \
                and self.treeView.itemFromIndex(indexes[0]).data(0, 0) == "Блокировки":
            item = self.treeView.currentItem()
            net_server_conn_data = self.net_server_conn_data(item)
            ib_admin = indexes[0].sibling(2, 19).data(0)
            ib_admin_pwd = indexes[0].sibling(2, 20).data(0)
            item = self.treeView.currentItem()
            self.add_sessions_lock_conn(item, ib_admin, ib_admin_pwd, "lock list", net_server_conn_data)
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "5" \
                and self.treeView.itemFromIndex(indexes[0]).data(0, 0) == "Соединения":
            item = self.treeView.currentItem()
            net_server_conn_data = self.net_server_conn_data(item)
            ib_admin = indexes[0].sibling(1, 19).data(0)
            ib_admin_pwd = indexes[0].sibling(1, 20).data(0)
            item = self.treeView.currentItem()
            self.add_sessions_lock_conn(item, ib_admin, ib_admin_pwd, "connection list", net_server_conn_data)
        elif self.treeView.itemFromIndex(indexes[0]).data(1, 0) == "5" \
                and self.treeView.itemFromIndex(indexes[0]).data(0, 0) == "Администратор базы":
            model = TreeTableModel("5", "Администратор базы")
            self.tableView.setModel(model.model)
            self.tableView.model().insertRow(0)
            li_st = [0, 1, 19, 20]
            count = 0
            for a in li_st:
                cellData = self.treeView.itemFromIndex(indexes[0]).data(a, 0)  # Данные в ячейке
                colHeader = self.treeView.model().headerData(a, Qt.Qt.Horizontal)  # Заголовок колонки дерева
                if a == 20:
                    if cellData != "":
                        item = QStandardItem("******")
                    else:
                        item = QStandardItem()
                else:
                    item = QStandardItem(cellData)
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                model.model.setItem(0, count, item)
                count += 1
            self.tableView.resizeColumnsToContents()

    def add_sessions_lock_conn(self, item, ib_admin, ib_admin_pwd, s_l_c, net_server_conn_data=None):
        self.tableView.setModel(None)
        cluster = self.cluster_id(item, net_server_conn_data)
        ib_id = self.ib_id(item)
        if ib_id == "":
            warning_text = "База не создана на сервере 1С"
            QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
            return
        if s_l_c == "infobase info":
            session_appendix = " --infobase-user=" + "\"" + ib_admin + "\"" + " --infobase-pwd=" + ib_admin_pwd
        else:
            session_appendix = ""
        if Linux:
            error_file = "/tmp/check_add_sessions_lock_conn_errors.txt"
            error_file_string = " 2>\"" + error_file + "\""
        else:
            win_path = os.path.expandvars("%LOCALAPPDATA%")
            error_file = win_path + r"\Temp\check_add_sessions_lock_conn_errors.txt"
            error_file_string = " 2>" + error_file
        if Linux:
            cluster_auth_data = self.cluster_auth_data(item)
            cluster_admin = cluster_auth_data[0]
            cluster_admin_pwd = cluster_auth_data[1]
            # if cluster_admin == "" or cluster_admin_pwd == "":
            #     warning_text = "Требуется ввести логин/пароль админа кластера!"
            #     QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
            #     return
            if cluster_admin == "":
                cluster_admin_pwd_command = ""
            else:
                cluster_admin_pwd_command = " --cluster-user=" + cluster_admin + " --cluster-pwd=" + cluster_admin_pwd
            if net_server_conn_data != None:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                              net_server_conn_data[1] + " " + s_l_c + " --cluster=" + cluster + \
                              cluster_admin_pwd_command + \
                              " --infobase=" + ib_id + session_appendix
                _bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                               net_server_conn_data[1] + " " + s_l_c + " --cluster=" + cluster + \
                               cluster_admin_pwd_command + \
                               " --infobase=" + ib_id + session_appendix + error_file_string
            else:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + s_l_c + " --cluster=" + cluster + \
                              cluster_admin_pwd_command + \
                              " --infobase=" + ib_id + session_appendix
                _bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + s_l_c + " --cluster=" + cluster + \
                               cluster_admin_pwd_command + \
                               " --infobase=" + ib_id + session_appendix + error_file_string
        else:
            if net_server_conn_data != None:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                              net_server_conn_data[1] + " " + s_l_c + " --cluster=" + cluster + \
                              " --infobase=" + ib_id + session_appendix
                _bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                              net_server_conn_data[1] + " " + s_l_c + " --cluster=" + cluster + \
                              " --infobase=" + ib_id + session_appendix + error_file_string
            else:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + s_l_c + " --cluster=" + cluster + \
                              " --infobase=" + ib_id + session_appendix + error_file_string
                _bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + s_l_c + " --cluster=" + cluster + \
                              " --infobase=" + ib_id + session_appendix
        _bashCommand = _bashCommand.replace("\"\"", "")
        os.system(_bashCommand)
        f = open(error_file, 'r')
        ErrorsData = f.read()
        if Linux:
            pass
        else:
            ErrorsData = ErrorsData.encode("cp1251")
            ErrorsData = ErrorsData.decode("cp866")
        if not ErrorsData == "":
            if ErrorsData.find("Недостаточно прав пользователя на информационную базу") != -1:
                QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
                f.close()
                os.remove(error_file)
                title = "Администратор инф. базы"
                cluster_adm_pwd = Cluster_admin(title)
                rez = cluster_adm_pwd.exec_()
                if rez == 0:
                    return
                ib_admin = cluster_adm_pwd.lineEdit_login.text()
                ib_admin_pwd = cluster_adm_pwd.lineEdit_pwd.text()
                if s_l_c == "infobase info":
                    bashCommand = bashCommand.replace("\"\"", "")
                    bashCommand = bashCommand.replace(" --infobase-user=", " --infobase-user=" + "\"" + ib_admin + "\"")
                    bashCommand = bashCommand.replace(" --infobase-pwd=", " --infobase-pwd=" + ib_admin_pwd)
        else:
            f.close()
            os.remove(error_file)


        try:
            raw_output = subprocess.check_output(bashCommand, shell=True)
        except:
            warning_text = "Требуется ввести логин/пароль админа информационной базы!\n" \
                           "(либо база не создана на SQL-сервере)"
            QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
            return
        if Linux:
            output = raw_output.decode('utf-8')
        else:
            output = raw_output.decode('cp866')
        output = output.replace("\r", "")
        str_ng = output.split("\n")
        li_st = []
        list_total = []
        for line in str_ng:
            if line == "":
                if len(li_st) != 0:
                    list_total.append(li_st)
                    li_st = []
                continue
            doublequote = line.find(":") + 1
            key = line[0:doublequote - 1].strip()
            value = line[doublequote:len(line)].strip()
            l = [key, value]
            li_st.append(l)
        if len(list_total) > 0:
            model = TreeTableModel("5", "СеансыБлокировкиСоединения", list_total)
            self.tableView.setModel(model.model)
            self.tableView.model().insertRow(0)
            line = 0
            for li_st in list_total:
                count = 0
                for row in li_st:
                    item = QStandardItem(row[1])
                    item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                    model.model.setItem(line, count, item)
                    count += 1
                line += 1
            self.tableView.resizeColumnsToContents()

    def cluster_id(self, item, net_server_conn_data=None):
        if net_server_conn_data != None:
            bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + net_server_conn_data[0] + ":" + \
                          net_server_conn_data[1] + " cluster list"
        else:
            bashCommand = "\"" + self.PathToRac.text() + "\"" + " cluster list"
        raw_output = subprocess.check_output(bashCommand, shell=True)
        if Linux:
            output = raw_output.decode('utf-8')
        else:
            output = raw_output.decode('cp866')
        output = output.replace("\r", "")
        str = output.split("\n")
        for line in str:
            doublequote = line.find(":") + 1
            if line.find("cluster") != -1:
                cluster_id = line[doublequote:len(line)].strip()
                break
        return cluster_id

    def cluster_auth_data(self, item):
        item_parent = item.parent()
        while item_parent.data(1, 0) != "2":
            item_parent = item_parent.parent()

        cluster_admin = item_parent.child(0).data(7, 0)
        cluster_admin_pwd = item_parent.child(0).data(8, 0)
        list = [cluster_admin, cluster_admin_pwd]
        return list

    def ib_id(self, item):
        if item.data(1, 0) == "4":
            ib_id = item.data(21, 0)
        else:
            item_parent = item.parent()
            while item_parent.data(1, 0) != "4":
                item_parent = item_parent.parent()
            ib_id = item_parent.data(21, 0)
        return ib_id

    def net_server_conn_data(self, item):
        item = self.treeView.currentItem()
        item_parent = item.parent()
        while item_parent.data(1, 0) != "1":
            item_parent = item_parent.parent()
        net_server = item_parent.data(0, 0)
        net_port = item_parent.data(22, 0)
        if net_port == "" or net_port == None:
            net_conn_data = None
        else:
            net_conn_data = [net_server, net_port]
        return net_conn_data

    def fill_tree(self, Linux):
        reply = QMessageBox.question(self, 'Важно!', "Настройки списка баз очистятся.\nПодолжить?", QMessageBox.Yes,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            if self.treeView.model().hasChildren():
                self.treeView.model().removeRows(0, self.treeView.model().rowCount())
            if self.service_started:
                self.fill_tree_next_step(Linux)
            else:
                ET = db_settings
                if Linux:
                    home = os.path.expanduser("~")
                    dir_path = home + "/.config/AdminConsole"
                    file = "common_settings.xml"
                    xml_file = dir_path + r"/" + file
                else:
                    win_path = os.path.expandvars("%APPDATA%")
                    xml_file = win_path + "\AdminConsole\common_settings.xml"
                try:
                    tree = ET.ElementTree(file=xml_file)
                    root = tree.getroot()

                    for child in root:
                        if child.tag == "CommonSettings":
                            for step_child in child:
                                if step_child.tag == "local_server":
                                    if step_child.text == "True":
                                        reply = QMessageBox.question(self, 'Важно!',
                                                                     "Согласно настроек, необходимо запустить\n"
                                                                     "локальный сервер ras!\n"
                                                                     "Запустить?", QMessageBox.Yes,
                                                                     QMessageBox.No)
                                        if reply == QMessageBox.Yes:
                                            self.start_ras_service()
                                        else:
                                            return

                        elif child.tag == "Servers":
                            count = 1
                            for step_child in child:
                                server_name = step_child.attrib["server_name"]
                                server_port = step_child.attrib["server_port"]
                                net_server = [server_name, server_port]
                                self.fill_tree_next_step(Linux, net_server)
                except:
                    reply = QMessageBox.question(self, 'Важно!', "Необходимо корректно заполнить настройки.\n"
                                                                 "Открыть меню настроек?", QMessageBox.Yes, QMessageBox.No)
                    if reply == QMessageBox.Yes:
                        self.edit_settings()
                    else:
                        return
        else:
            return

    def fill_tree_next_step(self, Linux, net_server=None):
        if Linux:
            title = "Администратор кластера"
            cluster_adm_pwd = Cluster_admin(title)
            rez = cluster_adm_pwd.exec_()
            if rez == 0:
                return
            else:
                cl_adm = cluster_adm_pwd.lineEdit_login.text()
                cl_pwd = cluster_adm_pwd.lineEdit_pwd.text()
                bashCommand = self.PathToRac.text() + " cluster list"
                conn_data = self.check_fill_tree_command(bashCommand, Linux)
        else:
            import winreg
            agent_port = ""
            server = ""
            server_port = ""
            need_to_search_cmd = True
            try:
                key = winreg.OpenKeyEx(winreg.HKEY_LOCAL_MACHINE, r"SYSTEM\\CurrentControlSet\\Services")
                key_exists = True
                i = 0
                while key_exists:
                    try:
                        a = winreg.EnumKey(key, i)
                    except:
                        key_exists = False
                        winreg.CloseKey(key)
                    i = i + 1
                    new_key = winreg.OpenKeyEx(winreg.HKEY_LOCAL_MACHINE,
                                               r"SYSTEM\\CurrentControlSet\\Services" + r"\\" + a)
                    try:
                        value = winreg.QueryValueEx(new_key, "ImagePath")  # Искомая строка
                        value_str = value[0]
                        if value_str.find("ras.exe") != -1:
                            need_to_search_cmd = False
                            winreg.CloseKey(new_key)
                            winreg.CloseKey(key)
                            key_exists = False
                    except:
                        winreg.CloseKey(new_key)
                    if new_key:
                        winreg.CloseKey(new_key)
            except Exception as e:
                print(e)

            if need_to_search_cmd:
                for p in psutil.process_iter():
                    if p.name() == 'ras.exe':
                        value_str = p.cmdline()[0] + " " + p.cmdline()[2] + " " + p.cmdline()[3]
                p1 = value_str.find("--port")
                if p1 == -1:
                    p1 = value_str.find("port:")
                is_eq = value_str.find("=", p1, p1 + 14)
                space = value_str.find(" ", p1, p1 + 14)
                double_quote = value_str.find(":", space)
                agent_port = value_str[is_eq + 1:space]
                server = value_str[space + 1:double_quote]
                server_port = value_str[double_quote + 1:]

            if Linux:
                error_file = "/tmp/check_cluster_list_errors.txt"
                error_file_string = " 2>\"" + error_file + "\""
            else:
                win_path = os.path.expandvars("%LOCALAPPDATA%")
                error_file = win_path + r"\Temp\check_cluster_list_errors.txt"
                error_file_string = " 2>" + error_file
            if net_server is not None:
                _bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + \
                              net_server[0] + ":" + net_server[1] + " cluster list" + error_file_string
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + \
                              net_server[0] + ":" + net_server[1] + " cluster list" + error_file_string
            else:
                _bashCommand = "\"" + self.PathToRac.text() + "\"" + " cluster list" + error_file_string
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " cluster list" + error_file_string
            os.system(_bashCommand)
            f = open(error_file, 'r')
            ErrorsData = f.read()
            if Linux:
                pass
            else:
                ErrorsData = ErrorsData.encode("cp1251")
                ErrorsData = ErrorsData.decode("cp866")

            if not ErrorsData == "":
                QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
                f.close()
                os.remove(error_file)
                return
            else:
                f.close()
                os.remove(error_file)
                conn_data = self.check_fill_tree_command(bashCommand, Linux)
            f.close()
            os.remove(error_file)

        nodeLevel = "1"
        nodeName = conn_data[1]
        sName = conn_data[1]
        sAddr = conn_data[1]
        sPort = conn_data[2]
        sAdmin = ""
        sPwd = ""

        cluster_nodeName = conn_data[3][1:len(conn_data[3]) - 1]
        cluster_nodeLevel = "2"
        clusterAdmin_nodeName = "Администратор кластера"
        clusterAdmin_nodeLevel = "2.1"
        if Linux:
            cluster_sAdmin = cl_adm
            cluster_sPwd = cl_pwd
        else:
            cluster_sAdmin = ""
            cluster_sPwd = ""

        adminAgentLevel = "1.1"
        adminAgent_nodeName = "Администратор агента"
        adminAgent = ""
        adminAgentPwd = ""

        infoBases_level = "3"
        infoBases_nodeName = "Информационные базы"

        self.treeView.setColumnCount(23)
        self.treeView.setHeaderLabels(["Сервер:", "Уровень", "Имя сервера", "Адрес сервера", "Порт", "Админ. агента",
                                       "Пароль админ. агента", "Админ. кластера", "Пароль админ. кластера",
                                       "Наименование БД", "Описание", "База данных", "Защищенное", "Адрес БД",
                                       "Тип СУБД", "Пользователь БД", "Пароль пользователя БД", "Язык БД",
                                       "Смещение дат", "Пользователь ИБ", "Пароль пользователя ИБ",
                                       "ID информационной базы", "Порт сервера для подключения"])
        if net_server is not None:
            l1 = QtWidgets.QTreeWidgetItem([nodeName, nodeLevel, sName, sAddr, sPort, "", "", "", "", "", "", "",
                                            "", "", "", "", "", "", "", "", "", "", net_server[1]])
        else:
            l1 = QtWidgets.QTreeWidgetItem([nodeName, nodeLevel, sName, sAddr, sPort, "", "", "", "", "", "", "",
                                            "", "", "", "", "", "", "", "", "", "", ""])
        # l1_1 = QtWidgets.QTreeWidgetItem([adminAgent_nodeName, adminAgentLevel, "", "", "", sAdmin, sPwd, "", "",
        #                                   "", "", "", "", "", "", "", "", "", "", "", "", "", ""])
        l2 = QtWidgets.QTreeWidgetItem([cluster_nodeName, cluster_nodeLevel, "", "", "", "", "", "", "", "", "",
                                        "", "", "", "", "", "", "", "", "", "", "", ""])
        l2_1 = QtWidgets.QTreeWidgetItem(
            [clusterAdmin_nodeName, clusterAdmin_nodeLevel, "", "", "", "", "", cluster_sAdmin, cluster_sPwd, "",
             "", "", "", "", "", "", "", "", "", "", "", "", ""])

        l3 = QtWidgets.QTreeWidgetItem([infoBases_nodeName, infoBases_level, "", "", "", "", "", "", "", "", "",
                                        "", "", "", "", "", "", "", "", "", "", "", ""])
        cluster_id = conn_data[0]
        if Linux:
            error_file = "/tmp/check_db_list_errors.txt"
            error_file_string = " 2>\"" + error_file + "\""
        else:
            win_path = os.path.expandvars("%LOCALAPPDATA%")
            error_file = win_path + r"\Temp\check_db_list_errors.txt"
            error_file_string = " 2>" + error_file

        if cluster_sAdmin == "":
            cluster_admin_pwd_command = ""
        else:
            cluster_admin_pwd_command = " --cluster-user=" + cluster_sAdmin + " --cluster-pwd=" + cluster_sPwd

        if net_server is not None:
            if Linux:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + \
                              net_server[0] + ":" + net_server[1] + " infobase summary list --cluster=" + \
                              cluster_id + cluster_admin_pwd_command + \
                              error_file_string
            else:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " " + \
                              net_server[0] + ":" + net_server[1] + " infobase summary list --cluster=" + \
                              cluster_id + error_file_string
        else:
            if Linux:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " infobase summary list --cluster=" + \
                              cluster_id + cluster_admin_pwd_command + \
                              error_file_string
            else:
                bashCommand = "\"" + self.PathToRac.text() + "\"" + " infobase summary list --cluster=" + \
                              cluster_id + error_file_string
        os.system(bashCommand)
        f = open(error_file, 'r')
        ErrorsData = f.read()
        if Linux:
            pass
        else:
            ErrorsData = ErrorsData.encode("cp1251")
            ErrorsData = ErrorsData.decode("cp866")

        if not ErrorsData == "":
            QMessageBox.warning(None, 'Предупреждение', ErrorsData, QMessageBox.Ok)
            f.close()
            os.remove(error_file)
            return
        f.close()
        os.remove(error_file)

        ib_data = self.check_ib_command(bashCommand)
        if ib_data != None:
            for ib in ib_data:
                l4 = QtWidgets.QTreeWidgetItem([ib[1], "4", "", "", "", "", "", "", "", "", ib[2], "", "", "",
                                                "", "", "", "", "", "", "", ib[0], ""])
                for node_name in ["Сеансы", "Блокировки", "Соединения", "Администратор базы"]:
                    l5 = QtWidgets.QTreeWidgetItem([node_name, "5", "", "", "", "", "", "", "", "", "", "", "", "",
                                                    "", "", "", "", "", "", "", "", ""])
                    l4.addChild(l5)
                l3.addChild(l4)
            if Linux:
                l2.addChild(l2_1)
            l2.addChild(l3)
            l1.addChild(l2)
            # l1.addChild(l1_1)

            self.treeView.addTopLevelItem(l1)
            SaveData(Linux, self.treeView)
        else:
            text = "На сервере 1С еще не зарегистрировано ни одной базы."
            message = QMessageBox.information(None, 'Информация', text, QMessageBox.Ok)
            SaveData(Linux, self.treeView)

    def check_fill_tree_command(self, bashCommand, Linux):
        raw_output = subprocess.check_output(bashCommand, shell=True)  # путь
        # subprocess возвращает результат в байтах,
        # его нужно преобразовать в текст (можно выбрать другую кодировку)
        if Linux:
            output = raw_output.decode('utf-8')
        else:
            output = raw_output.decode('cp866')
        output = output.replace("\r", "")
        str_ng = output.split("\n")
        kill_by_memory_with_dump = ""
        for line in str_ng:
            double_quote = line.find(":") + 1
            if line.find("cluster") != -1:
                cluster = line[double_quote:len(line)].strip()
            elif line.find("host") != -1:
                host = line[double_quote:len(line)].strip()
            elif line.find("port") != -1:
                port = line[double_quote:len(line)].strip()
            elif line.find("name") != -1:
                name = line[double_quote:len(line)].strip()
            elif line.find("expiration-timeout") != -1:
                expiration_timeout = line[double_quote:len(line)].strip()
            elif line.find("lifetime-limit") != -1:
                lifetime_limit = line[double_quote:len(line)].strip()
            elif line.find("max-memory-size") != -1:
                max_memory_size = line[double_quote:len(line)].strip()
            elif line.find("max-memory-time-limit") != -1:
                max_memory_time_limit = line[double_quote:len(line)].strip()
            elif line.find("security-level") != -1:
                security_level = line[double_quote:len(line)].strip()
            elif line.find("session-fault-tolerance-level") != -1:
                session_fault_tolerance_level = line[double_quote:len(line)].strip()
            elif line.find("load-balancing-mode") != -1:
                load_balancing_mode = line[double_quote:len(line)].strip()
            elif line.find("errors-count-threshold") != -1:
                errors_count_threshold = line[double_quote:len(line)].strip()
            elif line.find("kill-problem-processes") != -1:
                kill_problem_processes = line[double_quote:len(line)].strip()
            elif line.find("kill-by-memory-with-dump") != -1:
                kill_by_memory_with_dump = line[double_quote:len(line)].strip()
        list = [cluster, host, port, name, expiration_timeout, lifetime_limit, max_memory_size,
                max_memory_time_limit, security_level, session_fault_tolerance_level,
                load_balancing_mode, errors_count_threshold, kill_problem_processes, kill_by_memory_with_dump]
        return list

    def check_ib_command(self, bashCommand):
        raw_output = subprocess.check_output(bashCommand, shell=True)
        output = raw_output.decode('utf-8')
        output = output.replace("\r", "")
        str_ng = output.split("\n")
        li_st = []
        need_to_add = False

        infobase = None
        name = None
        descr = None

        for line in str_ng:
            double_quote = line.find(":") + 1
            if line.find("infobase") != -1:
                infobase = line[double_quote:len(line)].strip()
            elif line.find("name") != -1:
                name = line[double_quote:len(line)].strip()
            elif line.find("descr") != -1:
                descr = line[double_quote:len(line)].strip()
            elif line == "":
                if infobase != None and name != None and descr != None:
                    ib = [infobase, name, descr]
                    need_to_add = True
                else:
                    return None
            if need_to_add:
                li_st.append(ib)
                need_to_add = False
        li_st.remove(ib)
        return li_st

    def check_ib_details_command(self, bashCommand):
        try:
            raw_output = subprocess.check_output(bashCommand, shell=True, stderr=subprocess.STDOUT)  # путь
        except subprocess.CalledProcessError as e:
            if Linux:
                output = e.output.decode('utf-8')
            else:
                output = e.output.decode('cp866')
            output = output.replace("\r", "")
            str_ng = output.split("\n")
            return str_ng[0]
        # subprocess возвращает результат в байтах,
        # его нужно преобразовать в текст (можно выбрать другую кодировку)
        if Linux:
            output = raw_output.decode('utf-8')
        else:
            output = raw_output.decode('cp866')
        output = output.replace("\r", "")
        str_ng = output.split("\n")
        reserve_working_processes = ""
        for line in str_ng:
            double_quote = line.find(":") + 1
            if line.find("infobase") == 0:
                infobase = line[double_quote:len(line)].strip()
            elif line.find("name") == 0:
                name = line[double_quote:len(line)].strip()
            elif line.find("dbms") == 0:
                dbms = line[double_quote:len(line)].strip()
            elif line.find("db-server") == 0:
                db_server = line[double_quote:len(line)].strip()
            elif line.find("db-name") == 0:
                db_name = line[double_quote:len(line)].strip()
            elif line.find("db-user") == 0:
                db_user = line[double_quote:len(line)].strip()
            elif line.find("security-level") == 0:
                security_level = line[double_quote:len(line)].strip()
            elif line.find("license-distribution") == 0:
                license_distribution = line[double_quote:len(line)].strip()
            elif line.find("scheduled-jobs-deny") == 0:
                scheduled_jobs_deny = line[double_quote:len(line)].strip()
            elif line.find("sessions-deny") == 0:
                sessions_deny = line[double_quote:len(line)].strip()
            elif line.find("denied-from") == 0:
                denied_from = line[double_quote:len(line)].strip()
            elif line.find("denied-message") == 0:
                denied_message = line[double_quote:len(line)].strip()
            elif line.find("denied-parameter") == 0:
                denied_parameter = line[double_quote:len(line)].strip()
            elif line.find("denied-to") == 0:
                denied_to = line[double_quote:len(line)].strip()
            elif line.find("permission-code") == 0:
                permission_code = line[double_quote:len(line)].strip()
            elif line.find("external-session-manager-connection-string") == 0:
                external_session_manager_connection_string = line[double_quote:len(line)].strip()
            elif line.find("external-session-manager-required") == 0:
                external_session_manager_required = line[double_quote:len(line)].strip()
            elif line.find("security-profile-name") == 0:
                security_profile_name = line[double_quote:len(line)].strip()
            elif line.find("safe-mode-security-profile-name") == 0:
                safe_mode_security_profile_name = line[double_quote:len(line)].strip()
            elif line.find("reserve-working-processes") == 0:
                reserve_working_processes = line[double_quote:len(line)].strip()
            elif line.find("descr") == 0:
                descr = line[double_quote:len(line)].strip()
        li_st = [infobase, name, dbms, db_server, db_name, db_user, security_level, license_distribution,
                 scheduled_jobs_deny, sessions_deny, denied_from, denied_message, denied_parameter, denied_to,
                 permission_code, external_session_manager_connection_string, external_session_manager_required,
                 security_profile_name, safe_mode_security_profile_name, reserve_working_processes, descr]
        return li_st

    def check_settings(self):
        if Linux:
            home = os.path.expanduser("~")
            dir = home + "/.config/AdminConsole"
            file = "common_settings.xml"
            xml_file = dir + "/" + file
        else:
            # %APPDATA%          возвращает "AppData\Roaming"
            # %LOCALAPPDATA%     возвращает "AppData\Local"
            # %APPDATA%\LocalLow возвращает "AppData\LocalLow"
            xml_file = os.path.expandvars(r"%APPDATA%\AdminConsole\common_settings.xml")
        try:
            ET = db_settings
            tree = ET.ElementTree(file=xml_file)
        except:
            return
        root = tree.getroot()

        for child in root:
            if child.tag == "CommonSettings":
                for step_child in child:
                    if step_child.tag == "local_server":
                        if step_child.text == "True":
                            self.local_server = True
                            self.net_server = False
                        else:
                            self.local_server = False
                            self.net_server = True

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Консоль администрирования v.1.0"))
        self.label.setText(_translate("MainWindow", "Путь до файла rac:"))
        self.PathToRac.setToolTip(_translate("MainWindow", "<html><head/><body><p>Расположение файла <span style=\" "
                                                           "font-weight:600;\">./rac</span></p></body></html>"))
        self.pushButton.setText(_translate("MainWindow", "..."))
        self.pushButtonRefresh.setText(_translate("MainWindow", "Обновить данные"))
        self.pushButton_start_ras.setText(_translate("MainWindow", "Запустить ras"))
        self.pushButton_settings.setText(_translate("MainWindow", "Настройки"))
        if Linux:
            self.label_tooltip.setToolTip(_translate("MainWindow", "<html><head/><body><p>Путь до <span style="
                                                                   "\" font-weight:600;\">активного</span> файла ./rac"
                                                                   "</p></body></html>"))
        else:
            self.label_tooltip.setToolTip(_translate("MainWindow", "<html><head/><body><p>Путь до <span style="
                                                                   "\" font-weight:600;\">активного</span> файла rac.exe"
                                                                   "</p></body></html>"))
        self.label_tooltip.setText(_translate("MainWindow", "?"))
        self.RacUnderText.setText(_translate("MainWindow", ""))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)

    ui = Ui_MainWindow(app)
    ui.show()
    sys.exit(app.exec_())
