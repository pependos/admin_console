
from PyQt5 import QtCore
from PyQt5.QtGui import QStandardItemModel

class TreeTableModel():
    def __init__(self, level, node_name="", list_total=[], parent=None):
        super().__init__()

        if level == "1":
            self.set_model_1()
        elif level == "1.1":
            self.set_model_1_1()
        elif level == "2":
            self.set_model_2()
        elif level == "2.1":
            self.set_model_2_1()
        elif level == "4":
            self.set_model_4()
        elif level == "5" and node_name == "СеансыБлокировкиСоединения":
            self.set_model_sessions(list_total)
        elif level == "5" and node_name == "Администратор базы":
            self.set_model_5()


    def set_model_1(self):
        self.model = QStandardItemModel(0, 6)
        self.model.setHeaderData(0, QtCore.Qt.Horizontal, "Имя ноды")
        self.model.setHeaderData(1, QtCore.Qt.Horizontal, "Уровень")
        self.model.setHeaderData(2, QtCore.Qt.Horizontal, "Имя сервера")
        self.model.setHeaderData(3, QtCore.Qt.Horizontal, "Адрес сервера")
        self.model.setHeaderData(4, QtCore.Qt.Horizontal, "Порт")
        self.model.setHeaderData(5, QtCore.Qt.Horizontal, "Порт сервера для подключения")

    def set_model_1_1(self):
        self.model = QStandardItemModel(0, 4)
        self.model.setHeaderData(0, QtCore.Qt.Horizontal, "Имя ноды")
        self.model.setHeaderData(1, QtCore.Qt.Horizontal, "Уровень")
        self.model.setHeaderData(2, QtCore.Qt.Horizontal, "Админ. агента")
        self.model.setHeaderData(3, QtCore.Qt.Horizontal, "Пароль админ. агента")

    def set_model_2(self):
        self.model = QStandardItemModel(0, 1)
        self.model.setHeaderData(0, QtCore.Qt.Horizontal, "ID кластера")

    def set_model_2_1(self):
        self.model = QStandardItemModel(0, 4)
        self.model.setHeaderData(0, QtCore.Qt.Horizontal, "Имя ноды")
        self.model.setHeaderData(1, QtCore.Qt.Horizontal, "Уровень")
        self.model.setHeaderData(2, QtCore.Qt.Horizontal, "Админ. кластера")
        self.model.setHeaderData(3, QtCore.Qt.Horizontal, "Пароль админ. кластера")

    def set_model_4(self):
        self.model = QStandardItemModel(0, 13)
        self.model.setHeaderData(0, QtCore.Qt.Horizontal, "Имя ноды")
        self.model.setHeaderData(1, QtCore.Qt.Horizontal, "Уровень")
        self.model.setHeaderData(2, QtCore.Qt.Horizontal, "Наименование БД")
        self.model.setHeaderData(3, QtCore.Qt.Horizontal, "Описание")
        self.model.setHeaderData(4, QtCore.Qt.Horizontal, "База данных")
        self.model.setHeaderData(5, QtCore.Qt.Horizontal, "Защищенное")
        self.model.setHeaderData(6, QtCore.Qt.Horizontal, "Адрес БД")
        self.model.setHeaderData(7, QtCore.Qt.Horizontal, "Тип СУБД")
        self.model.setHeaderData(8, QtCore.Qt.Horizontal, "Пользователь БД")
        self.model.setHeaderData(9, QtCore.Qt.Horizontal, "Пароль пользователя БД")
        self.model.setHeaderData(10, QtCore.Qt.Horizontal, "Язык БД")
        self.model.setHeaderData(11, QtCore.Qt.Horizontal, "Смещение дат")
        self.model.setHeaderData(12, QtCore.Qt.Horizontal, "ID информационной базы")

    def set_model_5(self):
        self.model = QStandardItemModel(0, 4)
        self.model.setHeaderData(0, QtCore.Qt.Horizontal, "Имя ноды")
        self.model.setHeaderData(1, QtCore.Qt.Horizontal, "Уровень")
        self.model.setHeaderData(2, QtCore.Qt.Horizontal, "Пользователь БД")
        self.model.setHeaderData(3, QtCore.Qt.Horizontal, "Пароль пользователя БД")

    def set_model_sessions(self, list_total):
        line = 0
        for list in list_total:
            count = 0
            self.model = QStandardItemModel(line, len(list))
            for row in list:
                self.model.setHeaderData(count, QtCore.Qt.Horizontal, row[0])
                count += 1
            line += 1
