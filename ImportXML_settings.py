
from PyQt5.QtWidgets import QTableWidgetItem

import xml.etree.cElementTree as ET

class ImportXML_settings():
    def __init__(self, Linux, xml_file, parent=None):
        super().__init__()
        try:
            tree = ET.ElementTree(file=xml_file)
        except:
            return
        root = tree.getroot()

        for child in root:
            if child.tag == "CommonSettings":
                for step_child in child:
                    if step_child.tag == "local_server":
                        if step_child.text == "True":
                            parent.checkBox_local_srvr.setChecked(True)
                            parent.pushButton_add_srvr.setEnabled(False)
                            parent.pushButton_rem_srvr.setEnabled(False)
                        else:
                            parent.checkBox_local_srvr.setChecked(False)
                            parent.pushButton_add_srvr.setEnabled(True)
                            parent.pushButton_rem_srvr.setEnabled(True)
                    elif step_child.tag == "net_srvr":
                        if step_child.text == "True":
                            parent.checkBox_net_srvr.setChecked(True)
                        else:
                            parent.checkBox_net_srvr.setChecked(False)
            elif child.tag == "Servers":
                row = 0
                for step_child in child:
                    parent.tableWidget.insertRow(row)
                    parent.tableWidget.setItem(row, 0, QTableWidgetItem(step_child.attrib["server_name"]))
                    parent.tableWidget.setItem(row, 1, QTableWidgetItem(step_child.attrib["server_port"]))
                    row += 1
