
from PyQt5 import Qt, QtCore, QtWidgets
from PyQt5.QtWidgets import QMessageBox, QFileDialog, QInputDialog, QDialog

from Instruction_systemd import Instruction_systemd
from Instruction_systemd_win import Instruction_systemd_win

import os
import platform
import glob
import psutil
import ctypes
import sys
import time


class Ras(QtWidgets.QMainWindow):
    def __init__(self, Linux, parent=None):
        super().__init__(parent, QtCore.Qt.Window)

        self.setObjectName("MainWindow")
        self.resize(520, 368)
        self.setMinimumSize(QtCore.QSize(520, 368))
        self.setMaximumSize(QtCore.QSize(520, 368))
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setMinimumSize(QtCore.QSize(501, 128))
        self.frame.setMaximumSize(QtCore.QSize(501, 128))
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setLineWidth(2)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_start_systemd = QtWidgets.QLabel(self.frame)
        self.label_start_systemd.setMinimumSize(QtCore.QSize(259, 27))
        self.label_start_systemd.setMaximumSize(QtCore.QSize(259, 27))
        self.label_start_systemd.setObjectName("label_start_systemd")
        self.horizontalLayout.addWidget(self.label_start_systemd)
        spacerItem = QtWidgets.QSpacerItem(18, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButton_instr = QtWidgets.QPushButton(self.frame)
        self.pushButton_instr.setMinimumSize(QtCore.QSize(181, 30))
        self.pushButton_instr.setMaximumSize(QtCore.QSize(181, 30))
        self.pushButton_instr.setObjectName("pushButton_instr")
        self.horizontalLayout.addWidget(self.pushButton_instr)
        self.pushButton_instr.clicked.connect(lambda: self.show_instr(Linux))
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(self.frame)
        self.label.setMinimumSize(QtCore.QSize(91, 18))
        self.label.setMaximumSize(QtCore.QSize(18, 16777215))
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        spacerItem1 = QtWidgets.QSpacerItem(328, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.lineEdit_service_name_systemd = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_service_name_systemd.setMinimumSize(QtCore.QSize(280, 24))
        self.lineEdit_service_name_systemd.setMaximumSize(QtCore.QSize(280, 24))
        self.lineEdit_service_name_systemd.setObjectName("lineEdit_service_name_systemd")
        if Linux:
            self.lineEdit_service_name_systemd.setText("ras.service")
        self.horizontalLayout_3.addWidget(self.lineEdit_service_name_systemd)
        spacerItem2 = QtWidgets.QSpacerItem(17, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem2)
        self.pushButton_start_systemd = QtWidgets.QPushButton(self.frame)
        self.pushButton_start_systemd.setMinimumSize(QtCore.QSize(92, 30))
        self.pushButton_start_systemd.setMaximumSize(QtCore.QSize(92, 30))
        self.pushButton_start_systemd.setObjectName("pushButton_start_systemd")
        self.pushButton_start_systemd.clicked.connect(lambda: self.start_systemd(Linux))
        self.horizontalLayout_3.addWidget(self.pushButton_start_systemd)

        self.pushButton_services = QtWidgets.QPushButton(self.frame)
        self.pushButton_services.setMinimumSize(QtCore.QSize(75, 30))
        self.pushButton_services.setMaximumSize(QtCore.QSize(75, 30))
        self.pushButton_services.setObjectName("pushButton_services")
        self.pushButton_services.clicked.connect(self.start_services)
        self.horizontalLayout_3.addWidget(self.pushButton_services)

        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(20, 34, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem3, 1, 0, 1, 1)
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setMinimumSize(QtCore.QSize(501, 128))
        self.frame_2.setMaximumSize(QtCore.QSize(501, 128))
        self.frame_2.setFrameShape(QtWidgets.QFrame.Box)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setLineWidth(2)
        self.frame_2.setObjectName("frame_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame_2)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_start_cmd = QtWidgets.QLabel(self.frame_2)
        self.label_start_cmd.setMinimumSize(QtCore.QSize(241, 27))
        self.label_start_cmd.setMaximumSize(QtCore.QSize(241, 27))
        self.label_start_cmd.setObjectName("label_start_cmd")
        self.horizontalLayout_4.addWidget(self.label_start_cmd)
        spacerItem4 = QtWidgets.QSpacerItem(218, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem4)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_2 = QtWidgets.QLabel(self.frame_2)
        self.label_2.setMinimumSize(QtCore.QSize(111, 18))
        self.label_2.setMaximumSize(QtCore.QSize(111, 18))
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_6.addWidget(self.label_2)
        spacerItem5 = QtWidgets.QSpacerItem(318, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem5)
        self.verticalLayout_2.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.lineEdit_path_to_rac = QtWidgets.QLineEdit(self.frame_2)
        self.lineEdit_path_to_rac.setMinimumSize(QtCore.QSize(320, 24))
        self.lineEdit_path_to_rac.setMaximumSize(QtCore.QSize(320, 24))
        self.lineEdit_path_to_rac.setObjectName("lineEdit_path_to_rac")
        self.horizontalLayout_5.addWidget(self.lineEdit_path_to_rac)
        self.pushButton_sel_rac = QtWidgets.QPushButton(self.frame_2)
        self.pushButton_sel_rac.setMinimumSize(QtCore.QSize(41, 37))
        self.pushButton_sel_rac.setMaximumSize(QtCore.QSize(41, 37))
        self.pushButton_sel_rac.setObjectName("pushButton_sel_rac")
        self.pushButton_sel_rac.clicked.connect(lambda: self.chooseFileRas(Linux))
        self.horizontalLayout_5.addWidget(self.pushButton_sel_rac)
        self.pushButton_start_cmd = QtWidgets.QPushButton(self.frame_2)
        self.pushButton_start_cmd.setMinimumSize(QtCore.QSize(92, 37))
        self.pushButton_start_cmd.setMaximumSize(QtCore.QSize(92, 37))
        self.pushButton_start_cmd.setObjectName("pushButton_start_cmd")
        self.pushButton_start_cmd.clicked.connect(lambda: self.start_cmd(Linux, parent))
        self.horizontalLayout_5.addWidget(self.pushButton_start_cmd)
        self.verticalLayout_2.addLayout(self.horizontalLayout_5)
        self.gridLayout.addWidget(self.frame_2, 2, 0, 1, 1)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        spacerItem6 = QtWidgets.QSpacerItem(388, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem6)
        self.pushButton_Close = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Close.setMinimumSize(QtCore.QSize(92, 37))
        self.pushButton_Close.setMaximumSize(QtCore.QSize(92, 37))
        self.pushButton_Close.setObjectName("pushButton_Close")
        self.pushButton_Close.clicked.connect(self.close_window)
        self.horizontalLayout_7.addWidget(self.pushButton_Close)
        self.gridLayout.addLayout(self.horizontalLayout_7, 3, 0, 1, 1)
        self.setCentralWidget(self.centralwidget)

        self.retranslateUi(Linux)
        QtCore.QMetaObject.connectSlotsByName(self)

        self.onStartUp(Linux)

    def onStartUp(self, Linux):
        SysType = platform.system()
        SysArch = platform.architecture()

        if SysType == "Windows" and SysArch[0] == "32bit":
            targetPattern = r"c:\Program Files (x86)\1cv8\**\ras.exe"
            Files = glob.glob(targetPattern, recursive=True)
            Files.sort(reverse=True)
            if len(Files) > 0:
                self.lineEdit_path_to_rac.setText(Files[0])
        elif SysType == "Windows" and SysArch[0] == "64bit":
            targetPattern = r'c:\Program Files\1cv8\**\ras.exe'
            Files = glob.glob(targetPattern, recursive=True)
            Files.sort(reverse=True)
            if len(Files) > 0:
                self.lineEdit_path_to_rac.setText(Files[0])
        elif SysType == "Linux":
            targetPattern = r"/opt/1cv8/x86_64/**/ras"
            Files = glob.glob(targetPattern, recursive=True)
            Files.sort(reverse=True)
            if len(Files) > 0:
                self.lineEdit_path_to_rac.setText(Files[0])

        if Linux == False:
            for service in psutil.win_service_iter():
                bin_path = service.binpath()
                if bin_path.find("ras.exe") > 0:
                    self.lineEdit_service_name_systemd.setText(service.name())

    def start_systemd(self, Linux):
        service_name = self.lineEdit_service_name_systemd.text()
        if Linux:
            need_to_close = False
            if service_name != "":
                need_to_close = True
                bash_command = "systemctl start " + service_name +  " 2>\"/tmp/check_service_tmp.txt\""
                os.system(bash_command)
                f = open("/tmp/check_service_tmp.txt", 'r')
                something_in_file = f.read()
                if something_in_file != "":
                    need_to_close = False
                    warning_text = "Нет службы с таким именем!"
                    QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
                os.remove('/tmp/check_service_tmp.txt')
            if need_to_close == True:
                self.close()
        else:
            if self.lineEdit_service_name_systemd.text() != "":
                self.pushButton_start_systemd.setDisabled(False)
                bashCommand = "sc start " + "\"" + self.lineEdit_service_name_systemd.text() + "\""
                QtWidgets.QApplication.clipboard().setText(bashCommand)
                self.parent().trayIcon.showMessage(r"Команда скопирована",
                                          r"Команда запуска службы скопирована в буфер обмена", )
                ctypes.windll.shell32.ShellExecuteW(None, 'runas', 'cmd.exe', " ".join(sys.argv), None, 1)
            else:
                self.pushButton_start_systemd.setDisabled(True)

    def start_services(self):
        bash_command = "services.msc"
        os.system(bash_command)

    def close_window(self):
        self.close()

    def chooseFileRas(self, Linux):
        Path = self.lineEdit_path_to_rac.text()
        SysArch = platform.architecture()
        if Linux:
            fname = QFileDialog.getOpenFileName(self.pushButton_sel_rac, "Выберите файл ras", r"/opt/1cv8/x86_64", "ras")
        else:
            if SysArch[0] == "64bit":
                fname = QFileDialog.getOpenFileName(self.pushButton_sel_rac, "Выберите файл ras.exe", Path, "ras.exe")
            else:
                fname = QFileDialog.getOpenFileName(self.pushButton_sel_rac, "Выберите файл ras.exe", Path, "ras.exe")
        if not fname[0] == "":
            self.lineEdit_path_to_rac.setText(fname[0])

    def show_instr(self, Linux):
        text = self.lineEdit_path_to_rac.text()
        if Linux:
            self.instr_window = Instruction_systemd()
        else:
            self.instr_window = Instruction_systemd_win(text)
        self.instr_window.setWindowModality(Qt.Qt.ApplicationModal)
        self.instr_window.show()

    def start_cmd(self, Linux, parent):
        if Linux:
            bashCommand = self.lineEdit_path_to_rac.text() + " cluster --daemon"
            os.system(bashCommand)
            self.close()
            # parent.check_service_status()
            # if parent.service_started == True:
            #     self.label_icon.setPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/correct.svg"))
            #     self.label_icon.setToolTip("Сервер ras запущен")
            # else:
            #     self.label_icon.setPixmap(QtGui.QPixmap(r":/AdminConsole/Icons/wrong.svg"))
            #     self.label_icon.setToolTip("Сервер ras не запущен")
        else:
            if self.lineEdit_path_to_rac.text() != "":
                self.pushButton_start_cmd.setDisabled(False)
                inputDialog = QInputDialog()
                inputDialog.setOkButtonText("ОК")
                inputDialog.resize(350, 100)
                inputDialog.setCancelButtonText("Отмена")
                inputDialog.setWindowTitle("Порт сервера ras")
                inputDialog.setLabelText("Введите порт сервера ras:")
                inputDialog.setTextValue("1545")
                if inputDialog.exec_() == QDialog.Accepted:
                    ras_port = inputDialog.textValue()
                else:
                    return
                inputDialog = QInputDialog()
                inputDialog.setOkButtonText("ОК")
                inputDialog.resize(350, 100)
                inputDialog.setCancelButtonText("Отмена")
                inputDialog.setWindowTitle("Адрес сервера 1С")
                inputDialog.setLabelText("Введите адрес сервера 1С:")
                if inputDialog.exec_() == QDialog.Accepted:
                    srvr_addr = inputDialog.textValue()
                else:
                    return
                inputDialog = QInputDialog()
                inputDialog.setOkButtonText("ОК")
                inputDialog.resize(350, 100)
                inputDialog.setCancelButtonText("Отмена")
                inputDialog.setWindowTitle("Порт сервера 1С")
                inputDialog.setLabelText("Введите порт сервера 1С:")
                inputDialog.setTextValue("1540")
                if inputDialog.exec_() == QDialog.Accepted:
                    srvr_port = inputDialog.textValue()
                else:
                    return
                bashCommand = "\"" + self.lineEdit_path_to_rac.text() + "\"" + " cluster --port=" + ras_port +\
                              " " + srvr_addr + ":" + srvr_port
                os.system('start cmd /k ' + bashCommand)
                # time.sleep(2)  # Sleep for 3 seconds
                # parent.check_service_status()
                # if parent.service_started == True:
                #     pixmap = QtGui.QPixmap(r":/AdminConsole/Icons/correct.png")
                #     pixmap_scaled = pixmap.scaled(16, 16, Qt.Qt.KeepAspectRatio, Qt.Qt.FastTransformation)
                #     self.parent().label_icon.setPixmap(pixmap_scaled)
                #     self.parent().label_icon.setToolTip("Сервер ras запущен")
                # else:
                #     pixmap = QtGui.QPixmap(r":/AdminConsole/Icons/wrong.png")
                #     pixmap_scaled = pixmap.scaled(16, 16, Qt.Qt.KeepAspectRatio, Qt.Qt.FastTransformation)
                #     self.parent().label_icon.setPixmap(pixmap_scaled)
                #     self.parent().label_icon.setToolTip("Сервер ras не запущен")
            else:
                self.pushButton_start_cmd.setDisabled(True)
                # parent.check_service_status()
                # if parent.service_started == True:
                #     pixmap = QtGui.QPixmap(r":/AdminConsole/Icons/correct.png")
                #     pixmap_scaled = pixmap.scaled(16, 16, Qt.Qt.KeepAspectRatio, Qt.Qt.FastTransformation)
                #     self.parent().label_icon.setPixmap(pixmap_scaled)
                #     self.parent().label_icon.setToolTip("Сервер ras запущен")
                # else:
                #     pixmap = QtGui.QPixmap(r":/AdminConsole/Icons/wrong.png")
                #     pixmap_scaled = pixmap.scaled(16, 16, Qt.Qt.KeepAspectRatio, Qt.Qt.FastTransformation)
                #     self.parent().label_icon.setPixmap(pixmap_scaled)
                #     self.parent().label_icon.setToolTip("Сервер ras не запущен")
        self.close()

    def retranslateUi(self, Linux):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Запуск службы ras"))
        if Linux:
            self.label_start_systemd.setText(_translate("MainWindow", r"[Linux] Создать демон и запустить (systemd)"))
            self.label_start_cmd.setText(_translate("MainWindow", r"[Linux] Запустить сервер ras стандартно"))
            self.label_2.setText(_translate("MainWindow", r"Путь к файлу ras:"))
        else:
            self.label_start_systemd.setText(_translate("MainWindow", r"[Win] Создать как службу и запустить"))
            self.label_start_cmd.setText(_translate("MainWindow", r"[Win] Запустить сервер ras"))
            self.label_2.setText(_translate("MainWindow", r"Путь к файлу ras.exe:"))
        self.pushButton_instr.setText(_translate("MainWindow", "Инструкция и создание"))
        self.label.setText(_translate("MainWindow", "Имя службы:"))
        self.pushButton_start_systemd.setText(_translate("MainWindow", "Запустить!"))
        self.pushButton_services.setText(_translate("MainWindow", "Службы"))
        self.pushButton_sel_rac.setText(_translate("MainWindow", "..."))
        self.pushButton_start_cmd.setText(_translate("MainWindow", "Запустить!"))
        self.pushButton_Close.setText(_translate("MainWindow", "Закрыть"))
