
from PyQt5 import QtCore, QtWidgets

import ctypes
import sys

class Instruction_systemd_win(QtWidgets.QMainWindow):
    def __init__(self, text, parent=None):
        super().__init__(parent, QtCore.Qt.Window)

        self.setObjectName("MainWindow")
        self.resize(800, 809)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_16 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_16.setObjectName("horizontalLayout_16")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setMinimumSize(QtCore.QSize(61, 16))
        self.label.setMaximumSize(QtCore.QSize(61, 16))
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.lineEdit_ServiceName = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_ServiceName.setMinimumSize(QtCore.QSize(133, 20))
        self.lineEdit_ServiceName.setMaximumSize(QtCore.QSize(133, 20))
        self.lineEdit_ServiceName.setObjectName("lineEdit_ServiceName")
        self.lineEdit_ServiceName.editingFinished.connect(self.editingFinished)
        self.horizontalLayout.addWidget(self.lineEdit_ServiceName)
        self.horizontalLayout_10.addLayout(self.horizontalLayout)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setMinimumSize(QtCore.QSize(61, 16))
        self.label_2.setMaximumSize(QtCore.QSize(61, 16))
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_4.addWidget(self.label_2)
        self.lineEdit_PathToRas = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_PathToRas.setMinimumSize(QtCore.QSize(133, 20))
        self.lineEdit_PathToRas.setMaximumSize(QtCore.QSize(133, 20))
        self.lineEdit_PathToRas.setObjectName("lineEdit_PathToRas")
        self.lineEdit_PathToRas.setText(text)
        self.lineEdit_PathToRas.editingFinished.connect(self.editingFinished)
        self.horizontalLayout_4.addWidget(self.lineEdit_PathToRas)
        self.horizontalLayout_10.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setMinimumSize(QtCore.QSize(61, 16))
        self.label_3.setMaximumSize(QtCore.QSize(61, 16))
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_7.addWidget(self.label_3)
        self.lineEdit_RasPort = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_RasPort.setMinimumSize(QtCore.QSize(216, 20))
        self.lineEdit_RasPort.setMaximumSize(QtCore.QSize(216, 20))
        self.lineEdit_RasPort.setObjectName("lineEdit_RasPort")
        self.lineEdit_RasPort.editingFinished.connect(self.editingFinished)
        self.horizontalLayout_7.addWidget(self.lineEdit_RasPort)
        self.horizontalLayout_10.addLayout(self.horizontalLayout_7)
        self.horizontalLayout_16.addLayout(self.horizontalLayout_10)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_16.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout_16)
        self.horizontalLayout_17 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_17.setObjectName("horizontalLayout_17")
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setMinimumSize(QtCore.QSize(61, 16))
        self.label_4.setMaximumSize(QtCore.QSize(61, 16))
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        self.lineEdit_AgentName = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_AgentName.setMinimumSize(QtCore.QSize(133, 20))
        self.lineEdit_AgentName.setMaximumSize(QtCore.QSize(20, 16777215))
        self.lineEdit_AgentName.setObjectName("lineEdit_AgentName")
        self.lineEdit_AgentName.editingFinished.connect(self.editingFinished)
        self.horizontalLayout_2.addWidget(self.lineEdit_AgentName)
        self.horizontalLayout_11.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setMinimumSize(QtCore.QSize(61, 16))
        self.label_5.setMaximumSize(QtCore.QSize(61, 16))
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_5.addWidget(self.label_5)
        self.lineEdit_CtrlPort = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_CtrlPort.setMinimumSize(QtCore.QSize(133, 20))
        self.lineEdit_CtrlPort.setMaximumSize(QtCore.QSize(133, 20))
        self.lineEdit_CtrlPort.setObjectName("lineEdit_CtrlPort")
        self.lineEdit_CtrlPort.editingFinished.connect(self.editingFinished)
        self.horizontalLayout_5.addWidget(self.lineEdit_CtrlPort)
        self.horizontalLayout_11.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setMinimumSize(QtCore.QSize(61, 16))
        self.label_6.setMaximumSize(QtCore.QSize(61, 16))
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_8.addWidget(self.label_6)
        self.comboBox_auto = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_auto.setMinimumSize(QtCore.QSize(216, 20))
        self.comboBox_auto.setMaximumSize(QtCore.QSize(216, 20))
        self.comboBox_auto.setObjectName("comboBox_auto")
        self.comboBox_auto.addItem("")
        self.comboBox_auto.addItem("")
        self.comboBox_auto.addItem("")
        self.comboBox_auto.addItem("")
        self.comboBox_auto.currentTextChanged.connect(self.editingFinished)
        self.horizontalLayout_8.addWidget(self.comboBox_auto)
        self.horizontalLayout_11.addLayout(self.horizontalLayout_8)
        self.horizontalLayout_17.addLayout(self.horizontalLayout_11)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_17.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_17)
        self.horizontalLayout_15 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_15.setObjectName("horizontalLayout_15")
        self.horizontalLayout_12 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_12.setObjectName("horizontalLayout_12")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setMinimumSize(QtCore.QSize(61, 16))
        self.label_7.setMaximumSize(QtCore.QSize(61, 16))
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_3.addWidget(self.label_7)
        self.lineEdit_User = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_User.setMinimumSize(QtCore.QSize(133, 20))
        self.lineEdit_User.setMaximumSize(QtCore.QSize(133, 20))
        self.lineEdit_User.setObjectName("lineEdit_User")
        self.lineEdit_User.editingFinished.connect(self.editingFinished)
        self.horizontalLayout_3.addWidget(self.lineEdit_User)
        self.horizontalLayout_12.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setMinimumSize(QtCore.QSize(61, 16))
        self.label_8.setMaximumSize(QtCore.QSize(61, 16))
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_6.addWidget(self.label_8)
        self.lineEdit_Pwd = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Pwd.setMinimumSize(QtCore.QSize(133, 20))
        self.lineEdit_Pwd.setMaximumSize(QtCore.QSize(133, 20))
        self.lineEdit_Pwd.setObjectName("lineEdit_Pwd")
        self.lineEdit_Pwd.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_Pwd.editingFinished.connect(self.editingFinished)
        self.horizontalLayout_6.addWidget(self.lineEdit_Pwd)
        self.horizontalLayout_12.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setMinimumSize(QtCore.QSize(61, 16))
        self.label_9.setMaximumSize(QtCore.QSize(61, 16))
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_9.addWidget(self.label_9)
        self.lineEdit_DisplayName = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_DisplayName.setMinimumSize(QtCore.QSize(216, 20))
        self.lineEdit_DisplayName.setMaximumSize(QtCore.QSize(216, 20))
        self.lineEdit_DisplayName.setObjectName("lineEdit_DisplayName")
        self.lineEdit_DisplayName.editingFinished.connect(self.editingFinished)
        self.horizontalLayout_9.addWidget(self.lineEdit_DisplayName)
        self.horizontalLayout_12.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_15.addLayout(self.horizontalLayout_12)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_15.addItem(spacerItem2)
        self.verticalLayout.addLayout(self.horizontalLayout_15)
        self.label_15 = QtWidgets.QLabel(self.centralwidget)
        self.label_15.setMinimumSize(QtCore.QSize(0, 5))
        self.label_15.setMaximumSize(QtCore.QSize(16777215, 5))
        self.label_15.setText("")
        self.label_15.setObjectName("label_15")
        self.verticalLayout.addWidget(self.label_15)
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setMinimumSize(QtCore.QSize(61, 16))
        self.label_10.setMaximumSize(QtCore.QSize(61, 16))
        self.label_10.setObjectName("label_10")
        self.horizontalLayout_13.addWidget(self.label_10)
        self.lineEdit_Description = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Description.setMinimumSize(QtCore.QSize(339, 20))
        self.lineEdit_Description.setMaximumSize(QtCore.QSize(339, 20))
        self.lineEdit_Description.setObjectName("lineEdit_Description")
        self.lineEdit_Description.editingFinished.connect(self.editingFinished)
        self.horizontalLayout_13.addWidget(self.lineEdit_Description)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_13.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_13)
        self.label_12 = QtWidgets.QLabel(self.centralwidget)
        self.label_12.setMinimumSize(QtCore.QSize(0, 13))
        self.label_12.setMaximumSize(QtCore.QSize(16777215, 13))
        self.label_12.setText("")
        self.label_12.setObjectName("label_12")
        self.verticalLayout.addWidget(self.label_12)
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setMinimumSize(QtCore.QSize(111, 16))
        self.label_11.setMaximumSize(QtCore.QSize(111, 16))
        self.label_11.setObjectName("label_11")
        self.horizontalLayout_14.addWidget(self.label_11)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_14.addItem(spacerItem4)
        self.verticalLayout.addLayout(self.horizontalLayout_14)
        self.lineEdit_Command = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Command.setObjectName("lineEdit_Command")
        self.verticalLayout.addWidget(self.lineEdit_Command)
        self.gridLayout.addLayout(self.verticalLayout, 2, 0, 1, 1)
        self.pushButton_CreateService = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_CreateService.setObjectName("pushButton_CreateService")
        self.pushButton_CreateService.clicked.connect(self.CreateService)
        self.gridLayout.addWidget(self.pushButton_CreateService, 4, 0, 1, 1)
        self.pushButton_Close = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Close.setObjectName("pushButton_Close")
        self.pushButton_Close.clicked.connect(self.Close)
        self.gridLayout.addWidget(self.pushButton_Close, 5, 0, 1, 1)
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setMinimumSize(QtCore.QSize(0, 478))
        self.textEdit.setMaximumSize(QtCore.QSize(16777215, 478))
        self.textEdit.setObjectName("textEdit")
        self.gridLayout.addWidget(self.textEdit, 0, 0, 1, 1)
        self.label_14 = QtWidgets.QLabel(self.centralwidget)
        self.label_14.setText("")
        self.label_14.setObjectName("label_14")
        self.gridLayout.addWidget(self.label_14, 1, 0, 1, 1)
        spacerItem5 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem5, 3, 0, 1, 1)
        self.setCentralWidget(self.centralwidget)

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)

    def editingFinished(self):
        service_name = self.lineEdit_ServiceName.text()
        path_to_ras = self.lineEdit_PathToRas.text()
        ras_port = self.lineEdit_RasPort.text()
        agent_name = self.lineEdit_AgentName.text()
        ctrl_port = self.lineEdit_CtrlPort.text()
        auto = self.comboBox_auto.currentText()
        if auto == "Автоматически":
            auto = "auto"
        elif auto == "Вручную":
            auto = "demand"
        elif auto == "Отключена":
            auto = "disabled"
        elif auto == "Автоматически (отложенный запуск)":
            auto = "delayed-auto"

        user = self.lineEdit_User.text()
        password = self.lineEdit_Pwd.text()
        display_name = self.lineEdit_DisplayName.text()
        description = self.lineEdit_Description.text()

        command = self.create_command_string(service_name, path_to_ras, ras_port, agent_name, ctrl_port, auto, user,
                                             password, display_name, description)
        self.lineEdit_Command.setText(command)

    def create_command_string(self, service_name, path_to_ras, ras_port, agent_name, ctrl_port, auto, user,
                                   password, display_name, description):
        command = "sc create \"" + service_name + "\" binPath= \"\\\"" + path_to_ras + "\\\" cluster --service --port=" + \
                  ras_port + " " + agent_name + ":" + ctrl_port + "\" start= " + auto + " obj= \"" + user + \
                  "\" password= \"" + password + "\" displayname= \"" + display_name + "\"" + " & sc description \"" + \
                  service_name + "\" \"" + description + "\""
        return command

    def Close(self):
        self.close()

    def CreateService(self):
        command = self.lineEdit_Command.text()
        QtWidgets.QApplication.clipboard().setText(command)
        ctypes.windll.shell32.ShellExecuteW(None, 'runas', 'cmd.exe', " ".join(sys.argv), None, 1)

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Инструкция по созданию службы ras для Windows"))
        self.label.setText(_translate("MainWindow", "ServiceName"))
        self.lineEdit_ServiceName.setText(_translate("MainWindow", "1C Server RAS"))
        self.label_2.setText(_translate("MainWindow", "PathToRas"))
        self.label_3.setText(_translate("MainWindow", "RasPort"))
        self.lineEdit_RasPort.setText(_translate("MainWindow", "1545"))
        self.label_4.setText(_translate("MainWindow", "AgentName"))
        self.label_5.setText(_translate("MainWindow", "CtrlPort"))
        self.lineEdit_CtrlPort.setText(_translate("MainWindow", "1540"))
        self.label_6.setText(_translate("MainWindow", "auto"))
        self.comboBox_auto.setItemText(0, _translate("MainWindow", "Автоматически"))
        self.comboBox_auto.setItemText(1, _translate("MainWindow", "Автоматически (отложенный запуск)"))
        self.comboBox_auto.setItemText(2, _translate("MainWindow", "Вручную"))
        self.comboBox_auto.setItemText(3, _translate("MainWindow", "Отключена"))
        self.label_7.setText(_translate("MainWindow", "User"))
        self.label_8.setText(_translate("MainWindow", "Password"))
        self.label_9.setText(_translate("MainWindow", "DisplayName"))
        self.lineEdit_DisplayName.setText(_translate("MainWindow", "1C Server RAS"))
        self.label_10.setText(_translate("MainWindow", "Description"))
        self.lineEdit_Description.setText(_translate("MainWindow", "Служба RAS администрирования серверов 1С"))
        self.label_11.setText(_translate("MainWindow", "Итоговая команда:"))
        service_name = self.lineEdit_ServiceName.text()
        path_to_ras = self.lineEdit_PathToRas.text()
        ras_port = self.lineEdit_RasPort.text()
        agent_name = self.lineEdit_AgentName.text()
        ctrl_port = self.lineEdit_CtrlPort.text()
        auto = self.comboBox_auto.currentText()
        if auto == "Автоматически":
            auto = "auto"
        elif auto == "Вручную":
            auto = "demand"
        elif auto == "Отключена":
            auto = "disabled"
        elif auto == "Автоматически (отложенный запуск)":
            auto = "delayed-auto"

        user = self.lineEdit_User.text()
        password = self.lineEdit_Pwd.text()
        display_name = self.lineEdit_DisplayName.text()
        description = self.lineEdit_Description.text()

        command = self.create_command_string(service_name, path_to_ras, ras_port, agent_name, ctrl_port, auto, user,
                                   password, display_name, description)
        self.lineEdit_Command.setText(_translate("MainWindow", command))
        self.pushButton_CreateService.setText(_translate("MainWindow", "Скопировать строку в буфер обмена и запустить cmd.exe от Админитратора"))
        self.pushButton_Close.setText(_translate("MainWindow", "Закрыть окно"))
        self.textEdit.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-style:italic;\">1. Запустите cmd.exe с правами администратора</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'Noto Sans\'; font-size:10pt; font-style:italic;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-style:italic;\">2. Выполните следущую команду (отредактируйте под свои нужды):</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'Noto Sans\'; font-size:10pt; font-style:italic;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">sc create &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">ServiceName</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; binPath= &quot;\\&quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">PathToRas</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">\\&quot; cluster --service --port=</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">RasPort</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> </span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">AgentName</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">:</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">CtrlPort</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; start= </span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">auto</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> obj= &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">User</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; password= &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">Password</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; displayname= &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">DisplayName</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; &amp; sc description &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">ServiceName</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">Description</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot;</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'Noto Sans\'; font-size:10pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">где:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   ServiceName</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - имя создаваемой службы</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   PathToRas</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - путь к файлу </span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-style:italic;\">ras.exe</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">, который и будет запускать службу</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   RasPort</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - </span><span style=\" font-family:\'Inter\',\'Verdana\',\'Arial\',\'Helvetica\',\'sans-serif\'; font-size:10pt; color:#333333; background-color:#ffffff;\">сетевой порт на котором будет работать сервер администрирования (обычно 1545)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   AgentName</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - а</span><span style=\" font-family:\'Inter\',\'Verdana\',\'Arial\',\'Helvetica\',\'sans-serif\'; font-size:10pt; color:#333333; background-color:#ffffff;\">дрес агента сервера, который мы собираемся администрировать </span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">(имя сервера 1С)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   CtrlPort</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - </span><span style=\" font-family:\'Inter\',\'Verdana\',\'Arial\',\'Helvetica\',\'sans-serif\'; font-size:10pt; color:#333333; background-color:#ffffff;\">порт агента сервера, который мы собираемся администрировать (обычно 1540)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   auto</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - тип запуска службы (</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">auto</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> = автоматически)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   User</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - пользователь, из-под которого будет запускаться служба, например: obj= &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">.\\User1</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   Password</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - пароль пользователя</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   DisplayName</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - имя для отображения, которое будет выводиться в окне &quot;Службы&quot; (но это не имя службы! Имя службы - </span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">ServiceName</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">   Description</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> - описание службы</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\"><br /></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">Пример рабочей команды:</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'Noto Sans\'; font-size:10pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">sc create &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">1C Server RAS</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; binPath= &quot;\\&quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">C:\\Program Files\\1cv8\\8.3.20.1647\\bin\\ras.exe\\</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; cluster --service --port=</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">1545</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> </span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">serv1C</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">:</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">1540</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; start= </span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">auto</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\"> obj= &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">.\\User1</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; password= &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">Password</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; displayname= &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">1C Server RAS</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; &amp; sc description &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">1C Server RAS</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot; &quot;</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:600;\">Служба RAS администрирования серверов 1С</span><span style=\" font-family:\'Noto Sans\'; font-size:10pt;\">&quot;</span></p></body></html>"))
