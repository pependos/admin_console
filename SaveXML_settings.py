
import os

from xml.etree import cElementTree as xml

class SaveXML_settings():
    def __init__(self, Linux, parent=None):
        super().__init__()
        #Создаем XML файл.
        root = xml.Element("Settings")
        common_settings = xml.Element("CommonSettings")

        # создаем дочерний суб-элемент.
        is_local_server = xml.SubElement(common_settings, "local_server")
        is_local_server.text = str(parent.checkBox_local_srvr.isChecked())

        is_net_srvr = xml.SubElement(common_settings, "net_srvr")
        is_net_srvr.text = str(parent.checkBox_net_srvr.isChecked())

        servers = xml.Element("Servers")
        for line in range(parent.tableWidget.rowCount()):
            server = xml.SubElement(servers, "server", server_name=parent.tableWidget.item(line, 0).text(),
                                    server_port=parent.tableWidget.item(line, 1).text())
        root.append(common_settings)
        root.append(servers)
        tree = xml.ElementTree(root)

        # from xml.dom import minidom
        # xml_text = minidom.parseString(xml.tostring(root)).toprettyxml()

        if Linux:
            home = os.path.expanduser("~")
            dir = home + "/.config/AdminConsole"
            file = dir + "/" + "common_settings.xml"
            # with open(dir + "/" + "common_settings.xml", "w") as xml_file:
            tree.write(file, encoding='utf-8')
        else:
            file = os.path.expandvars(r"%APPDATA%\AdminConsole\common_settings.xml")
            tree.write(file, encoding='utf-8')
