
from PyQt5 import QtCore, QtXml
from PyQt5.QtWidgets import QTreeWidgetItem

class Parse(QtXml.QXmlDefaultHandler):
    def __init__(self, root):
        QtXml.QXmlDefaultHandler.__init__(self)
        self._root = root
        self._item = None
        self._text = ''
        self._error = ''

    def startElement(self, namespace, name, qname, attributes):
        if qname == 'node':
            if self._item is not None:
                self._item = QTreeWidgetItem(self._item)
            else:
                self._item = QTreeWidgetItem(self._root)
            self._item.setData(0, QtCore.Qt.UserRole, qname)
            self._item.setText(0, attributes.value('nodeName'))
            if qname == 'node':
                self._item.setExpanded(False)
                self._item.setText(1, attributes.value('level'))
                self._item.setText(2, attributes.value('serverName'))
                self._item.setText(3, attributes.value('serverAddress'))
                self._item.setText(4, attributes.value('port'))
                self._item.setText(5, attributes.value('agentAdmin'))
                self._item.setText(6, attributes.value('agentAdminPwd'))
                self._item.setText(7, attributes.value('clusterAdmin'))
                self._item.setText(8, attributes.value('clusterAdminPwd'))
                self._item.setText(9, attributes.value('dbName'))
                self._item.setText(10, attributes.value('description'))
                self._item.setText(11, attributes.value('db'))
                self._item.setText(12, attributes.value('protected'))
                self._item.setText(13, attributes.value('dbAdress'))
                self._item.setText(14, attributes.value('dbmsType'))
                self._item.setText(15, attributes.value('dbUser'))
                self._item.setText(16, attributes.value('dbUserPwd'))
                self._item.setText(17, attributes.value('dbLang'))
                self._item.setText(18, attributes.value('datesShift'))
                self._item.setText(19, attributes.value('ibUser'))
                self._item.setText(20, attributes.value('ibUserPwd'))
                self._item.setText(21, attributes.value('ib_id'))
                self._item.setText(22, attributes.value('conn_port'))
        self._text = ''
        return True

    def endElement(self, namespace, name, qname):
        if qname == 'root' or qname == 'node':
            if hasattr(self._item, 'parent'):
                self._item = self._item.parent()
        return True

    def characters(self, text):
        self._text += text
        return True

    def fatalError(self, exception):
        print('Parse Error: line %d, column %d: %s' % (
              exception.lineNumber(),
              exception.columnNumber(),
              exception.message(),
              ))
        return False

    def errorString(self):
        return self._error

class ImportXML():
    def __init__(self, tree, xml):
        super().__init__()
        tree.setHeaderLabels(["Сервер:", "Уровень", "Имя сервера", "Адрес сервера", "Порт", "Админ. агента",
                                       "Пароль админ. агента", "Админ. кластера", "Пароль админ. кластера", "Наименование БД",
                                       "Описание", "База данных", "Защищенное", "Адрес БД", "Тип СУБД", "Пользователь БД",
                                       "Пароль пользователя БД", "Язык БД", "Смещение дат", "Пользователь БД",
                                       "Пароль пользователя БД", "ID информационной базы", "Порт сервера для подключения"])
        tree.setHeaderHidden(False)

        source = QtXml.QXmlInputSource()
        source.setData(xml)
        handler = Parse(tree)
        reader = QtXml.QXmlSimpleReader()
        reader.setContentHandler(handler)
        reader.setErrorHandler(handler)
        reader.parse(source)
