
from PyQt5 import QtCore, QtWidgets, Qt, QtGui
from PyQt5.QtWidgets import QLineEdit, QMessageBox

from SaveData import SaveData

import os
import icons_rc

class ServerDialog(QtWidgets.QMainWindow):
    def __init__(self, Linux, parent=None):
        super().__init__(parent, QtCore.Qt.Window)
        self.setWindowTitle("Добавить сервер")
        self.resize(460, 220)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/AdminConsole/Icons/server-add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(QtGui.QIcon(icon))

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setMinimumSize(QtCore.QSize(460, 168))
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.labelSrvrName = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSrvrName.sizePolicy().hasHeightForWidth())
        self.labelSrvrName.setSizePolicy(sizePolicy)
        self.labelSrvrName.setObjectName("labelSrvrName")
        self.labelSrvrName.setText("Имя сервера:")
        self.gridLayout.addWidget(self.labelSrvrName, 0, 0, 1, 1)
        self.lineEditSrvrName = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEditSrvrName.sizePolicy().hasHeightForWidth())
        self.lineEditSrvrName.setSizePolicy(sizePolicy)
        self.lineEditSrvrName.setMinimumSize(QtCore.QSize(300, 0))
        self.lineEditSrvrName.setObjectName("lineEditSrvrName")
        #self.lineEditSrvrName.setStyleSheet("border: 1px solid black;")
        self.gridLayout.addWidget(self.lineEditSrvrName, 0, 1, 1, 1)
        self.labelSrvrAddress = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSrvrAddress.sizePolicy().hasHeightForWidth())
        self.labelSrvrAddress.setSizePolicy(sizePolicy)
        self.labelSrvrAddress.setObjectName("labelSrvrAddress")
        self.labelSrvrAddress.setText("Адрес сервера:")
        self.gridLayout.addWidget(self.labelSrvrAddress, 1, 0, 1, 1)
        self.lineEditSrvrAddress = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditSrvrAddress.setMinimumSize(QtCore.QSize(300, 0))
        self.lineEditSrvrAddress.setObjectName("lineEditSrvrAddress")
        self.gridLayout.addWidget(self.lineEditSrvrAddress, 1, 1, 1, 1)

        self.labelSrvrPort = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSrvrPort.sizePolicy().hasHeightForWidth())
        self.labelSrvrPort.setSizePolicy(sizePolicy)
        self.labelSrvrPort.setObjectName("labelSrvrPort")
        self.labelSrvrPort.setText("Порт сервера:")
        self.gridLayout.addWidget(self.labelSrvrPort, 2, 0, 1, 1)
        self.lineEditSrvrPort = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditSrvrPort.setMinimumSize(QtCore.QSize(300, 0))
        self.lineEditSrvrPort.setObjectName("lineEditSrvrPort")
        self.lineEditSrvrPort.setText("1540")
        self.gridLayout.addWidget(self.lineEditSrvrPort, 2, 1, 1, 1)

        self.labelAgentPort = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelAgentPort.sizePolicy().hasHeightForWidth())
        self.labelAgentPort.setSizePolicy(sizePolicy)
        self.labelAgentPort.setObjectName("labelSrvrPort")
        self.labelAgentPort.setText("Порт агента:")
        self.gridLayout.addWidget(self.labelAgentPort, 3, 0, 1, 1)
        self.lineEditAgentPort = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditAgentPort.setMinimumSize(QtCore.QSize(300, 0))
        self.lineEditAgentPort.setObjectName("lineEditSrvrPort")
        self.lineEditAgentPort.setText("1545")
        self.gridLayout.addWidget(self.lineEditAgentPort, 3, 1, 1, 1)

        self.labelAgentAdmin = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelAgentAdmin.sizePolicy().hasHeightForWidth())
        self.labelAgentAdmin.setSizePolicy(sizePolicy)
        self.labelAgentAdmin.setObjectName("labelAgentAdmin")
        self.labelAgentAdmin.setText("Админ. агента:")
        self.gridLayout.addWidget(self.labelAgentAdmin, 4, 0, 1, 1)

        self.lineEditAgentAdmin = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEditAgentAdmin.sizePolicy().hasHeightForWidth())
        self.lineEditAgentAdmin.setSizePolicy(sizePolicy)
        self.lineEditAgentAdmin.setMinimumSize(QtCore.QSize(300, 0))
        self.lineEditAgentAdmin.setObjectName("lineEditAgentAdmin")
        self.gridLayout.addWidget(self.lineEditAgentAdmin, 4, 1, 1, 1)

        self.labelAgentPwd = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelAgentPwd.sizePolicy().hasHeightForWidth())
        self.labelAgentPwd.setSizePolicy(sizePolicy)
        self.labelAgentPwd.setObjectName("labelAgentPwd")
        self.labelAgentPwd.setText("Пароль агента:")
        self.gridLayout.addWidget(self.labelAgentPwd, 5, 0, 1, 1)

        self.lineEditAgentPwd = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditAgentPwd.setMinimumSize(QtCore.QSize(300, 0))
        self.lineEditAgentPwd.setObjectName("lineEditAgentPwd")
        self.lineEditAgentPwd.setEchoMode(QLineEdit.Password)
        self.gridLayout.addWidget(self.lineEditAgentPwd, 5, 1, 1, 1)

        ###############################
        self.labelClusterAdmin = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelClusterAdmin.sizePolicy().hasHeightForWidth())
        self.labelClusterAdmin.setSizePolicy(sizePolicy)
        self.labelClusterAdmin.setObjectName("labelClusterAdmin")
        self.labelClusterAdmin.setText("Админ. кластера:")
        self.gridLayout.addWidget(self.labelClusterAdmin, 6, 0, 1, 1)

        self.lineEditClusterAdmin = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEditClusterAdmin.sizePolicy().hasHeightForWidth())
        self.lineEditClusterAdmin.setSizePolicy(sizePolicy)
        self.lineEditClusterAdmin.setMinimumSize(QtCore.QSize(300, 0))
        self.lineEditClusterAdmin.setObjectName("lineEditClusterAdmin")
        self.gridLayout.addWidget(self.lineEditClusterAdmin, 6, 1, 1, 1)

        self.labelClusterPwd = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelClusterPwd.sizePolicy().hasHeightForWidth())
        self.labelClusterPwd.setSizePolicy(sizePolicy)
        self.labelClusterPwd.setObjectName("labelClustertPwd")
        self.labelClusterPwd.setText("Пароль кластера:")
        self.gridLayout.addWidget(self.labelClusterPwd, 7, 0, 1, 1)

        self.lineEditClusterPwd = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditClusterPwd.setMinimumSize(QtCore.QSize(300, 0))
        self.lineEditClusterPwd.setObjectName("labelClusterPwd")
        self.lineEditClusterPwd.setEchoMode(QLineEdit.Password)
        self.gridLayout.addWidget(self.lineEditClusterPwd, 7, 1, 1, 1)
        #############################

        spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 8, 1, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.butOK = QtWidgets.QPushButton(self.centralwidget)
        self.butOK.setMaximumSize(QtCore.QSize(100, 16777215))
        self.butOK.setMinimumSize(QtCore.QSize(50, 25))
        self.butOK.setObjectName("butOK")
        self.butOK.setText("ОК")
        self.butOK.clicked.connect(lambda: self.accept(Linux, parent))
        self.horizontalLayout.addWidget(self.butOK)

        self.butCancel = QtWidgets.QPushButton(self.centralwidget)
        self.butCancel.setMaximumSize(QtCore.QSize(100, 16777215))
        self.butCancel.setMinimumSize(QtCore.QSize(50, 25))
        self.butCancel.setObjectName("butCancel")
        self.butCancel.setText("Отмена")
        self.butCancel.clicked.connect(self.reject)
        self.horizontalLayout.addWidget(self.butCancel)
        self.gridLayout.addLayout(self.horizontalLayout, 9, 1, 1, 1)

        self.lineEditSrvrName.setFocus()

        self.setCentralWidget(self.centralwidget)

        # Установите для свойства окна значение ApplicationModal, пользователь может закрыть основной интерфейс только
        # после закрытия всплывающего окна.
        self.setWindowModality(Qt.Qt.ApplicationModal)

    def keyPressEvent(self, event):
        if event.key() == Qt.Qt.Key_Return:
            self.accept()
        elif event.key() == Qt.Qt.Key_Escape:
            self.close()

    def accept(self, Linux, parent):
        nodeLevel = "1"
        nodeName = self.lineEditSrvrName.text()
        sName = self.lineEditSrvrName.text()
        sAddr = self.lineEditSrvrAddress.text()
        sPort = self.lineEditSrvrPort.text()
        sAdmin = self.lineEditAgentAdmin.text()
        sPwd = self.lineEditAgentPwd.text()

        # if nodeName == "" or sName == "" or sAddr == "" or sPort == "" or sAdmin == "" or sPwd == "":
        #     warning_text = "Заполнены не все поля!"
        #     QMessageBox.warning(None, 'Предупреждение', warning_text, QMessageBox.Ok)
        #     return

        cluster_nodeLevel = "2"
        cluster_nodeName = "Локальный кластер"
        cluster_nodeLevel = "2"
        clusterAdmin_nodeName = "Администратор кластера"
        clusterAdmin_nodeLevel = "2.1"
        cluster_sAdmin = self.lineEditClusterAdmin.text()
        cluster_sPwd = self.lineEditClusterPwd.text()


        adminAgentLevel = "1.1"
        adminAgent_nodeName = "Администратор агента"
        adminAgent = self.lineEditAgentAdmin.text()
        adminAgentPwd = self.lineEditAgentPwd.text()

        infoBases_level = "3"
        infoBases_nodeName = "Информационные базы"


        self.parent().setColumnCount(23)
        self.parent().setHeaderLabels(["Имя ноды", "Уровень", "Имя сервера", "Адрес сервера", "Порт", "Админ. агента",
                                       "Пароль админ. агента", "Админ. кластера", "Пароль админ. кластера", "Наименование БД",
                                       "Описание", "База данных", "Защищенное", "Адрес БД", "Тип СУБД", "Пользователь БД",
                                       "Пароль пользователя БД", "Язык БД", "Смещение дат", "Пользователь БД",
                                       "Пароль пользователя БД", "ID информационной базы", "Порт сервера для подключения"])
        l1 = QtWidgets.QTreeWidgetItem([nodeName, nodeLevel, sName, sAddr, sPort])
        l1_1 = QtWidgets.QTreeWidgetItem([adminAgent_nodeName, adminAgentLevel, "", "", "", sAdmin, sPwd])
        l2 = QtWidgets.QTreeWidgetItem([cluster_nodeName, cluster_nodeLevel])
        l2_1 = QtWidgets.QTreeWidgetItem([clusterAdmin_nodeName, clusterAdmin_nodeLevel, "", "", "", "", "", cluster_sAdmin, cluster_sPwd])

        l3 = QtWidgets.QTreeWidgetItem([infoBases_nodeName, infoBases_level])
        l2.addChild(l2_1)
        l2.addChild(l3)
        l1.addChild(l2)
        l1.addChild(l1_1)

        self.parent().addTopLevelItem(l1)

        self.parent().setHeaderHidden(False)

        SaveData(Linux, parent)

        self.close()

    def reject(self):
        self.close()
